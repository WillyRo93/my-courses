# Importamos los paquetes necesarios:
from ast import arg
import pytesseract
import argparse
import cv2

# Construimos el parser:
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path de la imagen a aplicar OCR")
ap.add_argument("-d", "--digits", type=int, default=1, help="Aplicable para identificar si se hará un reconocimiento solo a numeros o no")
args = vars(ap.parse_args())

# Cargamos la imagen, convertimos BGR a RGB
# Inicializamos las opciones de Tesseract como un string vacio:
image = cv2.imread(args["image"])
rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
options = ""

# El usuario (yo) desea leer solo numeros? Si coloco digist = 0, quiero que lea todo. 
if args["digits"] > 0:
        options = "outputbase digits"

# Hacemos OCR a la imagen deseada:
text = pytesseract.image_to_string(rgb, config=options)
print(text)

# -----------------------
# Páginas 34 del libro

# -----------------------
# Para procesar el codigo, se usa el siguiente script:
# Por ahora, las imagenes solo pueden estar en la misma carpeta que el código.

            # Sin movernos de la carpeta:
    # CMD:           ocr_digits.py --image apple_support.png
    #                ocr_digits.py --image apple_support.png --digits 0
    # Visual Studio: py ocr_digits.py --image apple_support.png
    #                py ocr_digits.py --image apple_support.png --digits 0

            # Moviendonos de carpeta: (POR AHORA NO FUNCIONA)
    # CMD:           ocr_digits.py --image ../datasets/img_ocr/apple_support.png
    # Visual Studio: py ocr_digits.py --image ../datasets/img_ocr/apple_support.png