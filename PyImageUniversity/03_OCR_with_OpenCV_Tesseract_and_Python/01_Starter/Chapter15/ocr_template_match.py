# Importamos las librerias necesarias:
from imutils.contours import sort_contours
import numpy as np
import argparse
import imutils
import sys
import cv2

# Construimos el Parser:
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path a la Imagen a aplicar OCR")
ap.add_argument("-r", "--reference", type=str, default="ocr_a_reference.png",
                help="Path a la imagen de referencia OCR-A")
args = vars(ap.parse_args())

# Definimos un diccionario que contiene el primer numero de la tarjeta
# Y con ese numero se identifica que tipo de tarjeta es.
FIRST_NUMBER = {"3": "American Express", "4": "Visa",
                "5": "Master Card", "6": "Discover Card"}

# Cargamos la imagen de referencia OCR-A, lo convertimos en grayscale y le hacemos
# threshold, de tal manera que los digitos aparezcan en blanco, con un fondo negro.
ref = cv2.imread(args["reference"])
ref = cv2.cvtColor(ref, cv2.COLOR_BGR2GRAY)
ref = cv2.threshold(ref, 10, 255, cv2.THRESH_BINARY_INV)[1]

# Encontramos los contornos de la imagen de referencia OCR-A y la ordenamos de izquierda a derecha.
refCnts = cv2.findContours(ref.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
refCnts = imutils.grab_contours(refCnts)
refCnts = sort_contours(refCnts, method="left-to-right")[0]

# Inicializamos un diccionario para mapear el digito correspondiente a la region de interes.
digits = {}

# Hacemos un loop sobre los contornos de nuestra referencia OCR-A.
for (i, c) in enumerate (refCnts):
    # Calculamos el bounding box para el digito, lo extraemos y le hacemos  un resize
    (x, y, w, h) = cv2.boundingRect(c)
    roi = ref[y:y + h, x:x + w]
    roi = cv2.resize(roi, (57, 88))

    # Actualizamos los digitos en el diccionario, mapeando el nombre de los digitos a la ROI.
    digits[i] = roi

# ==============================
# A partir de este punto, ya estamos listos extrayendo los digitos de nuestra imagen
# de referencia y asociandolo con nuestro digit name.
# ==============================

# Continuando con las imagenes de entrada, la cargamos y le hacemos un resize:
image = cv2.imread(args["image"])
image = imutils.resize(image, width=400)
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
cv2.imshow("Gray", gray)

# Inicializamos un rectangulo (mas ancho que alto) para aislar la tarjeta de credito del
# resto de la imagen.
kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (13, 7))

# Aplicamos un "tophat (whitehat) morphological operator" para encontrar regiones 
# iluminadas con fondo negro (Ejemplo: Los numeros de la tarjeta de credito).
tophat = cv2.morphologyEx(gray, cv2.MORPH_TOPHAT, kernel)
cv2.imshow("Tophat", tophat)

# ==============================
# Teniendo nuestra imagen tophat calculamos el gradiente a lo largo del eje X.
# ==============================
# Calculamos el gradiente Sharr de la imagen blackhat y lo escalamos a un rango [0, 255].
grad = cv2.Sobel(tophat, ddepth=cv2.CV_32F, dx=1, dy=0, ksize =-1)
grad = np.absolute(grad)
(minVal, maxVal) = (np.min(grad), np.max(grad))
grad = (grad - minVal) / (maxVal - minVal)
grad = (grad * 255).astype("uint8")
cv2.imshow("Gradient", grad)

# Aplicamos un "closing operation" usando un kernel rectangular para ayudar a cerrar los gaps
# entre los numeros de la tarjeta de credito. Luego, aplicamos el metodo thresholding de Otsu
# para binarizar la imagen.
grad = cv2.morphologyEx(grad, cv2.MORPH_CLOSE, kernel)
thresh = cv2.threshold(grad, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
cv2.imshow("Rect Close", thresh)

# ==============================
# La mayoria de las tarjetas de credito, tienen grupos de 4 digitos, llegando hasta los 16 digitos.
# Estos contornos son relativamente obvios y en este punto del codigo están conectados.
# A partir de acá, usaremos OpenCV para extraerlos.
# ==============================
# Encontramos los contornos de la imagen y los ordenamos de arriba a abajo:
cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
cnts = imutils.grab_contours(cnts)
cnts = sort_contours(cnts, method="top-to-bottom")[0]

# Inicializamos los contornos que corresponden los grupos de digitos:
locs = None

# ==============================
# Luego de inicializar las localizaciones como None, procedemos a llenarla de valores.
# ==============================
# Hacemos un loop sobre los contornos:
for i in range(0, len(cnts) - 4):
    # Tomamos el subset de contornos que estamos buscando e inicializamos la diferencia
    # total en los valores de las coordenadas en Y.
    subset = cnts [i:i + 4]
    yDiff = 0

    # Ahora hacemos un loop sobre el subset de contornos:
    for j in range(0, len(subset) - 1):
        # Calculamos las coordenadas del bounding box de cada contorno:
        (xA, yA, wA, hA) = cv2.boundingRect(subset[j])
        (xB, yB, wB, hB) = cv2.boundingRect(subset[j + 1])

        # Calculamos la diferencia absoluta entre la coordinada y de los bounding boxes
        # y lo sumamos al acumulador de diferencias en la coordenada Y.
        yDiff += np.abs(yA - yB)

    # Si existe una diferencia menor a 5 pixeles entre las coordenadas Y, significa que hemos
    # encontrado efectivamente nuestro grupo de digitos:
    if yDiff < 5:
        locs = [cv2.boundingRect(c) for c in subset]
        break

# Si el grupo de locations sigue siendo None, significa que no pudimos encontrar ningun digito en
# las imagenes de tarjetas de credito, asi que salimos el script.
if locs is None:
    print("[INFO] No se encontró ningun grupo de digitos")
    sys.exit(0)

# ==============================
# Dadas nuestras localizaciones  de digitos, los ordenamos y aislamos cada digito:
# ==============================
# Ordenar las localizaciones de digitos de izquierda a derecha, luego inicializamos
# una lista de digitos clasificados.
locs = sorted (locs, key=lambda x:x[0])
output = []

# Hacemos un loop sobre los cuatro grupos de 4 digitos:
for (gX, gY, gW, gH) in locs:
    # Inicializamos la lista de grupos de digitos:
    groupOutput = []

    # Extraemos el grupo de ROI de 4 digitos de la imagen en grayscale y aplicamos 
    # thresholding al segmento de digitos del fondo de la tarjeta de credito.
    group = gray[gY - 5:gY + gH + 5, gX - 5:gX + gW + 5]
    group = cv2.threshold(group, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

    # Detectamos los contornos de cada digito individual en el grupo, luego ordenamos
    # los contornos de los digitos de izquierda a derecha.
    digitCnts = cv2.findContours(group.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    digitCnts = imutils.grab_contours(digitCnts)
    digitCnts = sort_contours(digitCnts, method="left-to-right")[0]

    # ==============================
    # Vamos a seguir iterando sobre cada contorno individual de digitos y desarrollar un
    # template matching.
    # ==============================
    # Hacemos un loop sobre los contornos de digitos:
    for c in digitCnts:
        # Calculamos el bounding box de los digitos individuales, extraemos el digito y
        # hacemos un resize para tener el mismo fixed size que la referencia de la imageN OCR-A.
        (x, y, w, h) = cv2.boundingRect(c)
        roi = group[y:y + h, x:x + w]
        roi = cv2.resize(roi, (57, 88))

        # Inicializamos una lista de template matching scores:
        scores = []

        # Hacemos un loop sobre los digitos de referencia y los digitos de la ROI.
        for (digit, digitROI) in digits.items():
            # Aplicamos una correlation-based template matching, tomamos el score y actualizamos la lista.
            result = cv2.matchTemplate(roi, digitROI, cv2.TM_CCOEFF)
            (_, score, _, _) = cv2.minMaxLoc(result)
            scores.append(score)

        # La clasificacion para el digito en la ROI será el nombre del digito de la referencia con mayor matching score.
        groupOutput.append(str(np.argmax(scores)))

    # -------------------------------------------------------------------------
    # ==============================
    # Ya con eso hicimos el OCR con template matching para grupos de digitos. Ahora vamos a mostrar los resultados:
    # ==============================
    # Dibujamos las clasificaciones de digitos al rededor de los numeros.
    cv2.rectangle(image, (gX - 5, gY - 5), (gX + gW + 5, gY + gH + 5), (0, 0, 255), 2)
    cv2.putText(image, "".join(groupOutput), (gX, gY - 15), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 0, 255), 2)

    # Actualizamos la lista de digitos de salida.
    output.extend(groupOutput)
    print(groupOutput)

# Determinamos el tipo de carta:
cardType = FIRST_NUMBER.get(output[0], "Unknown")

# Mostramos la informacion de la tarjeta de crédito:
print("Tipo de Tarjeta de Crédito: {}".format(cardType))
print("Tarjeta de Crédito #: {}".format("".join(output)))
cv2.imshow("Imagen", image)

cv2.waitKey(0)
# -----------------------
# Páginas 130-139 del libro

# -----------------------
# CMD:           ocr_template_match.py --image images/credit_card_01.png
#                ocr_template_match.py --image images/credit_card_02.png
#                ocr_template_match.py --image images/credit_card_03.png
#                ocr_template_match.py --image images/credit_card_04.png
#                ocr_template_match.py --image images/credit_card_05.png
# Visual Studio: py ocr_template_match.py --image images/credit_card_01.png
#                py ocr_template_match.py --image images/credit_card_02.png
#                py ocr_template_match.py --image images/credit_card_03.png
#                py ocr_template_match.py --image images/credit_card_04.png
#                py ocr_template_match.py --image images/credit_card_05.png