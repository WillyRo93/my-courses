# Importamos las librerias necesarias:
from imutils.contours import sort_contours
import numpy as np
import py
import pytesseract
import argparse
import imutils
import sys
import cv2

# Construimos el parser:
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path para la imagen a aplicar OCR")
args = vars(ap.parse_args())

# Cargamos la imagen, la convertimos en grayscale y tomamos sus dimensiones:
image = cv2.imread(args["image"])
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
(H, W) = gray.shape

# Ininializamos un Kernel rectangular:
rectKernel = cv2.getStructuringElement(cv2.MORPH_RECT, (25, 7))
sqKernel = cv2.getStructuringElement(cv2.MORPH_RECT, (21, 21))

# Se limpia la imagen usando un "3x3 Gaussian blur", luego se le aplica un "blackhat
# morphological operator" para encontrar regiones oscuras en un fondo luminoso.
gray = cv2.GaussianBlur(gray, (3, 3), 0)
blackhat = cv2.morphologyEx(gray, cv2.MORPH_BLACKHAT, rectKernel)
cv2.imshow("BlackHat", blackhat)

# Calculamos el "gradiente Scharr" de la imagen blackhat y escalamos el resultado
# a un range de [0, 255].
grad = cv2.Sobel(blackhat, ddepth=cv2.CV_32F, dx=1, dy=0, ksize=-1)
grad = np.absolute(grad)
(minVal, maxVal) = (np.min(grad), np.max(grad))
grad = (grad - minVal) / (maxVal - minVal)
grad = (grad * 255).astype("uint8")
cv2.imshow("Gradient", grad)

# ==========================
# El proximo paso es tratar de detectar las lineas de la Machine Readable Zone (MZR):
# ==========================
# Aplicamos una closing operation usando un kernel rectangular para cerrar los gaps
# entre letras -- Luego se aplica el metodo de Thresholdind de Otsu.
grad = cv2.morphologyEx(grad, cv2.MORPH_CLOSE, rectKernel)
thresh = cv2.threshold(grad, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
cv2.imshow("Rect Close", thresh)

# Realizamos otra closing operation, esta vez usando el kernel cuadrado para cerrar los gaps
# entre lineas del MRZ, luego realizar una serie de erosiones para separar los componentes conectados.
thresh = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, sqKernel)
thresh = cv2.erode(thresh, None, iterations=2)
cv2.imshow("Square Close", thresh)

# ==========================
# Ahora que nuestra Machine Readable Zone es visible, vamos a encontrar los contornos en la imagen thresh
# Este proceso nos permitirá detectar y extraer la MRZ
# ==========================
# Encontramos los contornos en la thresholded imagen y la ordenamos desde abajo hasta arriba
# (Debido a que la MRZ esta el fondo del pasaporte.)
cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
cnts = imutils.grab_contours(cnts)
cnts = sort_contours(cnts, method="bottom-to-top")[0]

# Inicializamos el bounding box asociado a la MRZ.
mrzBox = None

# ==========================
# Vamos a intentar encontrar el mrzBox en los siguientes pasos:
# ==========================
# Hacemos un loop sobre los contornos:
for c in cnts:
    # Calculamos el bounding box del contorno y luego vemos cuanto de la imagen del bounding box
    # ocupa en terminos de ancho y alto.
    (x, y, w, h) = cv2.boundingRect(c)
    percentWidth = w / float(W)
    percentHeight = h/ float(H)

    # Si el bounding box ocupa > 80% del ancho y > 4% del alto de la imagen
    # Entonces, asumimos que hemos encontrado el MRZ.
    if percentWidth > 0.8 and percentHeight > 0.04:
        mrzBox = (x, y, w , h)
        break

# ==========================
# A partir de ahora, podemos procesar la MRZ.
# ==========================
# De no haber encontrado la MRZ, se sale del script:
if mrzBox is None:
    print("[INFO] MRZ no pudo ser encontrada")
    sys.exit(0)

# Rellenamos de nuevo el bounding box ya que aplicamos erosiones y necesitamos que "vuelva a crecer":
(x, y, w, h) = mrzBox
pX = int((x + w) * 0.03)
pY = int((y + h) * 0.03)
(x, y) = (x -pX, y - pY)
(w, h) = (w + (pX * 2), h + (pY * 2))

# Extraemos el "padded" MRZ de la imagen:
mrz = image[y:y + h, x:x + w]

# ==========================
# Teniendo ya la MRZ extraida, el paso final es usar Tesseract para aplicar el OCR.
# ==========================
# Usamos Tesseract para hacer el OCR a la MRZ, removiendo tambien cualquier "ocurrences of spaces"
mrzText = pytesseract.image_to_string(mrz)
mrzText = mrzText.replace(" ", "")
print(mrzText)

# Mostramos la imagen de la MRZ:
cv2.imshow("Machine Readable Zone", mrz)

cv2.waitKey(0)
# -----------------------
# Páginas 116-122 del libro

# -----------------------
# CMD:           ocr_passport.py --image passports/passport_01.png
#                ocr_passport.py --image passports/passport_02.png
# Visual Studio: py ocr_passport.py --image passports/passport_01.png
#                py ocr_passport.py --image passports/passport_02.png