# Este codigo va a poder dibujar los 10 numeros posibles en pantalla.

from Chapter16.pyimagesearch.seven_segment import segments
from pyimagesearch.seven_segment.segments import DIGITS
import numpy as np
import cv2

# Inicializamos las dimensiones de nuestra imagen de ejemplo y usamos esas dimensiones
# para definir el ancho y alto de cada uno de los 7 segmentos que vamos a examinar.
(h, w) = (470, 315)
(dW, dH, dC) = (int(w * 0.25), int(h * 0.15), int(h * 0.075))

# Definimos un set de 7 segmentos:
# NOTA: Estos son los 7 elementos o segmentos que pueden conformar un numero.
segments = [
    ((0, 0), (w, dH)), # top
    ((0, 0), (dW, h // 2)), # top-left
    ((w - dW, 0), (w, h // 2)), # top-right
    ((0, (h // 2) - dC), (w, (h // 2) + dC)), # center
    ((0, h // 2), (dW, h)), # bottom-left
    ((w - dW, h // 2), (w, h)), # bottom-right
    ((0, h - dH), (w, h)) # bottom
]

# Aqui, hacemos un loop sobre los digitos y los 7 segmentos asociados para cada digito particular.
for (digit, display) in DIGITS.items():
    # Colocamos memoria para la visualizacion del digito.
    vis = np.zeros((h, w, 3))

    # Hacemos un loop sobre los segmentos sin importar si ese segmento en particular está "ON" u "OFF".
    for (segment, on) in zip(segments, display):
        # Verificamos si el segmento está "ON"
        if on:
            # Unpack las coordenadas (X,Y) iniciales del segmento actual, luego lo dibujamos
            # en nuestra imagen de visualizacion.
            ( (startX, startY), (endX, endY) ) = segment
            cv2.rectangle(vis, (startX, startY), (endX, endY), (0, 0, 255), -1)

    # Mostramos el output de visualizacion para el digito:
    print("[INFO] Visualizacion para el numero '{}'".format(digit))
    cv2.imshow("Digito", vis)
    cv2.waitKey(0)

# -----------------------
# Páginas 147-148 del libro