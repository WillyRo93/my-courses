# Definimos un diccionario que mapea cada posible digito en el cual los
# segemnetos estan "ON".

DIGITS = {
    0: (1, 1, 1, 0, 1, 1, 1),
    1: (0, 0, 1, 0, 0, 1, 0),
    2: (1, 0, 1, 1, 1, 0, 1),
    3: (1, 0, 1, 1, 0, 1, 1),
    4: (0, 1, 1, 1, 0, 1, 0),
    5: (1, 1, 0, 1, 0, 1, 1),
    6: (1, 1, 0, 1, 1, 1, 1),
    7: (1, 0, 1, 0, 0, 1, 0),
    8: (1, 1, 1, 1, 1, 1, 1),
    9: (1, 1, 1, 1, 0, 1, 1),
}

# Ahora, usamos este diccionario de digitos para definir un diccionario inverso,
# el cual mapea segmentos que estan "ON" VS "OFF" a sus digitos correspondientes.
DIGITS_INV = {v:k for (k,v) in DIGITS.items()}
