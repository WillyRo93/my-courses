# Importamos las librerias necesarias:
from .segments import DIGITS_INV
import cv2

def recognize_digit(roi, minArea=0.4):
    # Tomamos las dimensiones de la ROI y usamos esas dimensiones para definir el ancho
    # y alto de cada uno de los 7 segmentos que examinaremos.
    (h, w) = roi.shape[:2]
    (dW, dH, dC) = (int(w * 0.25), int(h *0.15), int(h * 0.075))

    # Definimos el set de 7 segmentos:
    segments = [
        ((0, 0), (w, dH)), # top
        ((0, 0), (dW, h // 2)), # top-left
        ((w - dW, 0), (w, h // 2)), # top-right
        ((0, (h // 2) - dC), (w, (h // 2) + dC)), # center
        ((0, h // 2), (dW, h)), # bottom-left
        ((w - dW, h // 2), (w, h)), # bottom-right
        ((0, h - dH), (w, h)) # bottom
    ]

    # Inicializamos nuestro arreglo para guardar cual de los 7 segmentos está ON vs OFF.
    on = [0] * len(segments)

    # Hacemos un loop sobre los segmentos:
    for ( i, ( (startX, startY), (endX, endY) ) ) in enumerate(segments):
        # Extraemos el segmento de la ROI, contamos el numero total de pixeles thresholded en el
        # segmento, y luego se calcula el areal del segmento.
        segROI = roi[startY:endY, startX:endX]
        total = cv2.countNonZero(segROI)
        area = (endX - startX) * (endY - startY)

        # Si el numero total de "non-zero pixeles" es mayor que el porcentaje minimode area
        # Entonces, se marca el segmento como "ON".
        if total / float(area) > minArea:
            on[i] = 1
    
    # Se aplica OCR al digito usando nuestro diccionario:
    digit = DIGITS_INV.get(tuple(on), None)

    # Se retonra el digito al cual se le aplicó OCR:
    return digit

# -----------------------
# Páginas 150-151 del libro