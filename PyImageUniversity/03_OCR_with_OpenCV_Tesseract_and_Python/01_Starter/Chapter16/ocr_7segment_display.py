# Importamos las librerias necesarias:
from pyimagesearch.seven_segment.recognize import recognize_digit
import argparse
import imutils
import cv2

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path para el input de la imagen de 7 segmentos")
args = vars(ap.parse_args())

# Cargamos la imagen:
image = cv2.imread(args["image"])

# Pre procesamos la imagen y la redimensionamos, convirtiendola en grayscale, colocandola borrosa
# y haciendole thresholding.
image = imutils.resize(image, width=400)
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
blurred = cv2.GaussianBlur(gray, (5,5), 0)
thresh = cv2.threshold(blurred, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]

# Encontramos los contornos en el edge map, luego ordenamos por tamaño (orden descendente)
cnts = cv2.findContours(thresh.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
cnts = imutils.grab_contours(cnts)
cnts = sorted(cnts, key=cv2.contourArea, reverse=True)

# Inicializamos nuestros digitos a aplicar OCR:
digits = []

# Hacemos un loop sobre los contornos:
for c in cnts:
    # Calculamos los bounding box de l contorno, luego determinamos si el bounding box
    # pasa nuestro test de ancho y alto:
    (x, y, w, h) = cv2.boundingRect(c)
    passWidth = (w >= 50 and w <= 70)
    passHeight = (h >= 95 and h <= 115)

    # Verificamos que el contorno pasa ambos tests:
    if passWidth and passHeight:
        # Extraemos la ROI del digito y luego lo reconocemos:
        roi = thresh[y:y + h, x:x + w]
        digit = recognize_digit(roi)

        # Verificamos ahora, si nuestro digito pudo ser leido por OCR:
        if digit is not None:
            # Actualiza nuestra lista de digitos y dibuja el digito en la imagen:
            digits.append(digit)
            cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)
            cv2.putText(image, str(digit), (x - 10, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 255, 0), 2)

# Mostramos el tiempo en la pantalla:
formattedTime = "{}:{}{}" if len(digits) == 3 else "{}{}:{}{}"
formattedTime = formattedTime.format(*digits)
print("[INFO] La hora leida por OCR es: {}".format(formattedTime))

# Mostramos la imagen output:
cv2.imshow("Image", image)
cv2.waitKey(0)

# -----------------------
# Páginas 152-155 del libro

# -----------------------
# CMD:           ocr_7segment_display.py --image alarm_clock.png
# Visual Studio: py ocr_7segment_display.py --image alarm_clock.png