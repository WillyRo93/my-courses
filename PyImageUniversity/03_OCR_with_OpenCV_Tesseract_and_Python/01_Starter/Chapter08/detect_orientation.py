# Importamos los codigos necesarios:
from ast import arg
from numpy import require
# Output class especifica cuatro(4) datatypes, incluyendo diccionarios, los cuales usaremos:
from pytesseract import Output
import pytesseract
import argparse
import imutils
import cv2
import os

# Aqui cambiamos de directorio, porque debemos estar en la misma carpeta que los
# datasets que contienen las imagenes de los animales:
#os.chdir("images")
print("\n", "Direccion acual:", "\n", os.getcwd(), "\n")

# Construimos el parser:
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path de la imagen a aplicar OCR")
args = vars(ap.parse_args())

# Cargamos la imagen, se convierte al canal adecuado(RGB, BGR, etc)
# Luego usamos Tesseract para determinar la orientacion del texto.
image = cv2.imread(args["image"])
rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
results = pytesseract.image_to_osd(rgb, output_type=Output.DICT)

# Mostramos la informacion acerca de la orientacion:
print("[INFO] Orientacion detectada: {} ".format(results["orientation"]))
print("[INFO] Rotar {} grados° para corregir ".format(results["rotate"]))
print("[INFO] Script detectado: {} ".format(results["script"]))

# Rotamos la imagen a la orientacion correcta:
rotated = imutils.rotate_bound(image, angle=results["rotate"])

# Como ultimo paso, mostramos la imagen original y el output luego de ser corregida la orientacion.
cv2.imshow("Original", image)
cv2.imshow("Output", rotated)
cv2.waitKey(0)

# -----------------------
# Páginas 48-49 del libro

# -----------------------------------------------------
# Para procesar el codigo, se usa el siguiente script:
# Por ahora, las imagenes solo pueden estar en la misma carpeta que el código.

                        # Imagen normal, sin inclinacion:
    # CMD:           detect_orientation.py --image images/normal.png.png
    # Visual Studio: py detect_orientation.py --image images/normal.png

                        # Misma imagen pero rotada:
    # CMD:           detect_orientation.py --image images/rotated_90_clockwise.png
    # Visual Studio: py detect_orientation.py --image images/rotated_90_clockwise.png

                        # Imagen en hebreo, rotada:
    # CMD:           detect_orientation.py --image images/rotated_90_counter_clockwise_hebrew.png
    # Visual Studio: py detect_orientation.py --image images/rotated_90_counter_clockwise_hebrew.png

# -----------------------------------------------------