# Importamos los paquetes necesarios.
from textblob import TextBlob
import pytesseract
import argparse
import cv2

# Contruimos el parser:
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required = True, help= "Path para la imagen a ser aplicado OCR")
args = vars(ap.parse_args())

# Cargamos la imagen y convertir de BGR a RGB
image = cv2.imread(args["image"])
rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

# Usamos Teseract para aplicar el OCR.
text = pytesseract.image_to_string(rgb)

# Mostramos el texto *ANTES* DE SER APLICADO EL ocr-spellcheking.
print("ANTES DEL SPELLCHECKING")
print("====================")
print(text)
print("\n")

tb = TextBlob(text)
corrected = tb.correct()

print("DESPUES DEL SPELLCHECK")
print("================")
print(corrected)

# -----------------------
# Páginas 108-110 del libro

# -----------------------
# CMD:           ocr_and_spellcheck.py --image comic_spelling.png
# Visual Studio: py ocr_and_spellcheck.py --image comic_spelling.png