# Importamos los paquetes necesarios:

from pytesseract import Output
from pyimagesearch.helpers import cleanup_text
import pytesseract
import argparse
import cv2

# Construimos el parser:
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path para la imagen a aplicar OCR")
ap.add_argument("-c", "--min-conf", type=int, default=0,
                help="Valor minimo de confianza para filtrar malas detecciones de texto") 
args = vars(ap.parse_args())

# Cargamos nuestra imagen y la cambiamos de BGR a RGB, y usamos Tesseract para localizar
# cada area de texto que exista en la imagen.
image = cv2.imread(args["image"])
rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
results = pytesseract.image_to_data(rgb, output_type=Output.DICT)

# Hacemos un loop sobre cada localizacion individual de texto:
for i in range(0, len(results["text"])):
    # Extraemos las coordenadas del bounding box de texto del actual resultado.
    x = results["left"][i]
    y = results["top"][i]
    w = results["width"][i]
    h = results["height"][i]

    # Extraemos el texto junto con la confidence de la localizacion de texto:
    text = results["text"][i]
    conf = float(results["conf"][i])

    # Filtramos las localizaciones con poca confiabilidad:
    if conf > args["min_conf"]:
        # Mostramos la confiabilidad del texto:
        print("Confiabilidad: {}".format(conf))
        print("Texto: {}".format(text))
        print("-----------")

        # Hacemos strip de nuestros caracteres no-ASCII para poder ponerlos como texto usando OpenCV.
        # También dibujaremos una caja con ese texto sobrepuesto.
        text = cleanup_text(text)
        cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)
        cv2.putText(image, text, (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 1.2, (9, 9, 255), 3)

# Mostramos la imagen de salida:
cv2.imshow("Imagen", image)
cv2.waitKey(0)

# -----------------------
# Páginas 108-110 del libro

# -----------------------
# CMD:           localize_text_tesseract.py --image apple_support.png
#                localize_text_tesseract.py --image apple_support.png --min-conf 50
#                localize_text_tesseract.py --image car_wash.png
#                localize_text_tesseract.py --image car_wash.png --min-conf 50
# Visual Studio: py localize_text_tesseract.py --image apple_support.png
#                py localize_text_tesseract.py --image apple_support.png --min-conf 50
#                py localize_text_tesseract.py --image car_wash.png
#                py localize_text_tesseract.py --image car_wash.png --min-conf 50