# Con esta funcion observamos el valor ordinal del texto
# Cualquier valor ordinal mayor a 128 se considera que NO ES ASCII.

def cleanup_text(text):
    # Hacemos strip de los caracteres no ASCII, asi podremos escribirlos
    # como texto en la imagen usando OpenCV.
    return "".join([c if ord(c) < 128 else "" for c in text]).strip()

