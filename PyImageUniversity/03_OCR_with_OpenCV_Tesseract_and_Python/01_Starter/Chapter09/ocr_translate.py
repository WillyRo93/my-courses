# Importando los paquetes necesarios:
from fnmatch import translate
from textblob import TextBlob
import pytesseract
import argparse
import cv2

# Construimos el parser:
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path para la imagen a aplicar OCR")
ap.add_argument("-l", "--leng", type=str, default="es", help="Lenguaje a traducir (Default es español")
args = vars(ap.parse_args())

# Cargamos nuestra imagen y la cambiamos de canal(RGB, BGR, etc.)
image = cv2.imread(args["image"])
rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

# Usamos Tesseract para hacer el OCR a la imagen, reemplazamos los caracteres de espacio o
# nuevas lineas, lo reemplazaremos por un espacio simple.
text = pytesseract.image_to_string(rgb)
text = text.replace("\n", " ")

# Mostramos el texto del OCR original:
print("ORIGINAL")
print("========")
print(text)
print("")

# A partir de acá, se traduce a los lenguajes deseados.
tb = TextBlob(text)
translated = tb.translate(to=args["leng"])

# Mostramos la imagen traducida:
print("TRADUCIDO")
print("=========")
print(translated)

# -----------------------
# Páginas 57-58 del libro

# -----------------------
# Para procesar el codigo, se usa el siguiente script:
# Por ahora, las imagenes solo pueden estar en la misma carpeta que el código.

                # Sin movernos de la carpeta:
    # CMD:           ocr_translate.py --image comic.png
    # Visual Studio: py ocr_translate.py --image comic.png