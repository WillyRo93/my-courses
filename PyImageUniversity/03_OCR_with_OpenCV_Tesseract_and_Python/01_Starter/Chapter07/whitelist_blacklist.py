# Este codigo es uan forma de tener una lista blanca o negra de usuarios, IPs, etc
import pytesseract
import argparse
import cv2

# Construimos el parser
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path de la imagen a aplicar OCR")
ap.add_argument("-w", "--whitelist", type=str, default="", help="Lista de caracteres para whitelist")
ap.add_argument("-b", "--blacklist", type=str, default="", help="Lista de caracteres para blacklist")
args = vars(ap.parse_args())

# Cargamos la imagen, intercambiamos el canal (RGB, BGR, etc)
# Inicializamos nuestras opciones de Tesseract como un string vacio.
image = cv2.imread(args["image"])
rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
options = ""

# Revisamos para ver si un set de caracteres whitelited han sido proveidas
# De ser asi, actualizamos nuestras opciones de caracteres.
if len(args["whitelist"]) > 0:
    options += "-c tessedit_char_whitelist={} ".format(args["whitelist"])

# De igual manera. Revisamos para ver si un set de caracteres blacklited han sido proveidas
# De ser asi, actualizamos nuestras opciones de caracteres.
if len(args["blacklist"]) > 0:
    options += "-c tessedit_char_blacklist={} ".format(args["blacklist"])

# Aplicamos el OCR a la imagen:
text = pytesseract.image_to_string(rgb, config=options)
print(text)

# -----------------------
# Páginas 34 del libro

# -----------------------------------------------------
# Para procesar el codigo, se usa el siguiente script:
# Por ahora, las imagenes solo pueden estar en la misma carpeta que el código.

                        # Placa con *:
    # CMD:           whitelist_blacklist.py --image pa_license_plate.png
    # Visual Studio: py whitelist_blacklist.py --image pa_license_plate.png

                        # Placa sin *:
    # CMD:           whitelist_blacklist.py --image pa_license_plate.png --blacklist "*#"
    # Visual Studio: py whitelist_blacklist.py --image pa_license_plate.png --blacklist "*#"

    # ----------------------------
                        # Factura con toda la informacion:
    # CMD:           whitelist_blacklist.py --image invoice.png
    # Visual Studio: py whitelist_blacklist.py --image invoice.png

                        # Placa sin toda la informacion (SOLO NUMEROS):
    # CMD:           whitelist_blacklist.py --image invoice.png --whitelist "0123456789.-"
    # Visual Studio: py whitelist_blacklist.py --image invoice.png --whitelist "0123456789.-"

                        # Factura sin los "0":
    # CMD:           whitelist_blacklist.py --image invoice.png --whitelist "0123456789.-" --blacklist "0"
    # Visual Studio: py whitelist_blacklist.py --image invoice.png --whitelist "0123456789.-" --blacklist "0"

# -----------------------------------------------------

# Al aplicar el script, nos lee la placa como el siguiente output,
# por lo tanto se usa el blacklist para quitar el *:
# -----------
# ZIW*4681
# visitPA.com
# -----------

