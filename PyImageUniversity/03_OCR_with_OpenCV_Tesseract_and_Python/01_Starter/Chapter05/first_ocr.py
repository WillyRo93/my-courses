# Importamos las librerias necesarias:
import pytesseract
import argparse
import cv2
import os

# Aqui cambiamos de directorio, porque debemos estar en la misma carpeta que los
# datasets que contienen las imagenes de los animales:
#os.chdir("../../../")
print("\n", "Direccion acual:", "\n", os.getcwd(), "\n")

# Construimos el argument parser y parseamos los argumentos:
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="path para introducir la imagen a leer: ")
args = vars(ap.parse_args())

# A partir de acá, comienza el OCR:
# Primero vamos a introducir la imagen como input y convertirlo de BGR a RGB, ordenando asi los canales:
image = cv2.imread(args["image"])
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

# Usamos Tesseract para aplicar el OCR a la imagen:
text = pytesseract.image_to_string(image)
print(text)

# -----------------------
# Páginas 25-26 del libro

# -----------------------
# Para procesar el codigo, se usa el siguiente script:
# Por ahora, las imagenes solo pueden estar en la misma carpeta que el código.

                # Sin movernos de la carpeta:
    # CMD:           first_ocr.py --image pyimagesearch_address.png
    #                first_ocr.py --image steve_jobs.png
    #                first_ocr.py --image whole_foods.png
    # Visual Studio: py first_ocr.py --image pyimagesearch_address.png
    #                py first_ocr.py --image steve_jobs.png
    #                py first_ocr.py --image whole_foods.png

            # Moviendonos de carpeta: (POR AHORA NO FUNCIONA)
    # CMD:           first_ocr.py --image ../datasets/img_ocr/pyimagesearch_address.png
    # Visual Studio: py first_ocr.py --image ../datasets/img_ocr/pyimagesearch_address.png