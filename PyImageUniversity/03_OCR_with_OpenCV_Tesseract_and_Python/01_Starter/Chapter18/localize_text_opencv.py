# Importamos las librerias necesarias:
from pyimagesearch.east.east import EAST_OUTPUT_LAYERS
from pyimagesearch.east.east import decode_predictions
import numpy as np
import argparse
import time
import cv2

# Construimos los parser necesarios:
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True,
                help="Path de la imagen a usar")
ap.add_argument("-e", "--east", required=True,
                help="Path para el input EAST text detector")
ap.add_argument("-w", "--width", type=int, default=320,
                help="Redimensionamiento del ancho (Debe ser multiplo de 32)")
ap.add_argument("-t", "--height", type=int, default=320,
                help="Redimensionamiento del alto (Debe ser multiplo de 32)")
ap.add_argument("-c", "--min-conf", type=float, default=0.5,
                help="Probabilidad minima requerida al inspeccionar una region de texto")
ap.add_argument("-n", "--nms-thresh", type=float, default=0.4,
                help="Non-maximum suppression threshold")
args = vars(ap.parse_args())

# Cargamos la imagen y tomamos sus dimensiones:
image = cv2.imread(args["image"])
(origH, origW) = image.shape[:2]

# Establecemos el nuevo ancho y alto y luego determinamos la proporcion
# de cambio para el ancho y alto:
(newW, newH) = (args["width"], args["height"])
rW = origW / float(newW)
rH = origH / float(newH)

# Cargamos nuestro pre-entrenado detector de texto EAST.
print("[INFO] Cargando el detector de texto EAST... ")
net = cv2.dnn.readNet(args["east"])

# construct a blob from the image and then perform a forward pass of
# the model to obtain the two output layer sets
blob = cv2.dnn.blobFromImage(image, 1.0, (newW, newH), (123.68, 116.78, 103.94), swapRB=True, crop=False)
start = time.time()
net.setInput(blob)
(scores, geometry) = net.forward(EAST_OUTPUT_LAYERS)
end = time.time()

# show timing information on text prediction
print("[INFO] La deteccion de texto tomó {:.6f} segundos".format(end - start))

# decode the predictions form OpenCV's EAST text detector and then
# apply non-maximum suppression (NMS) to the rotated bounding boxes
(rects, confidences) = decode_predictions(scores, geometry, minConf=args["min_conf"])
idxs = cv2.dnn.NMSBoxesRotated(rects, confidences, args["min_conf"], args["nms_thresh"])

# ensure that at least one text bounding box was found
if len(idxs) > 0:
    # loop over the valid bounding box indexes after applying NMS
    for i in idxs.flatten():

        # compute the four corners of the bounding box, scale the
        # coordinates based on the respective ratios, and then
        # convert the box to an integer NumPy array
        box = cv2.boxPoints(rects[i])
        box[:, 0] *= rW
        box[:, 1] *= rH
        box = np.int0(box)

        # draw a rotated bounding box around the text
        cv2.polylines(image, [box], True, (0, 255, 0), 2)

# show the output image
cv2.imshow("Text Detection", image)
cv2.waitKey(0)

# py localize_text_opencv.py --east ../models/east/frozen_east_text_detection.pb --image images/car_wash.png
# py localize_text_opencv.py --east ../models/east/frozen_east_text_detection.pb --image images/car_wash(1).png
# py localize_text_opencv.py --east ../models/east/frozen_east_text_detection.pb --image images/sign.jpg
# py localize_text_opencv.py --east ../models/east/frozen_east_text_detection.pb --image images/store_front.jpg