Documentacion sencilla de los capitulos:

CAPITULO 1:
CAPITULO 2:
CAPITULO 3:
CAPITULO 4:
CAPITULO 5:
CAPITULO 6:
CAPITULO 7:

===========================================================================
CAPITULO 8:
    Leer sobre el Output Class de Tesseract
    Buscar por que se da el error
        |
        -- > Warning. Invalid resolution 0 dpi. Using 70 instead. Too few characters. Skipping this page Error during processing.

===========================================================================
CAPITULO 9:
    Textblob: Paquete para ayudarnos a traducir lo que leemos.
    Comando para descargar el Toolkit de Natural Lenguage de Textblob: python -m textblob.download_corpora (NO FUNCIONA CON POETRY ADD)
    translate_test.py --> NO SIRVE (POR AHORA)
    ocr_translate.py --> NO SIRVE (POR AHORA)

===========================================================================
CAPITULO 10:
    Como manejar lenguajes con Tesseract --> git clone https://github.com/tesseract-ocr/tessdata
    Basicamente no lei nada de este Capitulo.
    Leer y buscar sobre opciones para traducir.

===========================================================================
CAPITULO 11: Mejorando resultados con las opciones de Tesseract (PSM)
    Como obtener ayuda e informacion sobre psm.
        --> tesseract --help-psm
            Page segmentation modes:
            0    Orientation and script detection (OSD) only.
            1    Automatic page segmentation with OSD.       
            2    Automatic page segmentation, but no OSD, or OCR. (not implemented)
            3    Fully automatic page segmentation, but no OSD. (Default)
            4    Assume a single column of text of variable sizes.
            5    Assume a single uniform block of vertically aligned text.
            6    Assume a single uniform block of text.
            7    Treat the image as a single text line.
            8    Treat the image as a single word.
            9    Treat the image as a single word in a circle.
            10    Treat the image as a single character.
            11    Sparse text. Find as much text as possible in no particular order.
            12    Sparse text with OSD.
            13    Raw line. Treat the image as a single text line,
                bypassing hacks that are Tesseract-specific.

    (PSM 0) Orientation and script detection (OSD) only.
    Como saber u obtener la informacion de una imagen con Tesseract?
        --> tesseract normal.png stdout --psm 0 
        Obtenemos:
            Page number: 0
            Orientation in degrees: 0
            Rotate: 0
            Orientation confidence: 6.48
            Script: Latin
            Script confidence: 2.78

        --> tesseract rotated_90_clockwise.png stdout --psm 0
        Obtenemos:
            Page number: 0
            Orientation in degrees: 90
            Rotate: 270
            Orientation confidence: 4.86
            Script: Latin
            Script confidence: 0.99

        --> tesseract chinese_image.png stdout --psm 0 
        Obtenemos:
            Page number: 0
            Orientation in degrees: 0
            Rotate: 0
            Orientation confidence: 2.85
            Script: Han
            Script confidence: 1.43

    (PSM 1) Automatic page segmentation with OSD. 
        --> tesseract normal.png stdout --psm 1 
        Obtenemos:
            In the first part of this tutorial, we'll discuss how
            autoencoders can be used used for image retrieval
            and building image search engines.

    (PSM 2) Automatic Page Segmentation, But No OSD, or OCR (Por ahora NO vale la pena)
    Dentro de este metodo, Tesseract va a:
        1) Intentar segmentar automaticamente el texto, tratandolo como una "pagina" de texto
        con multiples palabras, multiples lineas y multiples parrafos, etc.
        2) Despues de la segmentacion, Tesseract aplicará OCR y devolverá el texto.

    Como sea, Tesseract no va a realzizar una deteccion de orientacion. Para realizar eso,
    debemos utilizar dos scripts, --psm 0 y --psm 3 en este caso.
        1) tesseract example.png stdout --psm 0
        2) tesseract example.png stdout --psm 3

    De solo desear el texto y no la informacion de la orientacion, se aplica solo el segundo script.

    (PSM 4). Assume a Single Column of Text of Variable Sizes.
    Ejemplo para usar --psm 4: Una imagen que contenga tablas, un recibo, factura, o un spreadsheet.

    --> tesseract receipt.png stdout
        Leyendolo con el --psm default que es el --psm 3, no lo lee de la mejor manera posible.

    --> tesseract receipt.png stdout --psm 4
        Leyendolo con el --psm 4, el resultado es notablemente mejor.

    (PSM 5). Assume a Single Uniform Block of Vertically Aligned Text
    Este método se puede aplicar de la misma manera que --psm 4, pero en imagenes verticales
    o rotadas.
    El ejemplo siguiente es el recibo anterior pero rotado 90°:
    --> tesseract receipt_rotated.png stdout --psm 5
        Los resultados son incluso mejores en este caso que con --psm 4.

    (PSM 6). Assume a Single Uniform Block of Text.
    Funciona bien para libros "simples", debido a que usan la misma fuente y tamaño en todo el libro.
    La clave acá es "TEXTO UNFORME", lo cual significa que la fuente del texto no tendrá grandes variaciones
    o ninguna variacion.
    Acá se muestra la lectura de una pagina de una novela de Sherlock Holmes:
    --> tesseract sherlock_holmes.png stdout --psm 6

    (PSM 7). Treat the Image as a Single Text Line.
    Se debe usar cuando hay una sola linea de texto uniforme. Por ejemplo: Matriculas de autos.
    --> tesseract license_plate.png stdout --psm 7

        NOTA: En este caso, ambos me funcionaron, tanto el default --psm 3, como --psm 7.

    (PSM 8). Treat the Image as a Single Word.
    Igualmente como el anterior, si tenemos una sola palabra de texto uniforme se usa este método.
    Un tipico caso de uso sería:
        1) Aplicar deteccion de texto a una imagen.
        2) Looping alrededor de todas las Regiones de Interes. (ROI)
        3) Extraerlas.
        4) Pasar cada texto de las regiones de interes por Tesseract para aplicar OCR.

    Para leer el logo de designer:
    --> tesseract designer.png stdout --psm 8

        NOTA: Tanto --psm 7 como --psm 8 funcionan relativamente parecido, ambos pueden llegar a funcionaron
        para los mismos casos.

    (PSM 9). Treat the Image as a Single Word in a Circle.
    Sin demasiada documentacion alrededor de este método, la recomendacion es no usarla.
    Pues no queda claro si la imagen debe estar dentro de un circulo.
    O quizas el texto deba formar un arco o circulo.

    La recomendacion es NO USARLO.

    (PSM 10). Treat the Image as a Single Character.
    Tratar con un simple elemento, caracter o letra, esto debe realizarse una vez ya se extrajo un elemento
    para ser procesado.

    Un ejemplo es que se localiza un elemento de una placa vehicular y se le desea aplicar OCR a ese caracter unico.
    --> tesseract number.png stdout --psm 10

    (PSM 11). Sparse Text: Find as Much Text as Possible in No Particular Order.
    Se usa cuando se desea obtener el texto sin importar lo esparcido que esté.

    Un use case, puede ser si estas realizando un "Information Retrieval", aplicas OCR al texto y puedes encontrar
    todo el texto posible en un dataset de imagenes y asi construir un "text-based search engine" mediante
    "Retrieval algorithms (tf-idf, inverted indexes, etc.)."

    Ejemplo de la seccion "Get Started" de la pagina de PyImageSearch:
    --> tesseract website_menu.png stdout
        Al aplicar el estandar o default --psm 3, se detectan lineas incorrectamente. Espaciados que no corresponden.

    -- > tesseract website_menu.png stdout --psm 11
        En este caso, los espaciados se leen de mejor manera.

    (PSM 12). Sparse Text with OSD.
    En escencia funciona de la misma manera que --psm 11.
    Se supone que muestra la informacio del texto, pero es mejor si se quiere realizar ese proceso:
        1) Usar --psm 0, para obtener los datos.
        2) Usar --psm 11, para realizar el OCR.

    (PSM 13). Raw Line: Treat the Image as a Single Text Line, Bypassing Hacks That Are Tesseract-Specific.
    Hay casos donde OSD, segmentacion y otras tecnicas de pre-procesado no van a dejar funcionar adecuadamente
    el desempeño del OCR, ya sea por:
        1) Reducir su efectividad.
        2) Ningun texto detectado.

    Esto puede llegar a suceder si el texto en la imagen está muy cerca al crop realizado, si el texto está
    generado/estilizado por computadora. Cuando esto ocurra, se considera el uso de --psm 13
    como un "ultimo recurso".

    Como ejemplo, usaremos un texto, "The Old Engine"
    --> tesseract the_old_engine.p ng stdout --psm 13

    RECOMENDACIONES FINALES:
        * Aplicar el default, --psm 3
        * En caso de no funcionar bien, aplicar --psm 13 (Funciona muy bien sobre todo cuando se pre-procesa la imagen)
        * Si --psm 13 no funciona correctamente, aplicar modos 4-8
        * Como siguiente paso, aplicar --psm 0 para verificar si la rotacion de la imagen está siendo identificada
        correctamente, pues esto afecta la lectura de la imagen.
        * Teniendo el script necesario y si el angulo es detectado correctamente, se siguen los lineamientos
        dados en este capitulo.
            NOTA: EVITAR MODOS 1, 2, 9 y 12. (Al menos que sea un uso especifico para el caso en cuestion).
        * Como paso final, de cualquier manera se pueden probar los modos 1-13, es decir, todos.
        Para comprobar el funcionamiento de cada uno de los modos con la imagen a leer.

===========================================================================
CAPITULO 12: Pre-procesando imaganes antes de aplicarles OCR.

    BUSCAR:
        Metodos de OpenCV:
            cv2.threshold
                cv2.THRESH_BINARY_INV
                cv2.THRESH_OTSU
                cv2.THRESH_BINARY
            cv2.distanceTransform
                cv2.DIST_L2
            cv2.normalize
                cv2.NORM_MINMAX
            cv2.getStructuringElement
                cv2.MORPH_ELLIPSE
            cv2.morphologyEx
                cv2.MORPH_OPEN
            cv2.findContours
                cv2.RTR_EXTERNAL
                cv2.CHAIN_APROX_SIMPLE
            cv2.boundingRect
            cv2.convexHull
            cv2.drawContours
            cv2.dilate
            cv2.bitwise_and

        Método Imutils:
            imutils.grab_contours

        Método Numpy:
            np.zeros

        Metodo Python:
            .astype

        NOTA: El código me funcionó cuando intervine en el valor de iteracion del mask.
        Cambié el valor de iteración de 2 hasta 60.

===========================================================================
CAPITULO 13: Corrigiendo el spell de las oraciones con TexBlob.

    BUSCAR:
        Métodos Textblob:
            tb.correct()

    Se debe tener cuidado con el spellchecking, siempre es bueno estar atento a los resultados.

    Si los resultados no son los deseados:
        * Buscar otra libreria que ayude con el spellchecking ademas de TextBlob.
        * Reemplazar el spellcheking con metodos "heuristic-based" (Ejemplo: Regular Expression Matching).
        * Permitir que exista el misspelling en algunas ocasiones, pues el OCR no es 100% efectivo.

===========================================================================
CAPITULO 14: Encontrando Text Blobs en una imagen con OpenCV.

    BUSCAR:
        Métodos OpenCV:
            cv2.MORPH_RECT
            cv2.GaussianBlur
            cv2.MORPH_BLACKHAT
            cv2.Sobel
            cv2.CV_32F
            cv2.MORPH_CLOSE
            cv2.erode

        Método
            sort_contours

    NOTA:
        El codigo funciona de buena manera con el passport_01.png, pero con passport_02.png no.

===========================================================================
CAPITULO 15: OCR usando Template Matching.

    En este capitulo nos presentan varios tipos de caracteres especiales para OCR:
    OCR-A, OCR-B, MICR E-13B.

    BUSCAR:
        Métodos OpenCV:
            cv2.MORPH_TOPHAT
            cv2.matchTemplate
            cv2.TM_CCOEFF
            cv2.minMaxLoc
            cv2.rectangle
            cv2.putText
            cv2.FONT_HERSHEY_SIMPLEX

        Métodos Numpy:
            np.absolute
            np.abs
            np.argmax
            np.min
            np.max

        Métodos Python:
            Lambda

    NOTA: Por ahora, sin hacer nigun cambio, el codigo no me funcionó.

===========================================================================
CAPITULO 16: Aplicando OCR con Procesamiento de Imagenes Basico.

    NO FUNCIONA.

===========================================================================
CAPITULO 17: Localizar cuadro delimitador de texto y aplicar OCR con Tesseract.

    Como realizar una buena lectura si no puedes controlar la iluminacion de la imagen?

    Con helpers.py usaremos una  funcion que llamaermos cleanup_text, esta nos va a ayudar a dividir
    caracteres no ASCII y a la vez escribir eso en la imagen.

    NOTA:   FUNCIONA RELATIVAMENTE BIEN CON LA IMAGEN DE APPLE.
            El único error visible es que lee la manzana con una confiablidad alta.
            Cosa que no comprendo por qué.

===========================================================================
CAPITULO 18: Localizacion de Cajas de Texto rotadas. (Con OpenCV).

    Desafíos a la hora de leer una imagen:
        - Image/sensor noise.
        - Viewing Angles.
        - Blurring
        - Lighting Conditions.
        - Resolution.
        - Non-paper objects.
        - Non-planar objects.
        - Unknown layout.

    BUSCAR:
        EAST text detector: An Efficient and Accurate Scene Text Detector

    Nuestro directorio "images" contiene varias imágenes de muestra a las que aplicaremos el detector de texto EAST de OpenCV.
    El directorio "models" contiene la definición real de la arquitectura EAST y los pesos del modelo.
    Cargaremos este modelo desde el disco usando OpenCV y luego lo utilizaremos para la detección de texto.

    El submódulo "east" de pyimageserach contiene east.py.
    Este archivo almacena la función auxiliar decode_predictions, que acepta la salida del modelo EAST,
    la analiza y devuelve una tupla de 2 que contiene las ubicaciones del cuadro delimitador del texto
    en nuestra imagen de entrada, junto con las probabilidades asociadas con cada detección.

    Finalmente, "localize_text_opencv.py" actúa como un script de controlador,
    uniendo todas las piezas y permitiéndonos detectar texto en nuestras imágenes.

        Métodos Numpy:
            np.cos
            np.sin
            np.pi
            np.int0

        Métodos OpenCV:
            cv2.dnn.readNet
            cv2.dnn.blobFromImage
            cv2.dnn.NMSBoxesRotated
            cv2.boxPoints
            cv2.polylines

        Métodos Python:
            .setInput
            .forward
            .flatten

===========================================================================
CAPITULO 19: Pipeline Completo de Deteccion de Texto y aplicacion de OCR.

    5 Pasos para nuestro Pipeline de deteccion de texto y aplicacion de OCR:
        1) Leemos la imagen cargada.
        2) Aplicamos la deteccion de texto EAST para localizar el texto en la imagen.
        3) Hacemos un loop sobre cada ROI que contenga texto y lo extraemos.
        4) Tomamos el texto de la ROI y lo pasamos a través de Tesseract para aplicar OCR.
        5) Mostramos la deteccion final del texto y los resultados del OCR.
    
    Todo el proceso puede observarse en la imagen "OCR_Pipeline.png"