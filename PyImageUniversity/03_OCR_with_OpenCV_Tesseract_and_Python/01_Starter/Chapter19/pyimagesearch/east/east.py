# Importamos los paquetes necesarios.
import numpy as np

# Definimos los dos nombres de las etiquetas para el modelo detector de EAST
#   1) El primero son las probabilidades (Sigmoid Activation):
#      Esto nos da las probabilidades de saber si hay texto o no.
#   2) El segundo puede usarse para derivar la caja de coordenadas de texto.
#      Esto mapea la "geometria" de la imagen, con esto seremos capaces de derivar.
EAST_OUTPUT_LAYERS = [
    "feature_fusion/Conv_7/Sigmoid",
    "feature_fusion/concat_3"
]

# Definimos la funcion decode_predictions
#   scores: Contiene la probabilidad de que una region en especifico posea texto.
#   geometry: Mapa usado para derivar las coordenadas del bounding box.
#   minConf: Confiabilidad usada para filtrar detecciones de texto.
def decode_predictions(scores, geometry, minConf=0.5):
    # Tomamos el numero de filas y columnas del "volumen de score", luego inicializamos
    # nuestro set de rectangulos de bounding box y los correspondientes scores de confiablidad.
    (numRows, numCols) = scores.shape[2:4]

    # rects: Guarda las coordenadas (x, y) del bounding box, para las regiones con texto.
    rects = []
    # confidences: Guarda la probabilidad asociada a cada uno de los bounding boxes en los rects.
    confidences = []

    # A partir de acá, hacemos un loop sobre el numero de filas:
    for y in range(0, numRows):
        # Extraemos los scores(probabilidades), seguido por la data geométrica usada
        # para derivar potenciales coordenadas de bounding boxes que rodean el texto.
        scoresData = scores[0, 0, y]
        xData0 = geometry[0, 0, y]
        xData1 = geometry[0, 1, y]
        xData2 = geometry[0, 2, y]
        xData3 = geometry[0, 3, y]
        anglesData = geometry[0, 4, y]

        # Hacemos loop sobre el numero de columnas:
        for x in range(0, numCols):
            # Tomamos el score de confiablidad para la deteccion de texto actual:
            score = float(scoresData[x])

            #  Si nuestro score no tiene "suficiente probabilidad" lo ignoramos:
            if score < minConf:
                continue

            # Ahora, calculamos el factor offset, ya que nuestros "resulting feature map"
            # serán 4 veces menor que la imagen inputeada.
            (offsetX, offsetY) = (x * 4.0, y * 4.0)

            # Extraemos el angulo de rotacion para la prediccion y luego calculamos seno y coseno.
            angle = anglesData[x]
            cos = np.cos(angle)
            sin = np.sin(angle)

            # Usamos el volumen de geometria para derivar el ancho y alto del bounding box.
            h = xData0[x] + xData2[x]
            w = xData1[x] + xData3[x]

            # Usamos el offset y el angulo de rotacion para calcular el bounding box rotado:
            offset = ([
                offsetX + (cos * xData1[x]) + (sin * xData2[x]),
                offsetY - (sin * xData1[x]) + (cos * xData2[x])
            ])

            # Luego, derivamos la esquina superior derecha y la esquina inferior derecha de nuestro
            # bounding box rotado.
            topLeft = ((-sin * h) + offset[0], (-cos * h) + offset[1])
            topRight = ((-cos * w) + offset[0], (sin * w) + offset[1])

            # Calculamos las coordenadas del centro (x, y) de nuestro bounding box rotado:
            cX = 0.5 * (topLeft[0] + topRight[0])
            cY = 0.5 * (topLeft[1] + topRight[1])

            # Entonces, la informacion que tenemos del bounding box rotado consiste en:
            # 1) Las coordenadas del centro (x, y) de la caja.
            # 2) El ancho y alto de la caja.
            # 3) El angulo de rotacion.
            box = ((cX, cY), (w, h), -1 * angle * 180.0 / np.pi)

            # Ahora, actualizamos nuestra lista de confiabilidad de detecciones.
            rects.append(box)
            confidences.append(score)

    # Retornamos una tupla, que contiene los bounding boxes y su confiablilidad asociada:
    return (rects, confidences)