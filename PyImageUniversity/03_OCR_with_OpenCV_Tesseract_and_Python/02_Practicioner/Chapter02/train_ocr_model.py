# Importamos matplotlib y establecemos el backend de matplotlib,
# asi podemos guardar figuras en el backgorund(?)
import matplotlib
matplotlib.use("Agg")

from pyimagesearch.models.resnet import ResNet
from pyimagesearch.az_dataset.helpers import load_az_dataset, load_mnist_dataset
from tensorflow import keras
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import SGD
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from imutils import build_montages
import matplotlib.pyplot as plt
import numpy as np
import argparse
import cv2

# Construimos el parser:
ap = argparse.ArgumentParser()
ap.add_argument("-a", "--az", required=True,
                help="El path del A-Z dataset.")
ap.add_argument("-m", "--model", type=str, required=True,
                help="El path para mostrarel modelo entrenado de reconocimiento de handwriting.")
ap.add_argument("-p", "--plot", type=str, default="plot.png",
                help="El path para mostrar el training history file.")
args = vars(ap.parse_args())

# Inicializamos el numero de epochs que vamos a entrenar,
# el valor inicial de learning rate y el batch size.
EPOCHS = 50
INIT_LR = 1e-1
BS = 128

# Cargamos el A-Z dataset y el MNIST dataset, respectivamente.
print("[INFO] Cargando datasets...")
(azData, azLabels) = load_az_dataset(args["az"])
(digitsData, digitsLabels) = load_mnist_dataset()

# El dataset MNIST ocupa los labels desde el 0 hasta el 9, asi que vamos a añadir
# 10 a cada label de A-Z, asi nos aseguramos que los caracteres A-Z
# no estén siendo labeled incorrectamente como digitos.
azLabels += 10

# Apilamos la data y labels de A-Z con los data digits y labels de MNSIT.
data = np.vstack([azData, digitsData])
labels = np.hstack([azLabels, digitsLabels])

# Cada imagen en los datasets A-Z t MNIST son 28x28 pixeles;
# sin embargo, la arquitectura que estamos usando está diseñada para imagenes 32x32,
# por lo tanto, debemos hacerles un resize a 32x32.
data = [cv2.resize(image, (32, 32)) for image in data]
data = np.array(data, dtype="float32")

# Ahora, añadimos un channel dimension a cada imagen en el dataset y escalamos
# los pixeles de la imagen desde [0, 255] hasta [0, 1].
data = np.expand_dims(data, axis=-1)
data /= 255.0

# Convertimos los labels, de integers a vectores.
le = LabelBinarizer()
labels = le.fit_transform(labels)
counts = labels.sum(axis=0)

# Tomamos en cuenta el sesgo en la data labeled.
classTotals = labels.sum(axis=0)
classWeight = {}

# loop over all classes and calculate the class weight
for i in range(0, len(classTotals)):
    classWeight[i] = classTotals.max() / classTotals[i]

# partition the data into training and testing splits using 80% of
# the data for training and the remaining 20% for testing
(trainX, testX, trainY, testY) = train_test_split(data, labels, test_size=0.20, stratify=labels, random_state=42)

# construct the image generator for data augmentation
aug = ImageDataGenerator(rotation_range=10, zoom_range=0.05, width_shift_range=0.1, height_shift_range=0.1,
                        shear_range=0.15, horizontal_flip=False, fill_mode="nearest")

# initialize and compile our deep neural network
print("[INFO] compiling model...")

opt = SGD(lr=INIT_LR, decay=INIT_LR / EPOCHS)
model = ResNet.build(32, 32, 1, len(le.classes_), (3, 3, 3), (64, 64, 128, 256), reg=0.0005)
model.compile(loss="categorical_crossentropy", optimizer=opt, metrics=["accuracy"])

# train the network
print("[INFO] training network...")
H = model.fit(aug.flow(trainX, trainY, batch_size=BS),
                validation_data=(testX, testY),
                steps_per_epoch=len(trainX) // BS,
                epochs=EPOCHS,
                class_weight=classWeight,
                verbose=1)

# define the list of label names
labelNames = "0123456789"
labelNames += "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
labelNames = [l for l in labelNames]

# evaluate the network
print("[INFO] evaluating network...")
predictions = model.predict(testX, batch_size=BS)
print(classification_report(testY.argmax(axis=1), predictions.argmax(axis=1), target_names=labelNames))

# save the model to disk
print("[INFO] serializing network...")
model.save(args["model"], save_format="h5")

# construct a plot that plots and saves the training history
N = np.arange(0, EPOCHS)
plt.style.use("ggplot")
plt.figure()
plt.plot(N, H.history["loss"], label="train_loss")
plt.plot(N, H.history["val_loss"], label="val_loss")
plt.title("Training Loss and Accuracy")
plt.xlabel("Epoch #")
plt.ylabel("Loss/Accuracy")
plt.legend(loc="lower left")
plt.savefig(args["plot"])

# initialize our list of output images
images = []

# randomly select a few testing characters
for i in np.random.choice(np.arange(0, len(testY)), size=(49,)):
    # classify the character
    probs = model.predict(testX[np.newaxis, i])
    prediction = probs.argmax(axis=1)
    label = labelNames[prediction[0]]

    # extract the image from the test data and initialize the text
    # label color as green (correct)
    image = (testX[i] * 255).astype("uint8")
    color = (0, 255, 0)

    # otherwise, the class label prediction is incorrect
    if prediction[0] != np.argmax(testY[i]):
        color = (0, 0, 255)

    # merge the channels into one image, resize the image from 32x32
    # to 96x96 so we can better see it and then draw the predicted
    # label on the image
    image = cv2.merge([image] * 3)
    image = cv2.resize(image, (96, 96), interpolation=cv2.INTER_LINEAR)
    cv2.putText(image, label, (5, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.75, color, 2)

    # add the image to our list of output images
    images.append(image)

# construct the montage for the images
montage = build_montages(images, (96, 96), (7, 7))[0]

# show the output montage
cv2.imshow("OCR Results", montage)
cv2.waitKey(0)