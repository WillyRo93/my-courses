# Importamos los paquetes necesarios:
from tensorflow import keras
from keras.datasets import mnist
import numpy as np
import argparse

def load_az_dataset(datasetPath):
    # Inicializamos una lista de labels y de data:
    data = []
    labels = []

    # Hacemos un loop sobre las filas del "A-Z handwritting digit dataset"
    for row in open(datasetPath):
        # Hacemos el parse del label y de la imagen:
        row = row.split(",")
        label= int(row[0])
        image = np.array([int(x) for x in row[1:]], dtype="uint8")

        # Las imagenes, están representadas en un solo canal (grayscale), estas imagenes son 28x28=784 pixeles
        # Vamos a convertir ese valor "chato" ó "flattened" y lo convertiremos en una matriz 28x28.
        image = image.reshape((28, 28))

        # Actualizamos la lista de labels y de data:
        data.append(image)
        labels.append(label)

        # Convertimos la data y los labels a arreglos Numpy:
        data = np.array(data, dtype="float32")
        labels = np.array(labels, dtype="int")

        # Retornamos una 2-tuple de data A-Z y labels:
        return(data, labels)

# Función para cargar el dataset mnist:
def load_mnist_dataset():
    # Cargamos el dataset Mnist y apilamos los datos de entrenamiento y testeo juntos
    # (Vamos a crear nuestros propios splits de entrenamiento y testeo mas adelante).
    ((trainData, trainLabels), (testData, testLabels)) = mnist.load_data()
    data = np.vstack([trainData, testData])
    labels = np.hstack([trainLabels, testLabels])

    # Retornamos una 2-tuple de data y labels de MNIST:
    return(data, labels)