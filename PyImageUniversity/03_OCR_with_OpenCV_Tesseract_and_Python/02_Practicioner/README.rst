Como debemos crear nuestro proyecto?
    1) poetry new 02_Practicioner
    2) poetry add tensorflow
    3) poetry add numpy
    4) poetry add imutils
    5) poetry add matplotlib
    6) poetry add scikit-learn
    ?) poetry add opencv-contrib-python
    8) poetry add progressbar
    9) poetry add opencv-python
    10) poetry add pyyaml
    11) poetry add easyocr
    12) poetry add pytesseract
    ?) poetry add keras (?)


===========================================================================
GitHub/GitLab:
    https://github.com/youngsoul/pyimagesearch-ocr-handwriting-reco
    https://github.com/small-ming/-/blob/master/myutils.py
===========================================================================

CAPITULO 1: Introduccion.
    Que aprenderemos en este libro?
        - Entrenar un modelo de machine learning para denoise documentos. Mejorando asi los resultados.
        - Llevar a cabo una alineacion y registro automático de imagenes/documentos.
        - Construir un proyecto que use esta alineacion para aplicar OCR a un documento.
        - Resolver un Sudoku con OCR.
        - Escanear y aplicar OCR a recibos/facturas.
        - Construir un sistema de reconocimiento automático de numeros de licencias.
        - Aplicar OCR en tiempo real en videos.
        - Reconocer el texto escrito a mano.
        - Incrementar la velocidad del OCR leveraging el GPU.
        - Entrenar modelos de Tesseract en nuestros datasets.

===========================================================================

CAPITULO 2: Entrenando un modelo de OCR con Keras y TensorFlow
    Que aprenderemos en este capitulo?
        - Familiarizarnos con datasets que contienen handwriting (para digitos y letras).
        - Entender como entrenar modelos de reconocimiento de digitos y letras escritos/as a mano.
        - Ganar experiencia en aplicar custom-trained models para data real.
        - Entender algunos retos al encontrarnos con imagenes reales y sus complicaciones.

    Descargas:
        a_z_handwritten_data.csv.zip:
            https://www.kaggle.com/code/kerneler/starter-a-z-handwritten-dataset-a0fcc351-3/data

    OCR con Keras y TenserFlow:
        1) Datasets (Digitos y Letras)
            - MNIST 0-9
            - Kaggle A-Z

            * Nuestro primer proyecto implementará el siuiente método:
                1.- Cargamos ambos datasets (MNIST 0-9 y Kaggle A-Z)
                2.- Combinamos estos Datasets en uno solo. 
                3.- Handle class label skew/imbalance from having a different number of samples per character (?).
                4.- Entrenamos satisfactoriamente un modelo Keras y TenserFlow en el dataset combinado.
                5.- Ploteamos los resultados del entrenamiento y observamos la data.

        2) Estructura del proyecto.
            |-- pyimagesearch
            | |-- __init__.py
            | |-- az_dataset
            | | |-- __init__.py
            | | |-- helpers.py
            | |-- models
            | | |-- __init__.py
            | | |-- resnet.py
            |-- a_z_handwritten_data.zip
            |-- a_z_handwritten_data.csv
            |-- handwriting.model
            |-- plot.png
            |-- train_ocr_model.py

                pyimagesearch:
                    Incluye los submodelos
                        * "az_dataset" para input/output los helper files.
                        * "models" para implementar el RestNet deep learning architecture.

                a_z_handwritten_data.csv:
                    Incluye el dataset Kaggle A-Z (se necesita extraer el .zip para obtener el archivo)

                handwriting.model:
                    Donde guardamos el modelo RestNet.

                plot.png:
                    Plotea los resultados mas recientes de nuestro entrenamiento RestNet.

                train_ocr_model.py:
                    Nuestro archivo principal donde entrenaremos nuestro modelo RestNet y mostraremos los resultados.

        3) Implementando nuestras Dataset Loader Functions.

            helpers.py:
                - Importamos los paquetes:
                    * from tensorflow.keras.datasets import mnist
                    * import numpy as np

                - Definimos la funcion "load_az_dataset":
                    * Esta funcion "helper" nos ayudará a cargar el dataset Kaglle A-Z.
                    * Nuestra funcion "load_az_dataset" toma una sola variable de entrada: "datasetPath", el cual es
                        el path hacia nuestro archivo .csv de Kaggle.
                    * Inicializamos nuestros arreglos para guardar los labels y la data.
                        NOTA: Cada fila en el archivo .csv de Sachin Patel contiene 785 columnas, una columna para los labels (ejemplo: A-Z)
                        y se le suman 784 columnas correspondientes a los pixeles grayscale (28x28).
                    * Hacemos un loop sobre nuestro .csv y lo parseamos para obtener el label y la imagen asociada.
                    * Tomamos el valor integer de la lista--> label=int(row[0]), porque sabemos que A es el integer 0, y Z es el integer 25.
                    * Con image=np.array([int(x)])... parseamos nuestra imagen como un arreglo Numpy de 8-bit integers, estos valores
                        corresponden a cada valor grayscale pixel desde [0, 255].
                    * Con image.reshape(28,28) es que convertimos nuestro arreglo de 784 valores a un arreglo 28x28.
                    * Luego guardamos cada imagen y label en sus respectivos arreglos.
                    * Para finalizar convertimos la data y labels en arreglos Numpy y retornamos data y labels.
                        - Data se convierte en un arreglo float32.
                        - Labels en un arreglo int.

                - Definimos ahora, la funcion "load_mnist_dataset"
                    * Con la funcion helper de Keras "mnist.load_data", cargamos nuestra data 0-9 de MNIST.
                        NOTA: No se especifica el "datasetPath" en esta función como si se hizo en la data de Kaggle porque convenientemente
                        Keras tiene el dataset built-in.
                    * La funcion "mnist.load_data" de Keras, viene por default con un split training data, asi que usaremos:
                        - np.vstack para nuestra image data.
                        - np.hstack para nuestros labels.

        4) Implementando nuestro Script de Entrenamiento:
            train_ocr_model.py:
                Aquí, vamos a combinar nuestro MNIST 0-9 digit data con nuestro Kaggle A-Z letters.
                En este punto, vamos a crear nuestro propio split de testeo y training data.
                
                En esta seccion, vamos a entrenar nuestro modelo OCR usando Keras, TensorFlow y una implementacion de PyImageSerach
                de la popular arquitectura de ResNet.

                - Importamos matplotlib y establecemos el backend de matplotlib, asi podemos guardar figuras en el backgorund(?)
                    * import matplotlib
                    * matplotlib.use("Agg")
                
                - Importamos todas las demas librerias que necesitamos:
                    from pyimagesearch.models import ResNet
                    from pyimagesearch.az_dataset import load_mnist_dataset
                    from pyimagesearch.az_dataset import load_az_dataset
                    from tensorflow.keras.preprocessing.image import ImageDataGenerator
                    from tensorflow.keras.optimizers import SGD
                    from sklearn.preprocessing import LabelBinarizer
                    from sklearn.model_selection import train_test_split
                    from sklearn.metrics import classification_report
                    from imutils import build_montages
                    import matplotlib.pyplot as plt
                    import numpy as np
                    import argparse
                    import cv2

                        Explicaciones de las librerias:
                        + ResNet de pyimagesearch.model: contiene nuestra implementacion customizada de ResNet.
                        + load_mnist_dataset, load_az_dataset: Son nuestras helper functions.
                        + ImageDataGenerator: Nos ayuda a aumentar eficientemente nuestro dataset (?).
                        + SGD: Stochastic Gradient Descent (SGD) optimization algorithm.
                        + LabelBinarizer: Para convertir nuestros labels de integers a un vector en lo que se llama un "one-hot encoding".
                        + train_test_split: Para ayudarnos a dividir facilmente nuestros datasets de entrenamiento y testeo.
                        + classification_report: Para el submodulo metrics, para imprimir un classification report con un buen formato.
                        + build_montages: Para ayudarnos a construir un "montage" desde una lista de imagenes.

                Luego, como siempre creamos nuestros parsers para dar los valores de entrada
                que querramos. En este caso, queremos los siguientes valores de entrada:
                    - "-a": El path del A-Z dataset.
                    - "-m": El path para mostrar el modelo entrenado de reconocimiento de handwriting.
                    - "-p": El path para mostrar el training history file.

                Desde este punto, vamos a hacer un set up de nuestros parametros de entrenamiento para ResNet 
                y cargar nuestra data numerica y escrita usando las helper functions (load_az_dataset, load_mnist_dataset).
                    - EPOCHS
                    - INIT_LR
                    - BS
                    - azData
                    - azLabels
                    - digitsData
                    - digitsLabels

                En los siguientes pasos, vamos a preparar nuestra data y labels para que sean
                compatibles con nuestro modelo ResNet en Keras y TenserFlow.

                Mientras combinamos nuestras letras y numeros en un mismo dataset de caracteres, queremos remover
                cualquier ambiguedad donde haya un overlap en los labels tal que cada label en el set de caracteres
                combinado sea único:

                    + En este punto, nuestros labels para A-Z van desde [0, 25], correspondientes a cada letra del alfabeto.
                    Los labels para nuestros digitos van de 0-9, asi que existe un overlap, este overlap sería problematico
                    si los combinamos directamente.
                    + Para corregir lo dicho previamente, añadimos 10 valores a cada label A-Z, de esta manera, todos van a tener
                    valores de label integer mayores que nuestros valores de digit labels. (azLabels += 10).
                    + Ahora tenemos un esquema de labeling para digitos 0-9 y letras A-Z sin overlaps.
                    + Con np.vstack, combinamos nuestros datasets para nuestros digitos y letras en un mismo dataset de caracteres.
                    + Con np.hstack, unificamos nuestros labels correspondientes para nuestros digitos y letras.

                Nuestra arquitectura ResNet requiere que las imagenes tengan un formato 32x32, pero nuestro input es de 28x28,
                por lo tanto debemos hacer un resize. (cv2.resize)

                Tenemos aun dos pasos para preparar nuestra data para usar el ResNet:
                    + Para esto añadimos un canal de dimension extra a cada imagen del dataset para hacerlo
                    compatible con el modelo ResNet en Keras/TensorFlow.
                    + Finalmente escalamos nuestros pixeles de un rango [0, 255] hasta [0.0, 1.0]

                Nuestro proximo paso para preparar los labels para ResNet, es "ponderar" los labels para tener en cuenta
                el sesgo en la cantidad de veces que cada clase (caracter) es representado en la data, luego de esto,
                dividimos la data en prueba y entrenamiento.


    BUSCAR:
        Métodos Mnist:
            mnist.load_data

        Métodos Numpy:
            np.vstack
            np.hstack
        
        Métodos MatPlotLib:
            matplotlib.use

        Por que?
            - Por que inicializa los EPOCHS en 50?
            - Por que el learning rate lo inicia en ese valor exponencial? --> 1e-1
            - Por que inicializa el Batch Size en 128.
            - Esto exactamente que hace?
                data = np.expand_dims(data, axis=-1)
                data /= 255.0
            - 




===========================================================================

CAPITULO 3: Handwriting Recognition with Keras and TensorFlow
===========================================================================

CAPITULO 4: Using Machine Learning to Denoise Images for Better OCR Accuracy
    Descargas:
        denoising-dirty-documents:
            https://www.kaggle.com/c/denoising-dirty-documents/data
===========================================================================

===========================================================================

===========================================================================

CAPITULO 7:
    7.1.2) Pasos para leer una factura:
        - Paso 1:
        Consiste en definir las localizaciones de los campos en el documento.
        (En mi caso, puede ser con PAINT).
        - Paso 2:
        Cargamos la imagen a aplicar OCR, y le aplicamos nuestro Pipeline.
        - Paso 3:
        Aplicamos alineamiento/registro automatico de la imagen, es decir alineamos
        la imagen.
        - Paso 4:
        Hacemos un loop sobre todas las localizaciones de campos de texto, extraemos las
        ROI y aplicamos OCR a las ROI. 
        NOTA: Durante este paso podemos aplicar OCR al texto y asociarlo con el
        campo de texto en el documento que funciona como template.
        - Paso 5:
        Mostrar nuestro output con el texto ya aplicado el OCR.

    7.1.3) Estructura del Proyecto:
        .. |-- pyimagesearch
        .. | |-- alignment
        .. | | |-- __init__.py
        .. | | |-- align_images.py
        .. | |-- __init__.py
        .. | |-- helpers.py
        .. |-- scans
        .. | |-- scan_01.jpg
        .. | |-- scan_02.jpg
        .. |-- form_w4.png
        .. |-- ocr_form.py

        Que contiene cada uno de los apartados?
            - scans
                * scans/scan_01.jpg: Contiene una muestra IRS W-4 que ha sido rellenada con
                informacion falsa, incluyendo el nombre del autor del libro.
                * scans/scan_02.jpg: Una muestra IRS W-4 similar rellenada con informacion falsa.

            - form_w4.png
                El template oficial para la forma 2020 IRS W-4. Esta forma vacía no contiene ninguna
                información rellenada. Necesitamos esta forma un las localizaciones de los campos
                para alinear los escaneos y finalmente extraer la informacion de los escaneos.
                Manualmente vamos a determinar las localizaciones de los campos con un editor
                externo de imagenes. (En mi caso usé PAINT).

            - ocr_form.py
                Este parser de formas recae en dos funciones helper que se encuentran en el directorio
                "pyimagesearch".
                * align_images
                * cleanup_text
        
    7.1.4) Implementando neustro script de detection de Texto.
        Importamos los paquetes necesarios:
            from pyimagesearch.alignment import align_images
            from pyimagesearch.helpers import cleanup_text
            from collections import namedtuple
            import pytesseract
            import argparse
            import imutils
            import cv2

            La funcion "cleanup_text" es usada para quitar los caracteres no ASCII de un texto. Lo hacemos con 
            esta función, ya que cv2.putText no es capaz de escribir estos caracteres no ASCII en una imagen.
        
        Creamos nuestro Argument Parser.
            -i: --image "Path para la imagen a aplicar OCR"
            -t: --template "Path para introducir la imagen que se usará como template"

        NOTA: No se está desarrollando un lector de facturas 100% automatizado,