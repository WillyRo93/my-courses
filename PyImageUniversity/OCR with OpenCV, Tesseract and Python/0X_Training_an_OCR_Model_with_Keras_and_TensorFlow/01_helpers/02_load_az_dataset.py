from tensorflow.keras.datasets import mnist
import numpy as np

# Luego definimos load_az_dataset para cargar el Kaggle A-Z letter data:

def load_az_dataset(datasetPath):
    # Inicializamos listas de data y labels:
    data = []
    labels = []

    # Hacemos un loop en las filas del handwritten A-Z Dataset
    for row in open(datasetPath):
        # parse the label and image from the row
        row = row.split(",")
        label = int(row[0])
        image = np.array([int(x) for x in row[1:]], dtype="uint8")

        # images are represented as single channel (grayscale) images
        # that are 28x28=784 pixels -- we need to take this flattened
        # 784-d list of numbers and reshape them into a 28x28 matrix
        image = image.reshape((28, 28))

        # Hacemos un update de las listas de data y labels:
        data.append(image)
        labels.append(label)

        # convert the data and labels to NumPy arrays
        data = np.array(data, dtype="float32")
        labels = np.array(labels, dtype="int")

        # return a 2-tuple of the A-Z data and labels
        return (data, labels)