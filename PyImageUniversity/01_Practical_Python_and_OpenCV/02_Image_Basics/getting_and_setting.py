from __future__ import print_function

# -----------------------------------------------
import os

# Aqui tenemos el directorio donde están contenidas las imagenes:
os.chdir("../..")
print(os.getcwd())

# -----------------------------------------------
# A partir de acá empieza el curso:
import argparse
import cv2

# Definimos el path y todo ese peo:
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required = True,
help = "Path to the image")
args = vars(ap.parse_args())

# Leemos la imagen y la mostramos:
image = cv2.imread(args["image"])
cv2.imshow("Original", image)

# Luego jugamos con los pixeles (0,0)
(b, g, r) = image[0, 0]
print("Pixel at (0, 0) - Red: {}, Green: {}, Blue: {}".format(r, g, b))

image[0, 0] = (0, 0, 255)
(b, g, r) = image[0, 0]
print("Pixel at (0, 0) - Red: {}, Green: {}, Blue: {}".format(r, g, b))

# Tomamos con corner una procion de 100x100 pixeles de la imagen
# Para tomar ciertos chunks de una imagen necesitamos lo siguiente:
# corner = image [start y: end y, start x: end x]
corner = image[0:100, 0:100]
cv2.imshow("Corner", corner)

image[0:100, 0:100] = (0, 255, 0)

cv2.imshow("Updated", image)
cv2.waitKey(0)

# Para correr el codigo en la consola, procedemos con el siguiente script:
# getting_and_setting.py --image images\trex.png