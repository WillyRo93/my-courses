# Importamos los paquetes necesarios:
from __future__ import print_function

# -----------------------------------------------
import os

# Aqui tenemos el directorio donde están contenidas las imagenes:
os.chdir("../..")
print(os.getcwd())

# -----------------------------------------------
# A partir de acá empieza el curso:
import argparse
import cv2

# Hacemos parte del codigo para leer la entrada del path:
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required = True,
help = "Path to the image")
args = vars(ap.parse_args())

# Una vez tenemos el path, cargamos la imagen y nos retorna un numpy array
image = cv2.imread(args["image"])
print("width: {} pixels".format(image.shape[1]))
print("height: {} pixels".format(image.shape[0]))
print("channels: {}".format(image.shape[2]))

cv2.imshow("Image", image)
cv2.waitKey(0)

# Finalmente guardamos la imagen como un archivo jpg:
cv2.imwrite("newimage.jpg", image)

# Para correr el codigo en consola Windows, hago lo siguiente:
# load_display_save.py --i images\trex.png