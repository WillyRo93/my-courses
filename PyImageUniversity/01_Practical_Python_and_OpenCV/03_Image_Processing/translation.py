# -----------------------------------------------
import os

# Aqui tenemos el directorio donde están contenidas las imagenes:
os.chdir("../..")
print(os.getcwd())

# -----------------------------------------------
# A partir de acá empieza el curso:

# Importamos las librerias necesarias:
import numpy as np
import argparse
import imutils
import cv2

# Ingresamos el parse para el Path:
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required = True,
help = "Path to the image")
args = vars(ap.parse_args())

# Leemos la imagen y la mostramos<.
image = cv2.imread(args["image"])
cv2.imshow("Original", image)

# Movemos la imagen abajo y a la derecha:
M = np.float32([[1, 0, 25], [0, 1, 50]])
shifted = cv2.warpAffine(image, M, (image.shape[1], image.shape[0]))
cv2.imshow("Shifted Down and Right", shifted)
cv2.waitKey(0)

# Movemos la imagen Arriba y a la Izquierda:
M = np.float32([[1, 0, -50], [0, 1, -90]])
shifted = cv2.warpAffine(image, M, (image.shape[1], image.shape[0]))
cv2.imshow("Shifted Up and Left", shifted)
cv2.waitKey(0)

# Para correr el codigo en la consola:
# translation.py -i images\trex.png