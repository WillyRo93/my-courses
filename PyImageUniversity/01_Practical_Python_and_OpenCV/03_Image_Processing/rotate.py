# -----------------------------------------------
import os

# Aqui tenemos el directorio donde están contenidas las imagenes:
os.chdir("../..")
print(os.getcwd())

# -----------------------------------------------
# A partir de acá empieza el curso:
import numpy as np
import argparse
import imutils
import cv2

# Path:
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required = True,
help = "Path to the image")
args = vars(ap.parse_args())

# Leemos la imagen y la mostramos:
image = cv2.imread(args["image"])
cv2.imshow("Original", image)

# Tomamos el ancho y alto de la imagen:
(h, w) = image.shape[:2]
center = (w // 2, h // 2)

# Rotamos la imagen acá (45 grados):
                    #(centro, angulo, escala de la imagen)
M = cv2.getRotationMatrix2D(center, 45, 1.0)
rotated = cv2.warpAffine(image, M, (w, h))
cv2.imshow("Rotated by 45 Degrees", rotated)

# Volvemos a rotar la imagen acá (90 grados):
M = cv2.getRotationMatrix2D(center, -90, 1.0)
# Luego usamos este otro metodo para leer la imagen rotada:
rotated = cv2.warpAffine(image, M, (w, h))
cv2.imshow("Rotated by -90 Degrees", rotated)

cv2.waitKey(0)

# Para correr el codigo en consola, usamos el siguiente script:
# rotate.py -i images\trex.png