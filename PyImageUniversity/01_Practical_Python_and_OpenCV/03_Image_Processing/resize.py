# -----------------------------------------------
import os

# Aqui tenemos el directorio donde están contenidas las imagenes:
os.chdir("../..")
print(os.getcwd())

# -----------------------------------------------
# A partir de acá empieza el curso:
import numpy as np
import argparse
import imutils
import cv2

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required = True,
help = "Path to the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
cv2.imshow("Original", image)

# Buscamos el valor del radio que vamos a utilizar:
# Todo esto en base a 150 pixeles:
r = 150.0 / image.shape[1]
dim = (150, int(image.shape[0] * r))

resized = cv2.resize(image, dim, interpolation = cv2.INTER_AREA)
cv2.imshow("Resized (Width)", resized)

r = 50.0 / image.shape[0]
dim = (int(image.shape[1] * r), 50)

resized = cv2.resize(image, dim, interpolation = cv2.INTER_AREA)
cv2.imshow("Resized (Height)", resized)
cv2.waitKey(0)

# Ahora usando imutils podemos obtener la imagen resized:
resized = imutils.resize(image, width = 100)
cv2.imshow("Resized via Function", resized)
cv2.waitKey(0)

# Para correr el codigo en consola se coloca el sig. script:
# resize.py -i images\trex.png