# -----------------------------------------------
import os

# Aqui tenemos el directorio donde están contenidas las imagenes:
os.chdir("../..")
print(os.getcwd())

# -----------------------------------------------
# A partir de acá empieza el curso:
import argparse
import cv2

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required = True,
help = "Path to the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
cv2.imshow("Original", image)

flipped = cv2.flip(image, 1)
cv2.imshow("Flipped Horizontally", flipped)

flipped = cv2.flip(image, 0)
cv2.imshow("Flipped Vertically", flipped)

flipped = cv2.flip(image, -1)
cv2.imshow("Flipped Horizontally & Vertically", flipped)
cv2.waitKey(0)

# Para correr el codigo en consola se coloca el sig. script:
# flipping.py -i images\trex.png