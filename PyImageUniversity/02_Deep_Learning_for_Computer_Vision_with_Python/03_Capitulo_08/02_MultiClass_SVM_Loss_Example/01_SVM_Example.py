# Dada una matriz W arbitraria y bias vector b, las puntuaciones de salida de f(x, W) = Wx + b
# se muestran en el cuerpo de la matriz.

# Cuanto mayores sean las puntuaciones, mayor confianza tendrá nuestra score function con respecto a la predicción.

# Como es la tabla?
    #           Image 1    Image 2    Image3 
    # Dog        4.26       3.76      -2.37
    # Cat        1.33      -1.20       1.03
    # Panda     -1.01      -3.81      -2.27

# Comencemos por calcular la pérdida Li para la clase "perro":

# Para Image 1:
          #max(0, score_cat - score_dog + 1) + max(0, score_panda - score_dog + 1)
loss_dog = max(0,    1.33 -     4.26 + 1)    + max(0,    -1.01      - 4.26   + 1)
print("Loss de la clase -Perro-", loss_dog, "\n")
# En este caso, el resultado es 0. Esto implica que el perro fue predecido correctamente.

# Para Image 2:
          #max(0, score_dog - score_cat + 1) + max(0, score_panda - score_cat + 1)
loss_gato = max(0,   3.76     - (-1.20) + 1) + max(0,     -3.81    - (-1.20) + 1)
print("Loss de la clase -Gato-", loss_gato, "\n")

# Para Image 3:
            #max(0, score_dog - score_panda + 1) + max(0,   score_cat - score_panda + 1)
loss_panda = max(0,   -2.37     - (-2.27) + 1)   + max(0,     1.03     - (-2.27) + 1)
print("Loss de la clase -Panda-", loss_panda, "\n")

# Ahora, si buscamos el promedio de estos valores de loss:
loss_promedio = (loss_dog + loss_gato + loss_panda)/3
print("Promedio del Loss Total:", loss_promedio)

# -----------------------
# Páginas 92-93 del libro