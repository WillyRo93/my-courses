# Esto NO es un ejemplo para demostrar como se entrena un modelo de inicio a fin.
# En cambio, se intenta mostrar como se inicializa la matriz W y el vector bias b.

# Se importan los paquetes necesarios:
import numpy as np
import cv2

# Se inicializan los class/labels
# Se establece el seed del pseudorandom number generator, asi podemos reproducir resultados:
labels = ["dog", "cat", "panda"]
np.random.seed(1)

# A continuacion, inicializamos nuestra matriz W y el vecto bias b
# NOTA: En un entrenamiento y en un classification task --real-- estos parametros deberian
#       ser --aprendidos-- por nuestro modelo.
#       Pero para el ejercicio que queremos hacer, usaremos valores random.
W = np.random.randn(3, 3072)
b = np.random.randn(3)

# ---------------------------------------------------------------------------------------------------------
# NOTA: If we were training this linear classifier from scratch we would need to learn the values of W
# and b through an optimization process. However, since we have not reached the optimization stage
# of training a model, I have initialized the pseudorandom number generator with a value 1 to ensure
# the random values give us the “correct” classification (I tested random initialization values ahead of
# time to determine which value gives us the correct classification). For the time being, simply treat
# the weight matrix W and the bias vector b as "black box arrays" that are optimized in a magical way
# – we’ll pull back the curtain and reveal how these parameters are learned in the next chapter.
# ---------------------------------------------------------------------------------------------------------

# Cargamos nuestra imagen de ejemplo, le hacemos un resize, y luego lo aplanamos
# Para tener nuestra representacion del "feature vector".
orig = cv2.imread("beagle.png")
image = cv2.resize( orig, (32, 32) ).flatten()

# El proximo paso es computar las label/clases de salida aplicando nuestra scoring function:
scores = W.dot(image) + b

# Finalmente, hacemos un loop sobre los scores + labels y los mostramos:
for (label, score) in zip(labels, scores):
    print( "[INFO] {}: {:.2f}".format(label, score) )

# Escribimos el label con el mayor score en nuestra imagen predecida:
cv2.putText( orig, "Label: {}".format(labels[np.argmax(scores)]), (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.9, (0, 255, 0), 2 )

# Mostramos nuestra imagen de entrada:
cv2.imshow("Image", orig)
cv2.waitKey(0)

# -----------------------
# Páginas 87-89 del libro