# Implementando Mini-batch SGD (Standard Gradient Descent)

from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.datasets import make_blobs
import matplotlib.pyplot as plt
import numpy as np
import argparse

# Computa el valor de la activacion de sigmoid para un valor de entrada:
def sigmoid_activation(x):
    return 1.0 / (1 + np.exp(-x))

def predict(X, W):
    # Toma el producto punto entre nuestros features y la weight matrix
    print("X es:", X, "\n")
    print("W es:", W, "\n")
    preds = sigmoid_activation( X.dot(W))

    # Se aplica una funcion de escalon para limitar los outputs a labels de clase binaria.
    preds[ preds<= 0.5 ] = 0
    preds[ preds > 0 ] = 1

    # Se retornan las predicciones
    return(preds)

# X: Nuestro dataset de entrenamiento de vectores de caracteristicas/intensidades de pixeles.
# y: Las etiquetas de clase asociadas con cada uno de los puntos de entrenamiento.
# batchSize: El tamaño de cada mini-batch que será retornado.
def next_batch( X, y, batchSize):
    # Hacemos un loop en nuestro dataset "X" en mini-batches
    # Resultando en una tupla de los datos y labels del batch o lote.
    for i in np.arange( 0, X.shape[0], batchSize ):
        yield ( X[i:i + batchSize], y[i:i + batchSize] )

# Construimos el argument parse y hacemos el parsing del argumento:
ap = argparse.ArgumentParser()
ap.add_argument("-e", "--epochs", type=float, default=100,  help="# de epochs")
ap.add_argument("-a", "--alpha",  type=float, default=0.01, help="learning rate o taza de aprendizaje")
ap.add_argument("-b", "--batch_size", type=int, default=32, help="tamaño de los SGD mini-batches")
args = vars(ap.parse_args())

# Se genera un problema de clasificacion de 2-class con 100 data points
# Donde cada data point es un 2D feauture vector.
(X, y) = make_blobs( n_samples=1000, n_features=2, centers=2, cluster_std=1.5, random_state=1)
y = y.reshape((y.shape[0], 1))

# Se inserta una columna de 1s como el ultimo valor de entrada en el feature
# matriz -- Este pequeño truco nos permite tratar el bias como un parametro entrenable 
#           dentro de la weight matrix
X = np.c_[X, np.ones((X.shape[0]))]

# Particion de la data entre entrenamiento y testeo
# 50% data para entrenamiento - 50% data restante para testeo.
(trainX, testX, trainY, testY) = train_test_split( X, y, test_size=0.5, random_state=42)

# Inicializamos nuestra weight matrix y la lista de losses:
print("[INFO] entrenando... ")
W = np.random.randn( X.shape[1], 1)
losses = []

# A partir de acá viene lo bueno

# Loop sobre los numeros de epochs deseados:
for epoch in np.arange( 0, args["epochs"] ):
    # Inicializamos el loss total por el epoch
    epochLoss = []

    # Loop sobre nuestra data en batches o lotes:
    for (batchX, batchY) in next_batch( X, y, args["batch_size"]):
        # Se toma el producto punto entre nuestro actual batch de features
        # Y la weight marix, luego se pasa este valor a traves de nuestra funcion de activacion.
        preds = sigmoid_activation( batchX.dot(W) )

        # Ahora que tenemos nuestra prediccion, necesitamos determinar el 
        # "error" el cual es la diferencia entre nuestras predicciones y los valores reales.
        print("preds es:", preds, "\n", "Y su shape es:", preds.shape, "\n")
        print("batchY es:", batchY, "\n", "Y su shape es:", batchY.shape, "\n")
        error = preds - batchY
        epochLoss.append( np.sum(error ** 2) )

        # La acualizacion del gradient descent es el producto punto entre
        # nuestro actual batch y el error en el mismo batch.
        print("batchX es:", batchX, "\n", "Y su shape es:", batchX.shape, "\n")
        print("el error del batch es:", error, "\n", "Y su shape es:", error.shape, "\n")
        gradient = batchX.dot(error)

        # En la fase de actualizacion, lo que se necesita hacer es "empujar" la weight matrix en
        # direccion negativa del gradiente, dando un pequeño paso hacia un set de parametros mas "optimos".
        W += -args["alpha"] * gradient

        # Actualizamos nuestro loss history tomando el loss promedioentre todos los batches.
        loss = np.average(epochLoss)
        losses.append(loss)

        # Se chequea para observar si debería hacerse una actualización:
        if epoch == 0 or (epoch + 1) % 5 == 0:
            print("[INFO] epoch={}, loss={:.7f}".format(int(epoch + 1)))

# Evaluando nuestro modelo:
print("[INFO] evaluando... ")
preds = predict( testX, W )
print(classification_report(testY, preds) )

# Terminamos nuestro codigo graficando los datos de clasificacion de prueba junto a la perdida por epoch:

# Ploteando la classification data(testing):
plt.style.use("ggplot")
plt.figure()
plt.title("Data")
plt.scatter(testX[:, 0], testX[:,1], marker="o", c=testY, s=30)

# Construimos una figura que grafique el loss a traves del tiempo:
plt.style.use("ggplot")
plt.figure()
plt.plot(np.arange( 0, args["epochs"]), losses)
plt.title("Training Loss")
plt.xlabel("Epoch #")
plt.ylabel("Loss")
plt.show()

# -----------------------
# Páginas 107-110 del libro

# -----------------------
# Para procesar el codigo, se usa el siguiente script:
    # CMD:           NO FUNCIONA POR AHORA
    # Visual Studio: NO FUNCIONA POR AHORA