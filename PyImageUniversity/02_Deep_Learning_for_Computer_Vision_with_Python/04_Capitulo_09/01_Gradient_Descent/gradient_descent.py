# Importamos los paquetes necesarios:
from cv2 import waitKey
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.datasets import make_blobs

import matplotlib.pyplot as plt
import numpy as np
import argparse

# Acá tenemos una funcion que es llamada "Funcion de Activacion"
# Porque se activa o se apaga dependiendo del valor de salida:
#   Si el valor de salida >  0.5 --> Se "activa"
#   Si el valor de salida <= 0.5 --> Se "apaga"
def sigmoid_activation(x):
    # Se computa el valor del Sigmoid Activation para un input dado:
    return ( 1.0 / (1 + np.exp(-x)) )

# Luego creamos un metodo de prediccion:
def predict( X, W ):
    preds = sigmoid_activation(X.dot(W))
    # Se aplica una funcion de paso o escalon para hacerle un "threshold" a los labels de tipo binario:
    preds[preds <= 0.5] = 0
    preds[preds > 0] = 1
    return preds

# A partir de acá creamos nuestra linea de argumentos de comando:
ap = argparse.ArgumentParser()
# Epochs: 3 de epochs que usaremos para entrenar nuestro classifier usando un gradient descent.
ap.add_argument("-e", "--epochs", type=float, default=100, help="# de epochs")
# Alpha: Es el learning rate de nuestro gradient descent.
# Generalmente los valores estan entre 0.1, 0.01 y 0.001 como valoes iniciales.
# Este generalmente es un hiperparametro que se necesita ajustar a nuestro problema especifico.
ap.add_argument("-a", "--alpha", type=float, default=0.01, help="learning rate")
args = vars(ap.parse_args())

# Ahora, se generan 2 clases, X, y, donde tendremos 1000 data points.
# Y cada uno de esos data points es un feature vector 2D.
(X, y) = make_blobs(n_samples=1000, n_features=2, centers=2, cluster_std=1.5, random_state=1)
y = y.reshape( (y.shape[0], 1) )

# Aqui, se inserta una columna de 1´s en la última entrada en la feauture matrix:
# Esto es un truco que nos permite tratar el bias como un parámetro entrenable dentro de la matriz W.
X = np.c_[X, np.ones( (X.shape[0]) )]

# Se realiza la famosa particion de la data, 50% para el training - 50% para el testing.
(trainX, testX, trainY, testY) = train_test_split(X, y, test_size=0.5, random_state=42)

# Acá, inicializamos nuestra matriz W y una lista de losses o perdidas:
print("[INFO] training...")
W = np.random.randn( X.shape[1], 1 )
losses = []

# Hacemos ahora un loop sobre la cantidad de epochs que queremos:
for epoch in np.arange(0, args["epochs"]):
    # Se toma el dot product entre nuestros features "X" y la matriz W
    # Luego, pasamos este valor por nuestra funcion de sigmoid activation
    # De este modo, tendremos nuestras predicciones para el dataset.
    preds = sigmoid_activation( trainX.dot(W) )

    # Ahora ya teniendo nuestras predicciones, el proximo paso es determinar el "error"
    # de estas predicciones.
    # El error es basicamente, la diferencia entre nuestras predicciones y los valores reales.
    error = preds - trainY

    # Calculamos el loss o la perdida.
    # Un loss simple tipicamente usado para problemas de clasificacion binaria es el sig:
        # Calcular el error de mínimos cuadrados sobre nuestras predicciones:
    loss = np.sum( error**2 )

    # Hacemos append del loss para tener como plotearlo con el tiempo:
    losses.append(loss)

    # Teniendo ahora el error, podemos calcular el gradiente y usarlo para actualizar nuestra matriz W.
    # El La actualizacion del gradient descent es el producto punto entre nuestros features y el error de las predicciones:
    gradient = trainX.T.dot(error)

    # En este punto, debemos actualizar. Todo lo que tnemos que hacer es "empujar" a la matriz W en la direccion
    # negativa del gradiente - de aqui viene el termino de "gradient descent" o gradiente de descenso- 
    # Como lo "empujamos"? -- > Se toman unos pequeños pasos hacie parametros mas "optimos"
    W += -args["alpha"] * gradient

    # Luego de actualizar nuestra matriz W, chequeamos si se debe actualizar nuestro terminal(?)
    # Seguimos haciendo este chequeo en loop, hasta que nuestro numero deseado de epochs sea cumplido.
    if epoch == 0 or (epoch + 1) % 5 == 0:
        print("[INFO] epoch={}, loss={:.7f}".format (int( epoch + 1 ), loss ) )

# Finalmente, nuestro classifier está entrenado, el proximo paso es la evaluacion:
print("[INFO] evaluando, espere un momento...")

# Para hacer la evaluación o prediccion, llamamos a la funcion predict para testX y W
preds = predict(testX, W)

# Imprimimos con un formato bien boniko
print(classification_report(testY, preds))

# Manejamos nuestra testing data para ploterla:
plt.style.use("ggplot")
plt.figure()
plt.title("Data")
plt.scatter(testX[:,0], testX[:, 1], marker="o", c=testY, s=30)

# Y con esto visualizamos el dataset que intentamos clasificar y nuestro loss atraves del tiempo:
plt.style.use("ggplot")
plt.figure()
plt.plot(np.arange (0, args["epochs"]), losses )
plt.title("Training Loss")
plt.xlabel("Epoch #")
plt.ylabel("Loss")
plt.show()

# -----------------------
# Páginas 102-106 del libro