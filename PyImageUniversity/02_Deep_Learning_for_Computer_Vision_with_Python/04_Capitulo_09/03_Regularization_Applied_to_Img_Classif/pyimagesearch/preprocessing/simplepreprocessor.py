# Se importan los paquetes necesarios:
import cv2

# Armamos la siguiente clase:
class SimplePreprocessor:

    # Con esto guardamos las caracteristicas de la imagen (ancho, alto e interpolacion)
    def __init__ (self, width, height, inter=cv2.INTER_AREA):
        # Cuando se aplica resizing, se hace de la siguiente manera:
        self.width = width
        self.height = height
        self.inter = inter

    # Con esta funcion le hacemos un resize a la imagen, ignorando su aspecto(?):
    def preprocess(self, image):
        #ratio bobis
        return cv2.resize(image, (self.width, self.height), interpolation=self.inter)

# -----------------------
# Página 72 del libro