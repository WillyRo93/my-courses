# La secuencia de Fibonacci es un ejemplo de como un input puede modificar el valor de salida,
# es una especie de simil con respecto al Deep Learning. 

# Aunque NO ES DEEP LEARNING.

def fib(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fib(n-1) + fib(n-2)

valor = int(input("Coloque el numero de la serie de Fibonacci que desea encontrar:  "))

respuesta = fib(valor)
print(f"El valor numerico para el valor #{valor} es: {respuesta}")