import os

# Aqui tenemos el directorio donde están contenidas las imagenes:
os.chdir("../../images")
print(os.getcwd())

# -----------------------------------------------
# A partir de acá empieza el curso:
import cv2
image = cv2.imread("beach.png")
print(image.shape)

cv2.imshow("Imagen de ejemplo:", image)
cv2.waitKey(0)

# La respues es la siguiente:
#(alto, ancho, depth)
#(233,  350,     3)

# Que significan estos valores?

# alto: 248 pixeles (Numero de filas)
# ancho: 300 pixeles (Numero de columnas)
# depth: 3 (Numero de canales)

# ------------------------------------------------
# Para acceder a cierto pixel hacemos lo siguiente:
(b, g, r) = image[20, 100] # accesses pixel at x=100, y=20
(b, g, r) = image[75, 25] # accesses pixel at x=25, y=75
(b, g, r) = image[90, 85] # accesses pixel at x=85, y=90