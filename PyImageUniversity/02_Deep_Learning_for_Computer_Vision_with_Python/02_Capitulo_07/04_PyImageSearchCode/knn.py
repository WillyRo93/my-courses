# Importamos los paquetes necesarios:
import os

# De la libreria skit-learn importamos todo lo siguiente:
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report

# Estos a continuacion son los códigos que ya hicimos previamente, para el procesado:
from pyimagesearch.preprocessing.simplepreprocessor import SimplePreprocessor
from pyimagesearch.datasets.simpledatasetloader import SimpleDatasetLoader
from imutils import paths
import argparse

# Aqui cambiamos de directorio, porque debemos estar en la misma carpeta que los
# datasets que contienen las imagenes de los animales:
os.chdir("../../")
print("\n", "Direccion acual:", "\n", os.getcwd(), "\n")

# Paso 1: Reunir toda la Data
# ------------------------------------------------------------------------------
# Construimos el argument parse y le hacemos parse a los argumentos:
ap = argparse.ArgumentParser()

# Con este se coloca el path para hacer input al dataset:
ap.add_argument("-d", "--dataset", required=True, help="path to input dataset")

# Argumento para el valor de k (Vecinos)
ap.add_argument("-k", "--neighbors", type=int, default=1, help="# de vecinos cercanos para la clasificacion")

# Argumento para los jobs o trabajos por k-NN
ap.add_argument("-j", "--jobs", type=int, default=1, help="# de trabajos por la distancia k-NN (-1 usa todos los nucleos posibles)" )

args = vars( ap.parse_args() )

# Ahora tomaremos las imagenes que estaremos describiendo:
print("[INFO] cargando imagenes ...")
imagePaths = list( paths.list_images ( args["dataset"] ) )
#print(imagePaths)

# Inicializamos el preprocesador de imagenes, cargamos el dataset desde el disco y hacemos un reshape a la matriz de data:
sp = SimplePreprocessor(32, 32)
sdl = SimpleDatasetLoader(preprocessors=[sp])
(data, labels) = sdl.load(imagePaths, verbose=500)
data = data.reshape( (data.shape[0], 3072) )

#Mostramos alguna informacion del consumo de memoria de las imagenes:
print( "[INFO] features matrix: {:.1f}MB".format( data.nbytes/(1024*1000.0) ) )


# Paso 2: Dividir el Dataset
# ------------------------------------------------------------------------------
# Encode the labels as integers:
le = LabelEncoder()
labels = le.fit_transform(labels)

# Aqui haremos la particion de la data (Training Data y Testing Data)
# Training (75%) - Testing (25%)

# Generalmente se usan las X para referirse al dataset que contiene los data points que se usaran en training y testing.
# Generalmente se usan las Y para referirse a las Class Labels.
(trainX, testX, trainY, testY) = train_test_split(data, labels, test_size=0.25, random_state=42)

# train and evaluate a k-NN classifier on the raw pixel intensities
print("[INFO] evaluating k-NN classifier...")
model = KNeighborsClassifier(n_neighbors=args["neighbors"], n_jobs=args["jobs"])
model.fit(trainX, trainY)
print(classification_report(testY, model.predict(testX), target_names=le.classes_))

# -----------------------
# Páginas 72-74 del libro

# -----------------------
# Para procesar el codigo, se usa el siguiente script:
    # CMD:           knn.py --dataset ../datasets/animals 
    # Visual Studio: py knn.py --dataset ../datasets/animals  