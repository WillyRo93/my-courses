# Se importan los paquetes necesarios:
from email.mime import image
import numpy as np
import cv2
import os

# Creo la siguiente Class:
class SimpleDatasetLoader:

    def __init__ (self, preprocessors=None):
        # Aqui guardo el valor del pre-procesado de la imagen:
        self.preprocessors = preprocessors

        # Si en la entrada, el pre-procesado es None, se inicializa como una lista vacia:
        if self.preprocessors is None:
            self.preprocessors = []

    def load(self, imagePaths, verbose=-1):
        # Inicializamos unas listas de labels y de data:
        data = []
        labels = []

        # Hacemos un loop sobre las imagenes:
        for (i, imagePath) in enumerate(imagePaths):
            # Se carga la imagen y se extrae el label asumiendo un formato como el sig:
                # /path/to/dataset/{class}/{image}.jpg
            image = cv2.imread(imagePath)
            # Aqui queremos la clase/label por lo tanto es el segundo valor de derecha a izq:

            # Por alguna razon, la siguiente linea no funcionaba correctamente
            # Aparentemente, el separador que leo es "\\"
            # Pero el que identifica el sistema operativo es "\"
            # El problema era que lo habia copiado mal jeje
            label = imagePath.split(os.path.sep)[-2]
            #print(label)

            #Por lo tanto, probaremos mientras con la siguiente forma de dividir:
            #label = imagePath.split("\\")[-2]
            #print(label)

            # Chequeamos si nuestros preprocessors NO son None:
            if self.preprocessors is not None:
                # Empezamos un loop sobre los pre-procesados y aplicamos cada uno a la imagen:
                for p in self.preprocessors:
                    image = p.preprocess(image)

            # Tratamos a nuestra imagen procesada como un "feauture vector" actualizando la lista de data seguida de los labels:
            data.append(image)
            labels.append(label)

            # En esta linea de codigo se quiere mostrar y hacer update de cada imagen "verbose"(?):
            if verbose > 0 and i > 0 and (i + 1) % verbose == 0:
                print( "[INFO] processed {}/{}".format( i + 1, len(imagePaths) ) )

        # Finalmente, retornamos un tuple de la data y de los labels:
        return ( np.array(data), np.array(labels) )

# -----------------------
# Páginas 72-74 del libro