# Programa de Python para leer un .json

import json
 
# Se abre el archivo .json
f = open('devdata/env.json')
 
# Se retorna el .json como un diccionario:
data = json.load(f)
 
# Imprimiendo un valor del .json
direccion = data["VAULT_SECRETO"]
print(direccion)

# Cerrando el archivo:
f.close()