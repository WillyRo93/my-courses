import cv2


# read image
img = cv2.imread('test.jpg')

# show image
cv2.namedWindow("test", cv2.WINDOW_NORMAL)
cv2.imshow('test', img)

#define the events for the mouse_click.
def mouse_click(event, x, y, flags, param):

    # to check if left mouse button was clicked
    if event == cv2.EVENT_LBUTTONDOWN:
		# Aqui inicializamos los valores de x e y:
        x_initial = x
        y_initial = y

		# font for left click event.
        font = cv2.FONT_HERSHEY_TRIPLEX
        LB = f'Punto de inicio ({x_initial}, {y_initial})'
		
		# display that left button was clicked.
        cv2.putText(img, LB, (x_initial, y_initial), font, 1, (0, 0, 0), 2)
        cv2.imshow('test', img)
		
		
	# to check if right mouse button was clicked
    if event == cv2.EVENT_RBUTTONDOWN:
		# Aqui inicializamos los valores de x e y:
        x_final = x
        y_final = y

		# font for right click event
        font = cv2.FONT_HERSHEY_TRIPLEX
        RB = f'Punto final ({x_final}, {y_final})'
		
		# display that right button was clicked.
        cv2.putText(img, RB, (x_final, y_final), font, 1, (0, 0, 255), 2)
        cv2.imshow('test', img)


cv2.setMouseCallback('test', mouse_click)

cv2.waitKey(0)

# close all the opened windows.
cv2.destroyAllWindows()
