import cv2
import numpy as np

#image_path
img_path="test.jpg"

#read image
img_raw = cv2.imread(img_path)

#select ROIs function
cv2.namedWindow("Select Rois", cv2.WINDOW_NORMAL)
ROIs = cv2.selectROIs("Select Rois",img_raw)

#print rectangle points of selected roi
print(ROIs)

#Crop selected roi ffrom raw image

#counter to save image with different name
crop_number=0 

#loop over every bounding box save in array "ROIs"
for rect in ROIs:
    x1=rect[0]
    y1=rect[1]
    x2=rect[2]
    y2=rect[3]

    #crop roi from original image
    img_crop=img_raw[y1:y1+y2,x1:x1+x2]
    
    #show cropped image
    cv2.namedWindow("crop"+str(crop_number), cv2.WINDOW_NORMAL)
    cv2.imshow("crop"+str(crop_number), img_crop)

	#save cropped image
    cv2.imwrite("crop"+str(crop_number)+".jpeg",img_crop)

    crop_number+=1

#hold window
cv2.waitKey(0)
cv2.destroyAllWindows()

# Desde aqui, utilizo mis ROIs:
# En mi primer caso, se que tengo 52 valores, asi que tomaré toda la zona
# de números (o códigos) y lo dividiré entre 52, pero lo haré con un 
# input que yo mismo colocaré.

division_codigos = input("Cuantos codigos existen en este archivo?:  ")
print(f"En esta imagen mostrada hay {division_codigos} códigos")

for region in ROIs:
    print("Inicio en x: ", region[0], "Inicio en y: ", region[1])
    print("Final en x: ", region[2], "Final en y: ", region[3])
    print("\n")

# Aqui hacemos un loop sobre nuestras regiones para recortar la cantidad de valores pedidos:
# while (int(division_codigos) > 0):
#     division_codigos =+ 1
#     x1=rect[0]
#     y1=rect[1]
#     x2=rect[2]
#     y2=rect[3]

#     #crop roi from original image
#     img_crop=img_raw[y1:y1+y2,x1:x1+x2]
    
#     #show cropped image
#     cv2.namedWindow("crop"+str(crop_number), cv2.WINDOW_NORMAL)
#     cv2.imshow("crop"+str(crop_number), img_crop)

# 	#save cropped image
#     cv2.imwrite("crop"+str(crop_number)+".jpeg",img_crop)

#     crop_number+=1