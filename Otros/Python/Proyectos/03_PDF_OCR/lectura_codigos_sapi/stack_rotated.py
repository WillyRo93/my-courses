import cv2
import numpy as np
import imutils

# load image as HSV and select saturation
img = cv2.imread("test.jpg")
# img = cv2.imread("wing.png")
hh, ww, cc = img.shape

# convert to gray
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# threshold the grayscale image
thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]

# Aplicamos ahora, una "transformacion de distancia" que calcula la distancia hasta el pixel "0" mas cercano.
# Es decir, por cada pixel negro que existe en la imagen.
dist = cv2.distanceTransform(thresh, cv2.DIST_L2, 5)

# Luego, normalizamos la transformacion de distancia de tal manera que la distancia permanezca en [0, 1]
# Y luego, convertimos esta distancia de nuevo a un 8-bit integer, en el rango de [0,255].
dist = cv2.normalize(dist, dist, 0, 1.0, cv2.NORM_MINMAX)
dist = (dist * 255).astype("uint8")

# Hacemos threshold a la transformacion de distancia usando el metodo "Otsu"
dist = cv2.threshold(dist, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

# find outer contour
cntrs = cv2.findContours(dist, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
cntrs = cntrs[0] if len(cntrs) == 2 else cntrs[1]

# get rotated rectangle from outer contour
rotrect = cv2.minAreaRect(cntrs[0])
box = cv2.boxPoints(rotrect)
box = np.int0(box)

# draw rotated rectangle on copy of img as result
result = img.copy()
cv2.drawContours(result,[box],0,(0,0,255),2)

# get angle from rotated rectangle
angle = rotrect[-1]

# from https://www.pyimagesearch.com/2017/02/20/text-skew-correction-opencv-python/
# the `cv2.minAreaRect` function returns values in the
# range [-90, 0); as the rectangle rotates clockwise the
# returned angle trends to 0 -- in this special case we
# need to add 90 degrees to the angle
if angle < -45:
    angle = -(90 + angle)

# otherwise, just take the inverse of the angle to make
# it positive
else:
    angle = -angle

print(angle,"deg")

# write result to disk
cv2.imwrite("wing2_rotrect.png", result)

# El resultado del Thresh:
cv2.namedWindow("THRESH", cv2.WINDOW_NORMAL)
cv2.imshow("THRESH", thresh)

# El resultado de la caja rotada:
cv2.namedWindow("RESULT", cv2.WINDOW_NORMAL)
cv2.imshow("RESULT", result)

# Ahora, por ultimo, rotamos la imagen a la orientacion correcta:
if angle < 0 and angle > -90:
    angle = 90 + angle
    rotated = imutils.rotate_bound(result, angle)

cv2.namedWindow("Rotado correctamente", cv2.WINDOW_NORMAL)
cv2.imshow("Rotado correctamente", rotated)

cv2.waitKey(0)
cv2.destroyAllWindows()