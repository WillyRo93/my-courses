import cv2
import math
import imutils

path = "test.jpg"
img = cv2.imread(path)

pointsList = []

def mousePoints(event, x, y, flags, params):
    if event == cv2.EVENT_LBUTTONDBLCLK:
        size = len(pointsList)
        if size != 0 and size % 2 != 0:
            cv2.line(img, tuple(pointsList[round((size-1)/3)*3]), (x, y), (0, 0, 255), 2)
        cv2.circle(img, (x, y), 5, (0, 0, 255), cv2.FILLED)
        pointsList.append([x, y])

def gradient(pt1, pt2):
    # pt1 = x1, y1
    # pt2 = x2, y2
    return (pt2[1] - pt1[1]) / (pt2[0] - pt1[0])

def getAngle(pointsList):
    pt1 = pointsList[0]
    pt2 = pointsList[1]
    if pt1[0] > pt2[0] and pt1[1] > pt2[1]:
        pt3 = [pt2[0], pt1[1]] 
    elif pt1[0] < pt2[0] and pt1[1] < pt2[1]:
        pt3 = [pt2[0], pt1[1]]
    elif pt1[0] > pt2[0] and pt1[1] < pt2[1]:
        pt3 = [pt2[0], pt1[1]]
    elif pt1[0] < pt2[0] and pt1[1] > pt2[1]:
        pt3 = [pt2[0], pt1[1]]

    m1 = gradient(pt1, pt2)
    m2 = gradient(pt1, pt3)
    angR = math.atan((m2-m1) / (1 + (m2*m1)) )
    angD = round(math.degrees(angR))

    # Angulo de rotacion:
    rotation_angle = -(angD - 90)
    print(rotation_angle)
    # print(angD, "grados")
    cv2.putText(img, str(angD), (pt1[0]-40, pt1[1] - 20),
                cv2.FONT_HERSHEY_COMPLEX, 1.5, (0, 0, 255))
    return angD, rotation_angle

imagen_rotada = img.copy()
# Acá tenemos el angulo de rotacion segun el valor:
rotated = imutils.rotate_bound(imagen_rotada, rotation_angle)

while True:
    if len (pointsList) % 2 == 0 and len(pointsList) != 0:
        angD, rotation_angle = getAngle(pointsList)

    cv2.namedWindow("Image", cv2.WINDOW_NORMAL)
    cv2.imshow("Image", img)

    cv2.namedWindow("Rotado correctamente", cv2.WINDOW_NORMAL)
    cv2.imshow("Rotado correctamente", rotated)

    cv2.setMouseCallback("Image", mousePoints)
    if cv2.waitKey(1) & 0xFF == ord("q"):
        pointsList = []
        img = cv2.imread(path)
