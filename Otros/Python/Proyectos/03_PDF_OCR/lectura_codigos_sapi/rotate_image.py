# import the necessary packages
import argparse
import imutils
import cv2
import pytesseract
from pytesseract import Output
import os

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", type=str, default="rotated.jpg",
	help="path to the input image")
args = vars(ap.parse_args())

# load the image and show it
image = cv2.imread(args["image"])
cv2.namedWindow("Original", cv2.WINDOW_NORMAL)
cv2.imshow("Original", image)

cwd = os.getcwd()
print("\n Este es el directorio actual", cwd, "\n")

# Esta es la configuracion para el OSD
osd_tess_config = (" -l osd --psm 0")

# En este punto, vamos a detectar cuan rotada está la imagen.
results = pytesseract.image_to_osd(f"{cwd}//rotated.jpg", output_type=Output.DICT, config=osd_tess_config)
# results = pytesseract.image_to_osd(f"{cwd}//codes1.png", output_type=Output.DICT, config="osd --psm 4 --dpi 300")
# results = pytesseract.image_to_osd(image, output_type=Output.DICT, config="osd --psm 4 --dpi 300")
print(results)

# # grab the dimensions of the image and calculate the center of the image
(h, w) = image.shape[:2]
(cX, cY) = (w // 2, h // 2)

# # rotate our image by 45 degrees around the center of the image
# M = cv2.getRotationMatrix2D((cX, cY), 45, 1.0)
# rotated = cv2.warpAffine(image, M, (w, h))
# cv2.imshow("Rotated by 45 Degrees", rotated)

# # rotate our image by -90 degrees around the image
# M = cv2.getRotationMatrix2D((cX, cY), -90, 1.0)
# rotated = cv2.warpAffine(image, M, (w, h))
# cv2.imshow("Rotated by -90 Degrees", rotated)

# # rotate our image around an arbitrary point rather than the center
# M = cv2.getRotationMatrix2D((10, 10), 45, 1.0)
# rotated = cv2.warpAffine(image, M, (w, h))
# cv2.imshow("Rotated by Arbitrary Point", rotated)

# # use our imutils function to rotate an image 180 degrees
# rotated = imutils.rotate(image, 180)
# cv2.imshow("Rotated by 180 Degrees", rotated)

# # rotate our image by 33 degrees counterclockwise, ensuring the
# # entire rotated image still renders within the viewing area
# rotated = imutils.rotate_bound(image, -33)
# cv2.imshow("Rotated Without Cropping", rotated)

# Ahora, por ultimo, rotamos la imagen a la orientacion correcta:
rotated = imutils.rotate_bound(image, -115)
# rotated = imutils.rotate_bound(image, angle=-results["rotate"])

cv2.namedWindow("Rotado correctamente", cv2.WINDOW_NORMAL)
cv2.imshow("Rotado correctamente", rotated)

cv2.waitKey(0)