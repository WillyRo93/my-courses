# Vamos a realizar un clever deseigned pipeline para procesar imagenes.

# Primero, importamos las librerias necesarias:
import numpy as np
import pytesseract
import argparse
import imutils
import cv2

# Construimos el parser:
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path de la imagen a aplicar OCR.")
args = vars(ap.parse_args())

# =====================================
# PROCESSING PIPELINE
image = cv2.imread(args["image"])
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# Aplicamos el threshold a la imagen, usando el metodo "Otsu"
thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]

# Mostramos nuestra imagen procesada.
cv2.imshow("Otsu", thresh)

# # Aplicamos ahora, una "transformacion de distancia" que calcula la distancia hasta el pixel "0" mas cercano.
# Es decir, por cada pixel negro que existe en la imagen.
dist = cv2.distanceTransform(thresh, cv2.DIST_L2, 5)

# # Luego, normalizamos la transformacion de distancia de tal manera que la distancia permanezca en [0, 1]
# # Y luego, convertimos esta distancia de nuevo a un 8-bit integer, en el rango de [0,255].
dist = cv2.normalize(dist, dist, 0, 1.0, cv2.NORM_MINMAX)
dist = (dist * 255).astype("uint8")
cv2.imshow("Dist", dist)

# Hacemos threshold a la transformacion de distancia usando el metodo "Otsu"
dist = cv2.threshold(dist, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
cv2.imshow("Dist Otsu", dist)

# Aplicamos una "morphological opening operation" para "desconectar" componentes en la imagen.
kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (7,7))
opening = cv2.morphologyEx(dist, cv2.MORPH_OPEN, kernel)
cv2.imshow("Opening", opening)

# A partir de este punto, ya podemos extraer contornos desde la imagen y filtrarlos para revelar 
# solo a los digitos. Como lo hacemos? Encontramos los contornos, luego inicializamos una lista
# de contornos que pertenezcan realmente a los caracteres a los cuales se les aplicará el OCR.
cnts = cv2.findContours(opening.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
cnts = imutils.grab_contours(cnts)
chars = []

# # Hacemos un loop sobre los contornos:
for c in cnts:
    # Calculamos el cuadro delimitador del contorno
    (x, y, w, h) = cv2.boundingRect(c)

    # Chequeamos si el contorno as al menos 35px de ancho y 100px de alto
    # De ser asi, se considera que este contorno es un digito.
    if w >= 0 and h >= 10:
        chars.append(c)

# # A partir de este momento, tenemos nuestros contornos delimitados, así que procedemos
# # a limpiar el area de al rededor.
# # Calculamos el "Convex Hull" de los caracteres.
chars = np.vstack([chars[i] for i in range (0, len(chars))])
hull = cv2.convexHull(chars)

# # Asignamos memoria para el "Convex Hull Mask", dibujamos el "Convex Hull" en la imagen
# # y luego se amplía a través de una dilatación
mask = np.zeros(image.shape[:2], dtype="uint8")
cv2.drawContours(mask, [hull], -1, 255, -1)
mask = cv2.dilate(mask, None, iterations=1000)
cv2.imshow("Mask", mask)

# # Se toma el bitwise de la "Opening Image" y la máscara para revelar *solo*
# # los caracteres de la imagen
# final = cv2.bitwise_and(opening, opening, mask=mask)

# # Con nuestro producto final, ya limpiado, aplicamos OCR usando tesseract
#options = "--psm 8 -c tessedit_char_whitelist=0123456789"
text = pytesseract.image_to_string(mask)
print("Mask:", text, "\n")

# # Finalmente, mostramos nuestro output final
# cv2.imshow("Final", final)

# # Con we waitkey mantenemos la ventana abierta.
cv2.waitKey(0)

# # -----------------------
# # Páginas 100-104 del libro

# # -----------------------
# # CMD:           process_image.py --image challenging_example.png
# # Visual Studio: py process_image.py --image challenging_example.png