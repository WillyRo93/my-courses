import argparse
from email.mime import image
import cv2

ap = argparse.ArgumentParser()
ap.add_argument('-i', '--image', required=True, help="Image Path")
args = vars(ap.parse_args())

img_path = args['image']
#Reading image with opencv
img = cv2.imread(img_path)

clicked = False
xpos = ypos = 0

def draw_function(event, x,y,flags,param):
    global xpos, ypos, clicked

    if event == cv2.EVENT_LBUTTONDBLCLK:
        clicked = True
        xpos = x
        ypos = y
        return xpos, ypos
    elif event == cv2.EVENT_LBUTTONDBLCLK:
        clicked = True
        xpos = x
        ypos = y
        return xpos, ypos

# cv2.namedWindow("image", cv2.WINDOW_NORMAL)
# cv2.setMouseCallback("image", draw_function)

while (1):

    #imageSmall = cv2.resize(img, (960, 540))
    cv2.namedWindow("image", cv2.WINDOW_NORMAL)
    cv2.setMouseCallback("image", draw_function)
    cv2.imshow("image",img)

    print(clicked)
    if (clicked):
        print("Si, has clickeado chaval")

    #Break the loop when user hits 'esc' key    
    if cv2.waitKey(20) & 0xFF ==27:
        break

cv2.destroyAllWindows()