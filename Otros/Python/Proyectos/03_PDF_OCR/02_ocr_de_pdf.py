# Vamos a realizar un clever deseigned pipeline para procesar imagenes.

# Primero, importamos las librerias necesarias:
import os
from PIL import Image
from pdf2image import convert_from_path
import pytesseract

import numpy as np
import pytesseract
import argparse
import imutils 
import cv2

my_config = r"--psm 4 --oem 3"

filePath = 'D:/Usuarios/wrodriguez/Downloads/Cursos_Gitlab/my-courses/Otros/PDF_OCR/codes.pdf'
doc = convert_from_path(filePath)
path, fileName = os.path.split(filePath)
fileBaseName, fileExtension = os.path.splitext(fileName)

for page_number, page_data in enumerate(doc):
    #txt = pytesseract.image_to_string(Image.fromarray(page_data)).encode("utf-8")
    txt = pytesseract.image_to_string(page_data, config = my_config).encode("utf-8")
    txt = str(txt)
    #txt = txt.rstrip
    print("Page # {} - {}".format(str(page_number),txt))