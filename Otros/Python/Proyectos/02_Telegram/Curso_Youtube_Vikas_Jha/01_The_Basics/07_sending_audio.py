import requests

# Para enviar audios usaremos "sendAudio"
base_url = "https://api.telegram.org/bot123abcFGH_jw9g/sendAudio"

# Agregamos un diccionario de parametros:
	# Donde "chat_id" es el id del chat que encontramos al hacer "getUpdates"
	# Y "photo" es un link con una imagen a enviar
parameters = {
	"chat_id" : "-786099214",
	"audio" : "https://www.bensound.com/bensound-music/bensound-anewbeginning.mp3"
	"caption" : "High Piece"
}

resp = requests.get(base_url, data = parameters)
print(resp.text)