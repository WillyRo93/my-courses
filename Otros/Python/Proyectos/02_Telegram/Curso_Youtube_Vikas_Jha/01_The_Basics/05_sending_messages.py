import requests

# Para enviar mensajes usaremos "sendMessage"
base_url = "https://api.telegram.org/bot123abcFGH_jw9g/sendMessage"

# Agregamos un diccionario de parametros:
	# Donde "chat_id" es el id del chat que encontramos al hacer "getUpdates"
	# Y "text" es el mensaje que se va a enviar
parameters = {
	"chat_id" : "-786099214",
	"text" : "Hi from the bot"
}

resp = requests.get(base_url, data = parameters)
print(resp.text)

# ----------------------------------------------------------------------------------
# Que pasa si lo queremos hacer un loop? Ademas que se ejecute cada cierto tiempo
# Hacemos una lista con distintos textos y lo reemplazamos en "text"
import time

mensajes = ["Wepale que tal?",
			"Wejele qlq",
			"Como tutas?",
			"Vaya vaya nos encontramos de nuebo"]

for mensaje in mensajes:
	time.sleep(6)
	parameters = {
		"chat_id" : "-786099214",
		"text" : mensaje
	}

	resp = requests.get(base_url, data = parameters)
	print(resp.text)