import requests

# Si nos damos cuenta, tenemos una url base para llamar a la API
# Esta URL es:
base_url = "https://api.telegram.org/bot123abcFGH_jw9g/getUpdates"

# Agregamos un diccionario de parametros:
	# Aqui agregamos offset y limit, que limita el numero de respuestas que queremos ver 
parameters = {
	"offset" : "786099214",
	"limit" : "1"
}

resp = requests.get(base_url, data = parameters)
print(resp.text)

# Antes teniamos lo siguiente:
	# resp = requests.get("https://api.telegram.org/bot123abcFGH_jw9g/getUpdates?offset=786099214")