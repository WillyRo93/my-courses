# Ahora queremos leer la informacion que el html no es capaz de leer
# Queremos profundizar la busqueda de datos

# Tenemos distintos parametros:
# 1) offset: Identifica el primer update a ser mostrado.

import requests

# Hacemos lo mismo que antes, pero añadimos "?offset=" al final del http
# Junto con el update_id que queremos buscar de primero:
	# Donde el update_id = 786099214
resp = requests.get("https://api.telegram.org/bot123abcFGH_jw9g/getUpdates?offset=786099214")

print(resp.text)