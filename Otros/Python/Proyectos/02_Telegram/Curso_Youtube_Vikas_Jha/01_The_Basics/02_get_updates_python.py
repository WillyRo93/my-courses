# Ahora, sabiendo como se accesa al sitio online
# Podemos ver como se revisa la info desde Python

# Importamos la libreria "requests"
import requests

# Asociamos un response al request de la pagina
resp = requests.get("https://api.telegram.org/bot123abcFGH_jw9g/getUpdates")

# Para mostrar la info imprimimos el texto que devuelve:
print(resp.text)