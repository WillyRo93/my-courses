import requests

base_url = "https://api.telegram.org/bot123abcFGH_jw9g"

# -------------------------------------------------------------------------
# Funcion que lee los mensajes:
def read_msg():
  parameters = {
      "offset" : "786099214"
  }

  resp = requests.get(base_url + "/getUpdates", data = parameters)
  data = resp.json()

  for result in data["result"]:
    if "hi" in result["message"]["text"]:
      send_msg()

# -------------------------------------------------------------------------
# Funcion que envía los mensajes:
def send_msg():
  parameters = {
      "chat_id" : "-786099214",
      "text" : "Hello thereeee!!!!"
  }

  resp = requests.get(base_url + "/sendMessage", data = parameters)
  print(resp.text)
  
# -------------------------------------------------------------------------

# Llamando a la funcion de leer mensaje:
read_msg()