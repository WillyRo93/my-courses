import requests

base_url = "https://api.telegram.org/bot123abcFGH_jw9g"

# -------------------------------------------------------------------------
# Funcion que lee los mensajes:
def read_msg(offset):
  parameters = {
      "offset" : offset
  }

  resp = requests.get(base_url + "/getUpdates", data = parameters)
  data = resp.json()
  print(data)

  for result in data["result"]:
    if "hi" in result["message"]["text"]:
      send_msg()
  
  if data["result"]:
    return data["result"][-1]["update_id"] + 1

# -------------------------------------------------------------------------
# Funcion que envía los mensajes:
def send_msg():
  parameters = {
      "chat_id" : "-786099214",
      "text" : "Hello Thereeee!!!!!"
  }

  resp = requests.get(base_url + "/sendMessage", data = parameters)
  print(resp.text)
  
# -------------------------------------------------------------------------

# Llamando a la funcion de leer mensaje:
offset = 0
while True:
  offset = read_msg(offset)