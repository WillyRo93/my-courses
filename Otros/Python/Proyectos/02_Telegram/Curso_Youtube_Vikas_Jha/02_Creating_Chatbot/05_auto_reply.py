import requests
import pandas as pd

url = "https://raw.githubusercontent.com/vikasjha001/telegram/main/qna_chitchat_professional.tsv"

df = pd.read_csv(url, sep="\t")

base_url = "https://api.telegram.org/bot123abcFGH_jw9g"

# -------------------------------------------------------------------------
# Funcion que lee los mensajes:
def read_msg(offset):
  parameters = {
      "offset" : offset
  }

  resp = requests.get(base_url + "/getUpdates", data = parameters)
  data = resp.json()
  print(data)

  for result in data["result"]:
    send_msg(result["message"]["text"], result["message"]["message_id"])
  
  if data["result"]:
    return data["result"][-1]["update_id"] + 1

# -------------------------------------------------------------------------
# Funcion de respuesta automática:
def auto_answer(message):
  answer = df.loc[df["Question"].str.lower() == message.lower()]

  if not answer.empty:
    answer = answer.iloc[0]["Answer"]
    return answer
  else:
    return "Sorry, I could not understand you! I am still learning and trying to get better answering."

# -------------------------------------------------------------------------
# Funcion que envía los mensajes:
def send_msg(message, message_id):
  answer = auto_answer(message)

  parameters = {
      "chat_id" : "-786099214",
      "text" : answer,
      "reply_to_message_id" : message_id
  }

  resp = requests.get(base_url + "/sendMessage", data = parameters)
  print(resp.text)
  
# -------------------------------------------------------------------------

# Llamando a la funcion de leer mensaje:
offset = 0
while True:
  offset = read_msg(offset)