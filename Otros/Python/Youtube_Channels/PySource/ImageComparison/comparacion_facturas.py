import cv2
import numpy as np
import glob
import argparse
from loguru import logger


# Construimos el parser:
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True,
                help="El path de la carpeta de imagenes a comparar.")
ap.add_argument("-t", "--template", required=True,
                help="El path del template en la carpeta de templates.")
args = vars(ap.parse_args())

path_to_compare = args["image"]
template = cv2.imread(args["template"])

# Como ya tenemos las direcciones de las carpetas, procedemos a cargar
# las imagenes que serán comparadas con el template escogido.
all_images_to_compare, titles = [], []
for file in glob.iglob(f"{path_to_compare}/*"):
    image = cv2.imread(file)
    # logger.info(file)
    all_images_to_compare.append(image)
    titles.append(file)
    # print(image_to_compare)
    # print(type(image_to_compare))

for image_to_compare, title in zip(all_images_to_compare, titles):
    # Primero, chequeamos que sean iguales:
    if image_to_compare.shape == template.shape:
        logger.info("Las dos imagenes tienen el mismo tamaño y canales")
        difference = cv2.subtract(image_to_compare, template)
        b, g, r = cv2.split(difference)

        if cv2.countNonZero(b) == 0 and cv2.countNonZero(g) == 0 and cv2.countNonZero(r) == 0:
            logger.info("Las imagenes son completamente iguales")

    else:
        width = template.shape[0]
        height = template.shape[1]
        logger.warning(f"Dimensiones del template--> {template.shape}")
        logger.warning(f"Dimensiones inciales de la imagen--> {image_to_compare.shape}")
        image_to_compare = cv2.resize(image_to_compare,
                                        (width, height),
                                        interpolation = cv2.INTER_AREA)

    aux_image_to_compare = image_to_compare[500:880, 50:460]
    # gray_image_to_compare = cv2.cvtColor(aux_image_to_compare, cv2.COLOR_BGR2GRAY)

    aux_template = template[500:880, 50:460]
    # gray_template = cv2.cvtColor(aux_template, cv2.COLOR_BGR2GRAY)

    # Luego, detectamos las similaridades entre las dos imagenes:
    # Usamos el Algoritmo SIFT para detectar los features.
    sift = cv2.xfeatures2d.SIFT_create()

    # Creamos key points y descriptors, para comparar las imagenes.
    key_points_1, descriptor_1 = sift.detectAndCompute(aux_image_to_compare, None)
    key_points_2, descriptor_2 = sift.detectAndCompute(aux_template, None)


    # Vamos a buscar una relacion porcentual en los matches:
    logger.info(f"Comparando con la imagen {title}")
    logger.info(f"La primera imagen tiene {len(key_points_1)}")
    logger.info(f"La segunda imagen tiene {len(key_points_2)}")

    # Creamos el flann con FLannBasedMatcher
    index_params = dict(algorithm = 0, trees = 5)
    search_params = dict()
    flann = cv2.FlannBasedMatcher(index_params, search_params)

    # Para tener los matches hacemos lo siguiente:
    matches = flann.knnMatch(descriptor_1, descriptor_2, k =2)

    # Queremos tener tan solo los mejores matches:
    good_points = []
    for m, n in matches:
        # Mientras mas grande sea la distancia, menor es la calidad del match:
        if m.distance < 0.6*n.distance:
            good_points.append(m)
    logger.info(f"Existen {len(good_points)} matches buenos.")

    # Ahora vamos a encontrar el % de esos matches:
    number_keypoints = 0
    if len(key_points_1) <= len(key_points_2):
        number_keypoints = len(key_points_1)
    else:
        number_keypoints = len(key_points_2)
    
    try:
        percentage_similarity = len(good_points) / number_keypoints * 100
        logger.info(f"El match tiene este porcentaje de coincidencia: {int(percentage_similarity)} %\n")

        # Vamos a mostrar los resultados:
        # 1) Si usamos drawMatchesKnn:
        # result = cv2.drawMatchesKnn(image_to_compare, key_points_1,
                                    # template, key_points_2,
                                    # matches, None)

        # 2) Si usamos drawMatches
        result = cv2.drawMatches(aux_image_to_compare, key_points_1,
                                    aux_template, key_points_2,
                                    good_points, None)

        cv2.imshow("Resultado", result)
        # cv2.imshow("Imagen a comparar", image_to_compare)
        # cv2.imshow("Template", template)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    except ZeroDivisionError:
        logger.warning("Numero divisor es igual a 0")
