import cv2
import numpy as np
import pytesseract
import os

pytesseract.pytesseract.tesseract_cmd = "C:\\Program Files\\Tesseract-OCR\\tesseract.exe"

imgQ = cv2.imread("query.png")
h, w, c = imgQ.shape
imgQ = cv2.resize(imgQ, (w//2, h//2))

# Nuestro detector orb
orb = cv2.ORB_create(1000)

# Nuestros KeyPoints (Puntos únicos de mi imagen)
kp1, des1 = orb.detectAndCompute(imgQ, None)
#impKp1 = cv2.drawKeypoints(imgQ, kp1, None)

# Este es el Path para los archivos en "UserForms"
path = "UserForms"
myPicList = os.listdir(path)
print(myPicList)

# El porcentaje que queremos buscar de matches va a ser el siguiente:
per = int(input("Que valor de porcentaje deseas de los matches?  "))

# Aqui vamos a tener una lista de regiones de interés:
# ¿Cómo encontré este valor? Con RegionSelector.py
roi = [[(44, 452), (316, 494), 'text', 'Name'],
        [(338, 446), (608, 494), 'text', 'Phone'],
        [(40, 524), (66, 550), 'box', 'Sign'],
        [(334, 526), (364, 548), 'box', 'Allergic'],
        [(42, 654), (312, 694), 'text', 'Email'],
        [(342, 648), (610, 694), 'text', 'ID'],
        [(44, 726), (314, 770), 'text', 'City'],
        [(340, 730), (614, 772), 'text', 'Country']]

# Haciendo un loop en los archivos que posee el directorio "UserForms"
for j, y in enumerate(myPicList):
    img = cv2.imread(path + "/" + y)
    #cv2.imshow(y, img)

    # Ahora, de nuevo voy a por los KeyPoints:
    kp2, des2 = orb.detectAndCompute(img, None)

    # Para encontrar el matcher, hacemos lo siguiente:
    bf = cv2.BFMatcher(cv2.NORM_HAMMING)
    matches = bf.match(des2, des1)

    # Podemos imprimir y ver como está funcionando el match
    # Esta funcion a continuacion, nos permite mapear o sortear todos los
    # valores de match basados en la "distancia"
    # Menor distancia --> Mejor es el match
    # Mayor distancia --> Peor es el match
    print("El tipo de variable de matches es:", type(matches))
    matches = sorted(matches, key= lambda x : x.distance)

    # Aqui, catalogamos los tipos de match en buenos y malos:
    good = matches[:int(len(matches)*(per/100))]

    imgMatch = cv2.drawMatches(img, kp2, imgQ, kp1, good[:100], None, flags=2)
    # cv2.imshow(y, imgMatch)

    srcPoints = np.float32([kp2[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
    dstPoints = np.float32([kp1[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)

    M, _ = cv2.findHomography(srcPoints, dstPoints, cv2.RANSAC, 5.0)
    imgScan = cv2.warpPerspective(img, M, (w,h))
    # cv2.imshow(y, imgScan)

    # Imprimimos nuestro proceso en nuestra imagen original
    # Para ello creamos una imagen que  sea del mismo tamaño que la original
    imgShow = imgScan.copy()

    # Luego creamos una mascara del mismo tamaño. 
    imgMask = np.zeros_like(imgShow)

    # Para esta imagen tenemos que hacer un loop sobre todas las regiones de interes:
    # Vamos a crear rectangulos en este mask y luego vamos a hacer overlay
    # en nuestra imagen original.
    for x, r in enumerate(roi):
        cv2.rectangle(imgMask, ( (r[0][0]), r[0][1] ), ( (r[1][0]), r[1][1] ), (0, 255, 0), cv2.FILLED )
        imgShow = cv2.addWeighted(imgShow, 0.99, imgMask, 0.1, 0)

    cv2.imshow(y + "2", imgShow)


#cv2.imshow("Key Points", impKp1)
cv2.imshow("Output", imgQ)
cv2.waitKey(0)