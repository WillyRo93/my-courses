BUSCAR:

-Orb Python (Que es? Que significa? Como se usa?)
    orb = cv2.ORB_create(1000)
    orb.detectAndCompute
    impKp1 = cv2.drawKeypoints

- OpenCV:
    bf = cv2.BFMatcher(cv2.NORM_HAMMING)
    imgMatch = cv2.drawMatches(img, kp2, imgQ, kp1, good, None, flags=2)
    M, _ = cv2.findHomography(srcPoints, dstPoints, cv2.RANSAC, 5.0)

- Numpy:
    srcPoints = np.float32([kp2[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)