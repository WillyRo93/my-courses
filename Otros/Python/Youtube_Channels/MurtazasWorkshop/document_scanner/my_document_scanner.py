# Este es mi Scanner personalizado:

import cv2
import numpy as np
import utlis
from PIL import Image

pathImage = "1.jpg"
# pathImage = "scan_02.jpg"
# pathImage = "test.jpg"
pathImage = "rotated.jpg"

image = Image.open(pathImage)
width, height = image.size

heightImg = height
widthImg  = width

utlis.initializeTrackbars()
count, i = 0, 0

while i >= 0:
    img = cv2.imread(pathImage)
    cv2.namedWindow("img", cv2.WINDOW_NORMAL)
    cv2.imshow("img", img)

    imgBlank = np.zeros((heightImg, widthImg, 3), np.uint8)
    cv2.namedWindow("imgBlank", cv2.WINDOW_NORMAL)
    cv2.imshow("imgBlank", imgBlank)

    imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    cv2.namedWindow("imgGray", cv2.WINDOW_NORMAL)
    cv2.imshow("imgGray", imgGray)

    imgBlur = cv2.GaussianBlur(imgGray, (5, 5), 1)
    cv2.namedWindow("imgBlur", cv2.WINDOW_NORMAL)
    cv2.imshow("imgBlur", imgBlur)

    thres=utlis.valTrackbars()
    imgThreshold = cv2.Canny(imgBlur,thres[0],thres[1])
    cv2.namedWindow("imgThreshold", cv2.WINDOW_NORMAL)
    cv2.imshow("imgThreshold", imgThreshold)

    kernel = np.ones((5, 5))
    imgDial = cv2.dilate(imgThreshold, kernel, iterations=2)
    cv2.namedWindow("imgDial", cv2.WINDOW_NORMAL)
    cv2.imshow("imgDial", imgDial)

    imgThreshold = cv2.erode(imgDial, kernel, iterations=1)
    cv2.namedWindow("imgThreshold2", cv2.WINDOW_NORMAL)
    cv2.imshow("imgThreshold2", imgThreshold)

    imgContours = img.copy()
    imgBigContour = img.copy()
    contours, hierarchy = cv2.findContours(imgThreshold, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cv2.drawContours(imgContours, contours, -1, (0, 255, 0), 10)
    cv2.namedWindow("imgContours", cv2.WINDOW_NORMAL)
    cv2.imshow("imgContours", imgContours)

    biggest, maxArea = utlis.biggestContour(contours)

    if biggest.size != 0:
        biggest=utlis.reorder(biggest)
        cv2.drawContours(imgBigContour, biggest, -1, (0, 255, 0), 20) # DRAW THE BIGGEST CONTOUR
        imgBigContour = utlis.drawRectangle(imgBigContour, biggest, 2)
        cv2.namedWindow("imgBigContour", cv2.WINDOW_NORMAL)
        cv2.imshow("imgBigContour", imgBigContour)

        pts1 = np.float32(biggest) # PREPARE POINTS FOR WARP
        pts2 = np.float32([[0, 0],[widthImg, 0], [0, heightImg],[widthImg, heightImg]]) # PREPARE POINTS FOR WARP
        matrix = cv2.getPerspectiveTransform(pts1, pts2)
        imgWarpColored = cv2.warpPerspective(img, matrix, (widthImg, heightImg))
        #cv2.drawContours(imgWarpColored, contours, -1, (0, 255, 0), 10)
        cv2.namedWindow("imgWarpColored", cv2.WINDOW_NORMAL)
        cv2.imshow("imgWarpColored", imgWarpColored)

        imgWarpGray = cv2.cvtColor(imgWarpColored,cv2.COLOR_BGR2GRAY)
        cv2.namedWindow("imgWarpGray", cv2.WINDOW_NORMAL)
        cv2.imshow("imgWarpGray", imgWarpGray)

        imgAdaptiveThre= cv2.adaptiveThreshold(imgWarpGray, 255, 1, 1, 7, 2)
        imgAdaptiveThre = cv2.bitwise_not(imgAdaptiveThre)
        imgAdaptiveThre=cv2.medianBlur(imgAdaptiveThre,3)
        cv2.namedWindow("imgAdaptiveThre", cv2.WINDOW_NORMAL)
        cv2.imshow("imgAdaptiveThre", imgAdaptiveThre)

    else:
        cv2.imshow("No hay resultado", imgBlank)

    if cv2.waitKey(1) & 0xFF == ord("s"):
        cv2.imwrite("Scanned/myImage"+str(count)+".jpg",imgWarpColored)
        count += 1

    cv2.waitKey(30)