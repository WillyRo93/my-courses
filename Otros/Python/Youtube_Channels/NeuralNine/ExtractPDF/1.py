from ast import pattern
import re
from pdfminer.high_level import extract_pages, extract_text
import glob

# for page_layout in extract_pages("85598532.pdf"):
#     for element in page_layout:
#         #print(element)
#         pass

# text = extract_text("85598532.pdf")
# print(text)

# Una forma de encontrar los valores es creando una lista con split
# Luego buscar el index siguiente valor y listo, tenemos el valor.
# splited_text = text.split()
# my_index = splited_text.index("N.I.C.:")
# print(splited_text[my_index + 1])


# La segunda forma de encontrar los valores es con regex:
# pattern = re.compile(r"N.I.C.:\s+(\S+)")
# matches = pattern.findall(text)
# print(matches)

def search_pattern(file):
    text = extract_text(f"{file}")
    # print(text)

    print(f"Entrando al archivo {file}")

    # Numero N.I.C
    nic_pattern = re.compile(r"N.I.C.:\s+(\S+)")
    nic_match = nic_pattern.findall(text)
    print(f"El N.I.C es: {nic_match[0]}")

    
    emision_pattern = re.compile(r"Emisión:\s+(\S+)")
    emision_match = emision_pattern.findall(text)
    print(f"La fecha de emision es: {emision_match[0]}")

    aviso_pattern = re.compile(r"Aviso de Cobro nº:\s+(\S+)")
    aviso_match = aviso_pattern.findall(text)
    try:
        print(f"El Aviso de cobro es: {aviso_match[0]}")
    except:
        print("No hay Aviso de cobro en este caso")

    fact_pattern = re.compile(r"Factura:\s+(\S+)")
    fact_match = fact_pattern.findall(text)
    try:
        print(f"El numero de factura es: {fact_match[0]}")
    except IndexError:
        print(f"No hay Numero de Factura en este caso")
    
    rif_pattern = re.compile(r"CI/RIF:\s+(\S+)")
    rif_match = rif_pattern.findall(text)
    print(f"La cedula o RIF es: {rif_match[0]}")

    emision_pattern = re.compile(r"Emisión:\s+(\S+)")
    emision_match = emision_pattern.findall(text)
    print(f"La Fecha de Emision es: {emision_match[0]}")

    vencim_pattern = re.compile(r"Vencimiento:\s+(\S+)")
    vencim_match = vencim_pattern.findall(text)
    print(f"La Fecha de Vencimiento es: {vencim_match[0]}")

    concept_pattern = re.compile(r"(?s)(?<=Deuda Anterior Bs:).*?(?=SUB-TOTAL)")
    concept_match = concept_pattern.findall(text)
    print(concept_match)
    concept_match = concept_match[0]
    concepts = len(concept_match.split("\n\n"))
    print(concept_match.split("\n\n"))
    print(f"Este documento posee {concepts}: conceptos")
    


    print("\n")

i = 0
for file in glob.iglob("pdfs/*"):
    search_pattern(file)
    # break
    i = i + 1
    if i == 5:
        break