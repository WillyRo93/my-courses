import random
import json
import pickle
import numpy as np
import tensorflow

#Natual Language
import nltk
# Importamos WordNetLemmatizer, con esto palabras como "comí", "comió", "comer" son lo mismo
from nltk.stem import WordNetLemmatizer

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Activation, Dropout
from tensorflow.keras.optimizers import SGD

lemmatizer = WordNetLemmatizer()

intents = json.loads (open ("01_intents.json").read() )

# Creamos tres listas vacías, las palabras, las clases y los documentos:
words = []
classes = []
documents = []

# Le decimos los caracteres que vamos a ignorar:
ignore_letter = ["?", "!", ".", ","]

for intent in intents["intents"]:
	for pattern in intents["patterns"]:
		# Aqui, con tokenize lo divimos en palabras individuales
		word_list = nltk.tokenize(pattern)
		words.extend(word_list)
		documment.append( ( word_list, intent["tag"] ) )

		if intent["tag"] not in classes:
			classes.append(intent["tag"])

print(documents)