import qrcode

#qr = qrcode.make("Hablame mano todo bien?")
#qr.save("myQR.png")

# Jugando con las configuraciones tenemos lo siguiente:

qr = qrcode.QRCode (version = 1,
					error_correction = qrcode.constants.ERROR_CORRECT_L,
					box_size = 20,
					border = 5)

qr.add_data ("https://www.neuraline.com/books")
qr.make (fit = True)

img = qr.make_image (fill_color = "black", back_color = "white")
img.save ("advanced.png")


# Enviar un correo a alguien:
<a href="mailto:wrodriguez@netnovation.com">

# Enviar correo a mas de una persona:
<a href="mailto:wrodriguez@netnovation.com,aantunes@netnovation.com">

# Enviar un mensaje a William, poner en CC a Alejandro y en BCC a otra persona:
<a href="mailto:wrodriguez@netnovation.com?cc=aantunes@netnovation.com&bcc=otrapersona@netnovation.com">

# Enviar correo con subject:
<a href="mailto:wrodriguez@netnovation.com?subject=Hola%20que%20tal%20bro?">

# Enviar correo con subject y mensaje:
<a href="mailto:wrodriguez@netnovation.com?subject=Hola%20que%20tal%20bro?&body=Disfruta%20de%20tus%20vacaciones.%0ASaludos.%20">