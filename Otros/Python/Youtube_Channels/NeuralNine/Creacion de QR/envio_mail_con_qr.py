import qrcode

data = """<a href="mailto:wrodriguez@netnovation.com?subject=Hola%20que%20tal%20bro?&body=Disfruta%20de%20tus%20vacaciones.%0ASaludos.%20">"""

qr = qrcode.make(data)
qr.save("QR"+".png")

# Remember to use %20 for the space character and %0A to add a line break in the body text. 
# You can also test any mailto command by directly typing the command in your browser’s address bar.

# url: https://github.com/somdipdey/Encrypted_QR_Code