# Con esto convierto las imagenes en archivos PDF O HOCR:

try:
    from PIL import Image
except ImportError:
    import Image
import pytesseract

# Get a searchable PDF
pdf = pytesseract.image_to_pdf_or_hocr('bcv_petro_etc.jpg', extension='pdf')
with open('bcv_petro_etc.pdf', 'w+b') as f:
    f.write(pdf) # pdf type is bytes by default


# Por alguna razon no corre esta parte del codigo:
# Get HOCR output
hocr = pytesseract.image_to_pdf_or_hocr('bcv_petro_etc.jpg', extension='hocr')