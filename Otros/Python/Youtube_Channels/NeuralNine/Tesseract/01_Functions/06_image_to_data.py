try:
    from PIL import Image
except ImportError:
    import Image
import pytesseract

# Configuracion de como leeremos la imagen:
my_config = r"--psm 11 --oem 3"

print(pytesseract.image_to_data(Image.open('bcv_petro_etc.jpg')))