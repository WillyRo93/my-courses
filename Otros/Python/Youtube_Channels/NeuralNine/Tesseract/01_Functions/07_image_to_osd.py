# Por alguna razon, este codigo da error:

try:
    from PIL import Image
except ImportError:
    import Image
import pytesseract

# Configuracion de como leeremos la imagen:
my_config = r"--psm 11 --oem 3"

# Get information about orientation and script detection
print(pytesseract.image_to_osd(Image.open('bcv_petro_etc.jpg')), config = my_config)