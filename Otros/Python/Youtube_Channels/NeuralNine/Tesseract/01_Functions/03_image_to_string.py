# Con este codigo entenderemos el funcionamiento de la funcion image_to_string

import pytesseract
import PIL.Image
import cv2

# Configuracion de como leeremos la imagen:
my_config = r"--psm 11 --oem 3"

#text = pytesseract.image_to_string(PIL.Image.open("1642721151.jpg"), config = my_config)

text = pytesseract.image_to_string(PIL.Image.open("bcv_petro_etc.jpg"), config = my_config)

#text = pytesseract.image_to_string(PIL.Image.open("paypal_skrill_uphold_amazon.jpg"), config = my_config)

#text = pytesseract.image_to_string(PIL.Image.open("promedio.jpg"), config = my_config)
#text = pytesseract.image_to_data(PIL.Image.open("promedio.jpg"), config = my_config)

# Aqui estan las imagenes filtradas por Alejandro:
#text = pytesseract.image_to_string(PIL.Image.open("Filtro_Alejandro.jpeg"), config = my_config)

#text = pytesseract.image_to_string(PIL.Image.open("Filtro_Alejandro_2.jpeg"), config = my_config)

# Aqui se lee la imagen erosionada:
#text = pytesseract.image_to_string(PIL.Image.open("erode.jpg"), config = my_config)

print(text)

print(pytesseract.get_languages(config=''))