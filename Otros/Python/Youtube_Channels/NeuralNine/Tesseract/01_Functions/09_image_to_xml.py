# Con esto convierto las imagenes en archivos XML:

try:
    from PIL import Image
except ImportError:
    import Image
import pytesseract

# Get ALTO XML output
xml = pytesseract.image_to_alto_xml('bcv_petro_etc.jpg')
print(xml)