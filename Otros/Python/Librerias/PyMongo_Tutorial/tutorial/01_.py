import pymongo
from pymongo import MongoClient

# Aqui hago la conexion a mi cluster:
cluster = MongoClient("blah blah")

# Que base de datos queremos? La siguiente:
db = cluster["test"]

# Ahora, que colexion queremos usar? A continuacion lo vemos:
collection = db["test"]

# Como añadimos algo?
collection.insert_one({})