import os
import time
from blabel import LabelWriter

label_writer = LabelWriter("item_template.html",
                           default_stylesheets=("style.css",))

records = []

rango = 2
count = 0
for i in range(rango):
    records.append(dict(sample_id = "s00000" + str(i), sample_name = "Caja " + str(i+1) + " de " + str(rango)))

print(records)

label_writer.write_labels(records, target='numero_de_cajas.pdf')

time.sleep(5)

#os.startfile("numero_de_cajas.pdf", "print")

# Esto me devuelve:
# [{'sample_id': 's00', 'sample_name': 'Caja 1 de 2'}, {'sample_id': 's01', 'sample_name': 'Caja 2 de 2'}]