import time
from blabel import LabelWriter

label_writer = LabelWriter("item_template.html",
                           default_stylesheets=("style.css",))

mi_batch = "20211112-A-Pale"
texto1= "Contenido del batch: 45 botellas de 220mL, 67 botellas de 620mL"
texto2= "Contenido del batch: 45 botellas de 220mL, 67 botellas de 620mL"

records= [
    dict(vacio = "", sample_id= texto1, batch = "Batch: 20211112-A-Pale", botl= "Fecha Embotellamiento: 12/11/2021", vend= "Fecha de venta: 28/11/2021", caja = "Caja 1 de 2"),
    dict(vacio = "", sample_id= texto1, batch = "20211112-A-Pale", botl= "Fecha Embotellamiento: 12/11/2021", vend= "Fecha de venta: 28/11/2021", caja = "Caja 2 de 2")
]

label_writer.write_labels(records, target='prueba.pdf')

# time.sleep(5)

# os.startfile("prueba.pdf", "print")