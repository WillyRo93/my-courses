# Para garantizar que lea bien los parametros, debemos tener cuidado
# Por ejemplo, si quiero tener paths como los siguientes:
# |
#  --- /users/me
# |
#  ---/users/{user_id}

# Donde el primer path me da info del usuario actual (me).
# Y el segundo path me da la info de un usuario segun su ID.

# El orden de las funciones debe ser el siguiente, para que el codigo no "piense"
# que "/users/me" es uno de los IDs.

from fastapi import FastAPI

app = FastAPI()

# Usar:
# http://127.0.0.1:8000/users/me
@app.get("/users/me")
async def read_user_me():
    return {"user_id": "the current user"}


# Usar:
# http://127.0.0.1:8000/users/34
@app.get("/users/{user_id}")
async def read_user(user_id: str):
    return {"user_id": user_id}
