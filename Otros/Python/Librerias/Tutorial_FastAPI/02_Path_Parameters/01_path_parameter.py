from fastapi import FastAPI

app = FastAPI()

# Con esto tenemos una variable denominada item_id

@app.get("/items/{item_id}")
async def read_item(item_id):
    return {"item_id": item_id}

# Si entramos al sitio: http://127.0.0.1:8000/items/foo

# Tendremos el siguiente valor como respuesta:
# {"item_id":"foo"}