from fastapi import FastAPI

app = FastAPI()

# Si queremos valores por ejemplo de tipo integer, hacemos lo sig:

@app.get("/items/{item_id}")
async def read_item(item_id: int):
    return {"item_id": item_id}

# Si entramos al sitio: http://127.0.0.1:8000/items/3

# Tendremos el siguiente valor como respuesta:
# {"item_id":3}