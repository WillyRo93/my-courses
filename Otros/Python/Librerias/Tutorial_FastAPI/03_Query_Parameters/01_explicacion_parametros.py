# Cuando se declaran otros function parameters que no son parte de los path parameters (?)
# Son automaticamente interpretados como parametros "query"

from fastapi import FastAPI

app = FastAPI()

fake_items_db = [{"item_name": "Foo"}, {"item_name": "Bar"}, {"item_name": "Baz"}]


@app.get("/items/")
async def read_item(skip: int = 0, limit: int = 10):
    return fake_items_db[skip : skip + limit]

# Un query es un grupo de key-value pairs que van despues del "?" en la URL, separados por un "&"
    # EJEMPLO: http://127.0.0.1:8000/items/?skip=0&limit=10
    # Aquí, los parametros del query son:
        # skip: Con un valor de 0.
        # limit: COn un valor de 10.


# Sin importar que pase, los valores por default serán:
#   skip = 0
#   limit = 10

# Por lo tanto, entrar en http://127.0.0.1:8000/items/ es igual a entrar en el link anterior.

# NOTA:
    # Pero en cambio, si vamos al siguiente URL: http://127.0.0.1:8000/items/?skip=20
    # skip = 20
    # limit = 10