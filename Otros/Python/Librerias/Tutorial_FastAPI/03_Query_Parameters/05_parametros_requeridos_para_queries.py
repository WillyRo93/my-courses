from fastapi import FastAPI

app = FastAPI()


@app.get("/items/{item_id}")
async def read_user_item(item_id: str, needy: str):
    item = {"item_id": item_id, "needy": needy}
    return item

# Aqui el parametro "needy" es requerido.

# Si abrimos la siguiente pagina nos daria error si tan solo tuviesemos esta expresion o funcion guardada:
# http://127.0.0.1:8000/items/foo-item

# Pero para ver su funcionamiento, podemos verla de la siguiente manera:
# http://127.0.0.1:8000/items/foo-item?needy=sooooneedy

# -----------------------------------------------------------------------------------------------------------------
# Tambien, por supuesto, podemos definir algunos parametros como obligatorios, algunos como default, y otros opcionales:

from fastapi import FastAPI

app = FastAPI()


@app.get("/items/{item_id}")
async def read_user_item(
    item_id: str, needy: str, skip: int = 0, limit: int | None = None
):
    item = {"item_id": item_id, "needy": needy, "skip": skip, "limit": limit}
    return item
