# Como hacer un cambio de tipo de variable a los query parameters?

# Para Python 3.6 en adelante:
from typing import Optional

from fastapi import FastAPI

app = FastAPI()


@app.get("/items/{item_id}")
async def read_item(item_id: str, q: Optional[str] = None, short: bool = False):
    item = {"item_id": item_id}
    if q:
        item.update({"q": q})
    if not short:
        item.update(
            {"description": "This is an amazing item that has a long description"}
        )
    return item

# ----------------------------------------------------------------

# Para Python 3.10 en adelante:

from fastapi import FastAPI

app = FastAPI()


@app.get("/items/{item_id}")
async def read_item(item_id: str, q: str | None = None, short: bool = False):
    item = {"item_id": item_id}
    if q:
        item.update({"q": q})
    if not short:
        item.update(
            {"description": "This is an amazing item that has a long description"}
        )
    return item

# En este caso, si vamos a las siguientes paginas:

    # http://127.0.0.1:8000/items/foo?short=1
    # http://127.0.0.1:8000/items/foo?short=True
    # http://127.0.0.1:8000/items/foo?short=true
    # http://127.0.0.1:8000/items/foo?short=on
    # http://127.0.0.1:8000/items/foo?short=yes

# Siempre vamos a encontrar al valor "short" del URL como True

# NOTA:
    # Para hacer que funcione, debemos comentar el codigo que pide integers
    # Es decir, 02_Path_Parameters/01_path_parameter.py