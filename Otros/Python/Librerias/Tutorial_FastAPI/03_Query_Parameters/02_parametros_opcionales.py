# Para Python 3.6 en adelante:

from typing import Optional

from fastapi import FastAPI

app = FastAPI()


@app.get("/items/{item_id}")
async def read_item(item_id: str, q: Optional[str] = None):
    if q:
        return {"item_id": item_id, "q": q}
    return {"item_id": item_id}

# ----------------------------------------------------------------

# Para Python 3.10 en adelante:

from fastapi import FastAPI

app = FastAPI()


@app.get("/items/{item_id}")
async def read_item(item_id: str, q: str | None = None):
    if q:
        return {"item_id": item_id, "q": q}
    return {"item_id": item_id}

# Aqui podemos ver que la function parameter q será opcional y por default será None.

# NOTA:
    # Debe notarse que item_id es el path parameter y q es un query parameter