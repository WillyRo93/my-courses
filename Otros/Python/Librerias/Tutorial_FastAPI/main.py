# Para correr el codigo -----> uvicorn main:app --reload

from enum import Enum
from fastapi import FastAPI

class ModelName(str, Enum):
    alexnet = "alexnet"
    resnet = "resnet"
    lenet = "lenet"

fake_items_db = [{"item_name": "Foo"}, {"item_name": "Bar"}, {"item_name": "Baz"}]

app = FastAPI()

# ----------------------------------------------------------------------------
                            # ------------- #
                            # 01_First_Steps
                            # ------------- #

# ----------------------------------------
        # 01_llamando_main.py

@app.get("/")
async def root():
    return {"message": "Hello World"}

# ----------------------------------------------------------------------------


                            # ----------------- #
                            # 02_Path_Parameters
                            # ----------------- #

# ----------------------------------------
        # 01_path_parameter.py

# NOTA: Comentamos esta parte del codigo porque obliga a que los valores sean integer
#       Y no siempre es necesario tener esa restriccion.

#@app.get("/items/{item_id}")
#async def read_item(item_id: int):
#    return {"item_id": item_id}

# ----------------------------------------
        # 02_path_parameter_with_types.py

@app.get("/users/me")
async def read_user_me():
    return {"user_id": "the current user"}

# ----------------------------------------
        # 03_order_of_parameters.py

@app.get("/users/{user_id}")
async def read_user(user_id: str):
    return {"user_id": user_id}


# ----------------------------------------
        # 04_enumerar_valores.py

@app.get("/models/{model_name}")
async def get_model(model_name: ModelName):
    if model_name == ModelName.alexnet:
        return {"model_name": model_name, "message": "Deep Learning FTW!"}

    if model_name.value == "lenet":
        return {"model_name": model_name, "message": "LeCNN all the images"}

    return {"model_name": model_name, "message": "Have some residuals"}


# ----------------------------------------------------------------------------

                            # ------------------- #
                            # 03_Query_Parameters/
                            # ------------------- #

# ----------------------------------------
# 01_explicacion_parametros.py

@app.get("/items/")
async def read_item(skip: int = 0, limit: int = 10):
    return fake_items_db[skip : skip + limit]

# ----------------------------------------
# 02_parametros_opcionales.py

@app.get("/items/{item_id}")
async def read_item(item_id: str, q: str | None = None):
    if q:
        return {"item_id": item_id, "q": q}
    return {"item_id": item_id}

# ----------------------------------------
# 03_conversion_query_parameters.py

@app.get("/items/{item_id}")
async def read_item(item_id: str, q: str | None = None, short: bool = False):
    item = {"item_id": item_id}
    if q:
        item.update({"q": q})
    if not short:
        item.update(
            {"description": "This is an amazing item that has a long description"}
        )
    return item

# ----------------------------------------
# 04_multiples_parametros_de_paths_y_queries.py

@app.get("/users/{user_id}/items/{item_id}")
async def read_user_item(
    user_id: int, item_id: str, q: str | None = None, short: bool = False
):
    item = {"item_id": item_id, "owner_id": user_id}
    if q:
        item.update({"q": q})
    if not short:
        item.update(
            {"description": "This is an amazing item that has a long description"}
        )
    return item

# ----------------------------------------
# 05_parametros_requeridos_para_queries.py

@app.get("/items/{item_id}")
async def read_user_item(item_id: str, needy: str):
    item = {"item_id": item_id, "needy": needy}
    return item

# ----------------------------------------------------------------------------