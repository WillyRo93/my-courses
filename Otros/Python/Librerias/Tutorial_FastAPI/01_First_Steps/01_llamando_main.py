# Primero importamos fastapi y luego creamos la app:

from fastapi import FastAPI

app = FastAPI()


@app.get("/")
async def root():
    return {"message": "Hello World"}

# Finalmente para correr el codigo, corremos el siguiente script:
# uvicorn main:app --reload