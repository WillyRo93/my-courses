# Link Video: https://www.youtube.com/watch?v=wSjB_iaQ1wE&list=PLS1QulWo1RIa7D1O6skqDQ-JZ1GGHKK-K&index=11

import numpy as np
import cv2

direccion_oficina = "D:/Usuarios/wrodriguez/Downloads/Cursos_Gitlab/my-courses/Otros/OpenCV/ProgrammingKnowledge/03_Formas_Geometricas/monitor7.jpg"
img = cv2.imread(direccion_oficina, 1)

# Esto retorna una tupla con numero de filas, columnas y canales(?):
print(img.shape)

# Esto retorna el numero total de Pixeles:
print(img.size)

# Esto retorna el datatype obtenido:
print(img.dtype)

b, g, r = cv2.split(img)
img = cv2.merge( (b, g, r) )

# Asumimos que ya sabemos la localizacion del balon, y queremos copiarlo en otra parte:
ball = img [280:340, 330:390]



cv2.imshow( "Image", img)
cv2.waitKey(0)
cv2.destroyAllWindows()