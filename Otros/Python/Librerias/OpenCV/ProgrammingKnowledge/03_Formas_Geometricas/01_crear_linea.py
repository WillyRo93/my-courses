import numpy as np
import cv2

direccion_oficina = "D:/Usuarios/wrodriguez/Downloads/Cursos_Gitlab/my-courses/Otros/OpenCV/ProgrammingKnowledge/03_Formas_Geometricas/monitor7.jpg"

img = cv2.imread(direccion_oficina, 1)

# Como dibujar una linea?
# Parametros:
#   1) img: Imagen
#   2) (0,0): Pimer punto de la linea.
#   3) (255, 255): Segundo punto de la linea.
#   4) (0, 0, 255): Color a mostrar.
#       Los colores son:
#       Azul (255, 0, 0).
#       Verde  (0, 255, 0).
#       Rojo  (0, 0, 255).
#       Si se desea cualquier otro color, podemos buscar "rgb color picker" en Google.
#   5) 5: Grosor de la linea.
img = cv2.line(img, (0, 0), (255, 255), (16, 119, 222), 5) # 222, 119, 16 (Naranja)
cv2.imshow("image", img)

cv2.waitKey(0)
cv2.destroyAllWindows()

