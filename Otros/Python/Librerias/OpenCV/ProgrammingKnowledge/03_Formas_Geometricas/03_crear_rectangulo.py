import numpy as np
import cv2

direccion_oficina = "D:/Usuarios/wrodriguez/Downloads/Cursos_Gitlab/my-courses/Otros/OpenCV/ProgrammingKnowledge/03_Formas_Geometricas/monitor7.jpg"

img = cv2.imread(direccion_oficina, 1)

# Como dibujar un rectangulo?.

# Aqui tenemos como esta estructurado nuestro triangulo
# x1, Y1 -------- 
#                |
#                |
#                |
#------------- x2, y2

#                         (x1, y1)   (x2, y2)
img = cv2.rectangle( img, (384, 0), (510, 128), (0,0,255), 5 )

# Como dato: Si quisieramos rellenar de un color alguna zona,
# en thicknes o grosor colocariamos -1, es decir:
#img = cv2.rectangle( img, (384, 0), (510, 128), (0,0,255), -1 )

cv2.imshow("image", img)

cv2.waitKey(0)
cv2.destroyAllWindows()