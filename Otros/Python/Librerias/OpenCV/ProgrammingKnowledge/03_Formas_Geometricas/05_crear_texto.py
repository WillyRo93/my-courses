import numpy as np
import cv2

direccion_oficina = "D:/Usuarios/wrodriguez/Downloads/Cursos_Gitlab/my-courses/Otros/OpenCV/ProgrammingKnowledge/03_Formas_Geometricas/monitor7.jpg"

img = cv2.imread(direccion_oficina, 1)

# Como crear texto?
# Primero se establece el font:
font = cv2.FONT_HERSHEY_DUPLEX
#                       Tetxo     Starting Point   FontFace   Tamaño      Color      Thickness    Line Type
img = cv2.putText(img, "OpenCV",   (10, 500),        font,      4,     (0, 255, 0),     7,        cv2.LINE_8)

cv2.imshow("image", img)

cv2.waitKey(0)
cv2.destroyAllWindows()