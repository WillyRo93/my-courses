# Para crear una imagen, especificamente en un solo color, por ahora:

import numpy as np
import cv2

img = np.zeros( [512, 512, 3], np.uint8 )

# Esta linea que se muestra, está explicada mas adelante:
img = cv2.line(img, (0, 0), (255, 255), (16, 119, 222), 5)

cv2.imshow( "Image", img)

cv2.waitKey(0)
cv2.destroyAllWindows()