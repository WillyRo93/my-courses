# Que vamos a hacer acá? Vamos a buscar los distintos colores que se
# puedan observar en la Imagen de Tasas en Paralelo

import cv2
import numpy as np

direccion_oficina = "D:/Usuarios/wrodriguez/Downloads/Cursos_Gitlab/my-courses/Otros/OpenCV/ProgrammingKnowledge/03_Formas_Geometricas/monitor7.jpg"

#direccion_oficina = "D:/Usuarios/wrodriguez/Downloads/Cursos_Gitlab/my-courses/Otros/OpenCV/ProgrammingKnowledge/05_HSV(Hue, Saturation and Value)/factura.jpeg"

def nothing (x):
    pass

cv2.namedWindow( "Buscando por color" )

cv2.createTrackbar( "Low Hue", "Buscando por color", 0, 255, nothing )
cv2.createTrackbar( "Low Saturation", "Buscando por color", 0, 255, nothing )
cv2.createTrackbar( "Low Value", "Buscando por color", 0, 255, nothing )

cv2.createTrackbar( "Up Hue", "Buscando por color", 255, 255, nothing )
cv2.createTrackbar( "Up Saturation", "Buscando por color", 255, 255, nothing )
cv2.createTrackbar( "Up Value", "Buscando por color", 255, 255, nothing )

while True:
    frame = cv2.imread( direccion_oficina )
    
    hsv = cv2.cvtColor( frame, cv2.COLOR_BGR2HSV )

    low_hue = cv2.getTrackbarPos( "Low Hue", "Buscando por color")
    low_saturation = cv2.getTrackbarPos( "Low Saturation", "Buscando por color")
    low_value = cv2.getTrackbarPos( "Low Value", "Buscando por color")

    up_hue = cv2.getTrackbarPos( "Up Hue", "Buscando por color")
    up_saturation = cv2.getTrackbarPos( "Up Saturation", "Buscando por color")
    up_value = cv2.getTrackbarPos( "Up Value", "Buscando por color")

    menor_color = np.array( [low_hue, low_saturation, low_value] )
    mayor_color = np.array( [up_hue, up_saturation, up_value] )

    # Hacemos un threshold para obtener solo los colores rojos:
    mask = cv2.inRange( hsv, menor_color, mayor_color)

    res = cv2.bitwise_and( frame, frame, mask = mask )


    cv2.imshow( "frame" , frame )

    cv2.imshow( "mask" , mask )

    cv2.imshow( "res" , res )

    key = cv2.waitKey(1)
    if key == 27:
        break

cv2.destroyAllWindows()