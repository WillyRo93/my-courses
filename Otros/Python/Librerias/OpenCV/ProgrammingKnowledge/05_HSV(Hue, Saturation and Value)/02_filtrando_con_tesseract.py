# Aqui vamos a intentar leer la imagen tratada con anterioridad:

#direccion = "D:/Usuarios/wrodriguez/Downloads/Cursos_Gitlab/my-courses/Otros/OpenCV/ProgrammingKnowledge/05_HSV(Hue, Saturation and Value)/Tasa_Dolar_Tratada.jpeg"
#direccion = "D:/Usuarios/wrodriguez/Downloads/Cursos_Gitlab/my-courses/Otros/OpenCV/ProgrammingKnowledge/05_HSV(Hue, Saturation and Value)/Tasa_Dolar.jpeg"
direccion = "D:/Usuarios/wrodriguez/Downloads/Cursos_Gitlab/my-courses/Otros/OpenCV/ProgrammingKnowledge/05_HSV(Hue, Saturation and Value)/lero.jpeg"

import pytesseract
import PIL.Image
import cv2

# Configuracion de como leeremos la imagen:
my_config = r"--psm 3 --oem 3"

text = pytesseract.image_to_string(PIL.Image.open(direccion), config = my_config)

print(text)