import cv2
import numpy as np

import pytesseract
import PIL.Image

# Configuracion de como leeremos la imagen:
my_config = r"--psm 11 --oem 3"

frame = cv2.imread("monitor7.jpg")

# It converts the BGR color space of image to HSV color space
hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
     
# Threshold of blue in HSV space
lower_blue = np.array([0, 0, 110])
upper_blue = np.array([255, 210, 255])
 
# preparing the mask to overlay
mask = cv2.inRange(hsv, lower_blue, upper_blue)
imagen_tratada = cv2.imwrite("mask.png", mask)

# The black region in the mask has the value of 0,
# so when multiplied with original image removes all non-blue regions
result = cv2.bitwise_and(frame, frame, mask = mask)


text = pytesseract.image_to_string(PIL.Image.open("mask.png"), config = my_config)
print(text)

cv2.imshow('frame', frame)
cv2.imshow('mask', mask)
cv2.imshow('result', result)
     
cv2.waitKey(0)
 
cv2.destroyAllWindows()
