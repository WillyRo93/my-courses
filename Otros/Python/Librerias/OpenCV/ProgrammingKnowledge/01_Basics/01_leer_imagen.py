# Como leer una imagen con OpenCV?

import cv2

# Con este codigo leemos la imagen, el segundo valor indica como se lee:
#	- 0: Imagen leida a escala de grises
#	- 1: Imagen leida a color
#	- 2: Imagen leida incluyendo un "alpha channel"
img = cv2.imread("monitor7.jpg", 0)

print(img)