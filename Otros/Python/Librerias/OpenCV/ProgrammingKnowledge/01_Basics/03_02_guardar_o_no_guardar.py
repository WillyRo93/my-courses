# Aqui veremos como usar las teclas para decidir si guardar o no la imagen:
import cv2

img = cv2.imread("monitor7.jpg", 0)

print(img)

cv2.imshow("image", img)

# Establecemos que k obtendrá la reaccion del teclado:
k = cv2.waitKey(0)

# Si k == 27, significa que se usó la tecla "ESC"
if k == 27:
	cv2.destroyAllWindows()

# Si k == s, significa que se usó la tecla "S"
elif k == ord("s"):
	cv2.imwrite("monitor7_tecla.jpg", img)
	cv2.destroyAllWindows()