# Como se muestra una imagen?
import cv2

img = cv2.imread("monitor7.jpg", 0)

print(img)

# Una vez que ya tenemos la configuracion, mostramos la imagen:
cv2.imshow("image", img)

# Debemos colocar un tiempo de espera, en este caso 5 seg:
cv2.waitKey(0)

# Finalmente destruimos todas las ventanas creadas:
cv2.destroyAllWindows()