# Como guardar una imagen?

import cv2

img = cv2.imread("monitor7.jpg", 0)

print(img)

cv2.imshow("image", img)
cv2.waitKey(0)
cv2.destroyAllWindows()

# Una vez tenemos todo lo anterior configurado, decidimos guardar la imagen:
cv2.imwrite("monitor7_copia.jpg", img)