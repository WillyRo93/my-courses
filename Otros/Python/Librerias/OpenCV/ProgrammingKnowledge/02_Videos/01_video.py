import cv2

cap = cv2.VideoCapture(0)

while (True):
	ret, frame = cap.read()

	cv2.imshow("frame", frame)

	if cv2.waitKey(1) & 0xFF == ord("q"):
		break

cap.release()
cv2.destroyAllWindows()

# -------------------------------------------------

# Llegamos hasta aqui:
# - How to read, write, show videos from camera in OpenCV
# - https://www.youtube.com/watch?v=-RtVZsCvXAQ&list=PLS1QulWo1RIa7D1O6skqDQ-JZ1GGHKK-K&index=7

# - Setting Camera Parameters in OpenCV
# - https://www.youtube.com/watch?v=FVnA3xpqEKY&list=PLS1QulWo1RIa7D1O6skqDQ-JZ1GGHKK-K&index=7

# - Show date and time on videos
# - https://www.youtube.com/watch?v=rRSyg9kYfcU&list=PLS1QulWo1RIa7D1O6skqDQ-JZ1GGHKK-K&index=8