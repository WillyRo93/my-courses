import psycopg2
import env.json 

core.config import user, password, host, port, dbname
 
cnx = psycopg2.connect(user=user, password=password, host=host, port=port, dbname=dbname)