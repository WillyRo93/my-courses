import psycopg2

# Conectandose a una base de datos de Postgres
conn = psycopg2.connect("dbname=test user=postgres port=port host=host password=password")

# Abrir un cursos para hacer operaciones en la base de datos:
cur = conn.cursor()

# Ejecutar un query
cur.execute("SELECT * FROM my_data")

# Devolver los resultados del query:
records = cur.fetchall()