import matplotlib.pyplot as plt

# Para compartir el eje y, usamos sharey:
fig, ax = plt.subplots(1, 2, sharey = True)

pais_x = [2016, 2017, 2018, 2019, 2020, 2021]
poblacion_x = [45, 46, 47, 48, 50, 51]

pais_y = [2016, 2017, 2018, 2019, 2020, 2021]
poblacion_y = [40, 41, 42, 43, 44, 45]

ax[0].plot(pais_x, poblacion_x, color = "r", label = "Colombia")
ax[0].legend()
ax[1].plot(pais_y, poblacion_y, color = "g", label = "Argentina")
ax[1].legend()

plt.show()