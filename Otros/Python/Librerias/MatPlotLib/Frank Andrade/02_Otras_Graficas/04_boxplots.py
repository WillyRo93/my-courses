import matplotlib.pyplot as plt

edades = [15, 16, 17, 20, 21, 21, 22, 23, 24, 25, 26, 30, 31, 32, 35, 70]
bins = [15, 20, 25, 30, 35]

# En este caso, solo se necesita un dato en este caso las edades:
plt.boxplot(edades)
plt.show()