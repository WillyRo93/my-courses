import matplotlib.pyplot as plt

# En los subplots, tenemos (Filas, Columnas)
fig, ax = plt.subplots(1, 2)

# Tomamos la data que hicimos previamente de prueba para paises:
pais_x = [2016, 2017, 2018, 2019, 2020, 2021]
poblacion_x = [45, 46, 47, 48, 50, 51]

pais_y = [2016, 2017, 2018, 2019, 2020, 2021]
poblacion_y = [40, 41, 42, 43, 44, 45]

# Este seria el plot de la izq:
ax[0].plot(pais_x, poblacion_x, color = "r")
# Este seria el plot de la der:
ax[1].plot(pais_y, poblacion_y, color = "g")

plt.show()