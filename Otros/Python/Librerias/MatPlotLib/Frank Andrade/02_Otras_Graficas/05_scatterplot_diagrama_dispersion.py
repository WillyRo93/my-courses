import matplotlib.pyplot as plt

a = [1 , 2 , 3 , 4 , 4 , 5 , 6, 7 , 4 , 8, 4, 3, 6, 7]
b = [1 , 3 , 3 , 5 , 4 , 7 , 6, 7 , 9 , 10, 4, 2, 1, 6]

# En este caso, necesitamos dos tablas de data
plt.scatter(a, b)
plt.show()