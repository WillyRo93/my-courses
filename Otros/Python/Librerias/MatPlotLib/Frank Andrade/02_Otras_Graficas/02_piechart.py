import matplotlib.pyplot as plt

paises = ["Argentina", "Colombia", "Perú"]
poblacion = [40, 50, 33]

# En este caso, los paises son el label, para mostrarlos:
plt.pie(poblacion, labels = paises)
plt.show()