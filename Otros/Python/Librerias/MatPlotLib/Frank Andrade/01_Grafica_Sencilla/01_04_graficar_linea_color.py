import matplotlib.pyplot as plt

#Line Plot (Creando PLots para Paises)
pais_x = [2016, 2017, 2018, 2019, 2020, 2021]
poblacion_x = [45, 46, 47, 48, 50, 51]

# Si queremos colocarle un color a la linea, hacemos lo siguiente:
# Colocamos un color
plt.plot(pais_x, poblacion_x, marker = '.', linestyle = ":", color = "k") 
plt.show()

# Tenemos que los colores de linea son los siguientes:
# "b" blue
# "g" green
# "r" red
# "c" cyan
# "m" magenta
# "y" yelllow
# "k" black
# "w" white