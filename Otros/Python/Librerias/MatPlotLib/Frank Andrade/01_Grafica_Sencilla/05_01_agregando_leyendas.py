import matplotlib.pyplot as plt

# Cambiando el tamaño de la figura:
plt.figure(figsize=(10, 6))

# Pais 1 (Pais X)
pais_x = [2016, 2017, 2018, 2019, 2020, 2021]
poblacion_x = [45, 46, 47, 48, 50, 51]

# Pais 2 (Pais Y)
pais_y = [2016, 2017, 2018, 2019, 2020, 2021]
poblacion_y = [40, 41, 42, 43, 44, 45]

# Para agregar leyendas, tenemos que agregar un label y luego activar las leyendas:
plt.plot(pais_x, poblacion_x, marker = '.', linestyle = "-", color = "r", label = "Colombia")
plt.plot(pais_y, poblacion_y, marker = '.', linestyle = "-", color = "g", label = "Argentina")

plt.xlabel("Años")
plt.ylabel("Poblacion (Millones)")
plt.title("Años Vs. Poblacion")
# Tambien debemos activar las leyendas:
plt.legend()

plt.yticks([45, 48, 51])

plt.show()