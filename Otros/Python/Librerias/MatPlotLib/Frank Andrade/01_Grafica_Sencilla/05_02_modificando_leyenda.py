import matplotlib.pyplot as plt

# Cambiando el tamaño de la figura:
plt.figure(figsize=(10, 6))

# Pais 1 (Pais X)
pais_x = [2016, 2017, 2018, 2019, 2020, 2021]
poblacion_x = [45, 46, 47, 48, 50, 51]

# Pais 2 (Pais Y)
pais_y = [2016, 2017, 2018, 2019, 2020, 2021]
poblacion_y = [40, 41, 42, 43, 44, 45]

plt.plot(pais_x, poblacion_x, marker = '.', linestyle = "-", color = "r", label = "Colombia")
plt.plot(pais_y, poblacion_y, marker = '.', linestyle = "-", color = "g", label = "Argentina")

plt.xlabel("Años")
plt.ylabel("Poblacion (Millones)")
plt.title("Años Vs. Poblacion")
# Para modificar el lugar donde aparece la leyenda, hacemos lo siguiente:
# Agregamos un loc para definir el lugar.
plt.legend(loc = "lower right")

# Las leyendas son:
# Location String   Location Code
# "best" ----------> 0
# "upper right" ---> 1
# "upper left" ----> 2
# "lower left" ----> 3
# "lower right" ---> 4
# "right" ---------> 5
# "center left" ---> 6
# "center right" --> 7
# "lower right" --> 8
# "upper center" --> 9
# "center" --> 10


plt.yticks([45, 48, 51])

plt.show()