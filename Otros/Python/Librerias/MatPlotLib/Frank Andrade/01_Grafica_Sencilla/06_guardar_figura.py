import matplotlib.pyplot as plt

plt.figure(figsize=(10, 6))

# Pais 1 (Pais X)
pais_x = [2016, 2017, 2018, 2019, 2020, 2021]
poblacion_x = [45, 46, 47, 48, 50, 51]

# Pais 2 (Pais Y)
pais_y = [2016, 2017, 2018, 2019, 2020, 2021]
poblacion_y = [40, 41, 42, 43, 44, 45]

plt.plot(pais_x, poblacion_x, marker = '.', linestyle = "-", color = "r", label = "Colombia")
plt.plot(pais_y, poblacion_y, marker = '.', linestyle = "-", color = "g", label = "Argentina")

plt.xlabel("Años")
plt.ylabel("Poblacion (Millones)")
plt.title("Años Vs. Poblacion")
plt.legend(loc = "lower right")

plt.yticks([45, 48, 51])

plt.savefig("poblacion_prueba.png")

plt.show()