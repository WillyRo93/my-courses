import matplotlib.pyplot as plt

#Line Plot (Creando PLots para Paises)
pais_x = [2016, 2017, 2018, 2019, 2020, 2021]
poblacion_x = [45, 46, 47, 48, 50, 51]

# Si queremos colocarle un marcador podemos hacer lo siguiente:
# Colocamos un marker para diferenciar los puntos
plt.plot(pais_x, poblacion_x, marker = '.')
plt.show()

# Y tenemos que los markers son los siguientes:
# '.' point marker
# ',' pixel marker
# 'o' circle marker
# 'v' triangle_down marker
# '' triangle_up marker
# '<' triangle_left marker
# '>' triangle_right marker
# '1' tri_down marker
# '2' tri_up marker
# '3' tri_left marker
# '3' tri_right marker