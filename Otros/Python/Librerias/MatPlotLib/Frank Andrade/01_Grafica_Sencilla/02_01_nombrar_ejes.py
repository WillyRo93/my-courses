import matplotlib.pyplot as plt

#Line Plot (Creando PLots para Paises)
pais_x = [2016, 2017, 2018, 2019, 2020, 2021]
poblacion_x = [45, 46, 47, 48, 50, 51]

plt.plot(pais_x, poblacion_x, marker = '.', linestyle = ":", color = "r")

# Si queremos nombrar los ejes podemos hacerlo de la siguiente manera:
plt.xlabel("Años")
plt.ylabel("Poblacion (Millones)")
plt.title("Años Vs. Poblacion")

plt.show()