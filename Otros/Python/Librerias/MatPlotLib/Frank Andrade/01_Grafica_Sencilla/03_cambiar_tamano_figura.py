import matplotlib.pyplot as plt

# Cambiando el tamaño de la figura:
plt.figure(figsize=(10, 6))

pais_x = [2016, 2017, 2018, 2019, 2020, 2021]
poblacion_x = [45, 46, 47, 48, 50, 51]

plt.plot(pais_x, poblacion_x, marker = '.', linestyle = ":", color = "r")

plt.xlabel("Años")
plt.ylabel("Poblacion (Millones)")
plt.title("Años Vs. Poblacion")

plt.yticks([45, 48, 51])

plt.show()