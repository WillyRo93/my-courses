
import matplotlib.pyplot as plt

#Line Plot (Creando PLots para Paises)
pais_x = [2016, 2017, 2018, 2019, 2020, 2021]
poblacion_x = [45, 46, 47, 48, 50, 51]

# Si queremos colocarle un estilo a la linea, hacemos lo siguiente:
# Colocamos un linestyle
plt.plot(pais_x, poblacion_x, marker = '.', linestyle = ":") 
plt.show()

# Tenemos que los estilos de linea son los siguientes:
# "" No Line
# "-" Solid Line Style
# "--" Dashed Line Style
# "-." Dash-Dot Line Style
# ":" Dotted Line Style