import pandas
import sqlalchemy
    
# Create the engine to connect to the PostgreSQL database
engine = sqlalchemy.create_engine('postgresql://postgres:test1234@localhost:5432/sql-shack-demo')
     
# Read data from SQL table
sql_data = pandas.read_sql_table('superstore',engine)