import pandas as pd
import matplotlib.pyplot as plt

# Link del curso:
# https://www.youtube.com/watch?v=3uXr6W66ilk&list=PLjdo6jnQHJFYUxftilqXD1pgzq9E9yJze&index=3

direccion_oficina = "D:/Usuarios/wrodriguez/Downloads/Cursos_Gitlab/my-courses/Otros/Pandas/Escuela de Bayes/players_20.csv"
fifa_20 = pd.read_csv(direccion_oficina)

# Primeros Registros:
print(fifa_20.head(10))

# Ultimos Registros:
print(fifa_20.tail())

# Forma del archivo:
print(fifa_20.shape)

# Nombre de las Columnas:
print(fifa_20.columns.values)

# Estadisticas básicas del dataset:
print(fifa_20.describe())

# Tipos de dato para cada Variable:
print(fifa_20.dtypes)

# Mostrar solo lo que quiero:
fifa_20_data_reducida = fifa_20[["short_name", "age", "nationality", "club", "overall", "potential", "wage_eur"]]
print(fifa_20_data_reducida.head(20))

# Jugadores de equipos especificos:
real_madrid = fifa_20_data_reducida[fifa_20_data_reducida["club"] == "Real Madrid"]
print(real_madrid)
print(real_madrid.shape)

barcelona = fifa_20_data_reducida[fifa_20_data_reducida["club"] == "FC Barcelona"]
print(barcelona)
print(barcelona.shape)

# Venezuela
venezuela = fifa_20_data_reducida[fifa_20_data_reducida["nationality"] == "Venezuela"]
print(venezuela.head(20))
print(venezuela.shape)

# Comparativa ("Que club tiene los mejores jugadores?")
plt.figure()

plt.subplot(121)
plt.boxplot(real_madrid["overall"])
plt.ylabel("Overall")
plt.title("Real Madrid")
plt.grid(True)

plt.subplot(122)
plt.boxplot(barcelona["overall"])
plt.title("Barcelona")
plt.grid(True)
plt.show()

