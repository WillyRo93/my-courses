import pandas as pd
import matplotlib.pyplot as plt

direccion_casa = "C:/Users/wrodriguez/Downloads/Mis_Cursos/Cursos_Gitlab/Otros/Pandas/Keith Galli/pokemon_data.csv"
direccion_oficina = "D:/Usuarios/wrodriguez/Downloads/Cursos_Gitlab/my-courses/Otros/Pandas/Keith Galli/pokemon_data.csv"

# ----------------------------------------------------------------------------------
#df = pd.read_csv(direccion_casa)
df = pd.read_csv(direccion_oficina)

# ----------------------------------------------------------------------------------
# Usando GroupBy
# Si queremos por ejemplo, tener estadisticas generales de Fuerza, Velocidad, etc
# Segun los tipos de Pokemon:
#df = df.groupby( ["Type 1"] ).mean()

# ----------------------------------------------------------------------------------
# Si quiero hacer lo mismo que en el paso anterior pero ordenarlo para observar
# que Pokemons son mas fuertes, rapidos, mayor defensa, etc:
#df = df.groupby( ["Type 1"] ).mean().sort_values("Defense", ascending = False)

# ----------------------------------------------------------------------------------
# Funcion sum() y Funcion count()
#df = df.groupby( ["Type 1"] ).sum()
#df = df.groupby( ["Type 1"] ).count()

# ----------------------------------------------------------------------------------
# Como hacer un conteo de Pokemons segun su tipo:
df["count"] = 1

# Asi seria tipo por tipo:
#df = df.groupby( ["Type 1"] ).count()["count"]

# Asi seria, segun tipo 1 y tipo 2:
df = df.groupby( ["Type 1", "Type 2"] ).count()["count"]

print(df.head(50))

