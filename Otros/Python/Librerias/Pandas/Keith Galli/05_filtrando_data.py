import pandas as pd
import matplotlib.pyplot as plt

direccion_casa = "C:/Users/wrodriguez/Downloads/Mis_Cursos/Cursos_Gitlab/Otros/Pandas/Keith Galli/pokemon_data.csv"
direccion_oficina = "D:/Usuarios/wrodriguez/Downloads/Cursos_Gitlab/my-courses/Otros/Pandas/Keith Galli/pokemon_data.csv"

# ----------------------------------------------------------------------------------
#df = pd.read_csv(direccion_casa)
df = pd.read_csv(direccion_oficina)

# ----------------------------------------------------------------------------------
# Si queremos tener dos condiciones que se cumplan, es decir,
# Si quisieramos obtener los pokemon (Tipo Hierba / Tipo Veneno):
# En Pandas las condiciones son:
#   - And: &
#   - Or:  |
tipo_hierba_veneno = df.loc[ (df["Type 1"] == "Grass") & (df["Type 2"] == "Poison") ]

#print(tipo_hierba_veneno)

# Entonces, si por ejemplo deseo obtener todos los que sean tipo Hierba,
# sin importar si está en Tipo1 o Tipo2, tendria lo sig:
tipo_hierba = df.loc[ (df["Type 1"] == "Grass") | (df["Type 2"] == "Grass") ]
#print(tipo_hierba.head(40))

# ----------------------------------------------------------------------------------
# Si queremos mostrar la tabla con nuevos index, hacemos lo sig:
#   Con (drop = True) eliminamos el index anterior: 
tipo_hierba_veneno = tipo_hierba_veneno.reset_index(drop = True)
tipo_hierba = tipo_hierba.reset_index(drop = True)

#print(tipo_hierba_veneno, "\n")
#print(tipo_hierba)

# ----------------------------------------------------------------------------------
# Quiero que me muestre a los Pokemon mas poderosos:
pokemon_attack = df.loc[ (df["Attack"] > 130) ]
#print(pokemon_attack)

# ----------------------------------------------------------------------------------
# Quiero obtener todos los Pokemon que tengan "Mega" en sus nombres:
MegaPokemons = df.loc[ df["Name"].str.contains("Mega") ]
#print(MegaPokemons)

# Si lo que quiero es mostrar los Pokemon que NO tengan "Mega":
data_sin_MegaPokemons = df.loc[ ~ df["Name"].str.contains("Mega") ]
#print(data_sin_MegaPokemons)

#print(data_sin_MegaPokemons)

# ----------------------------------------------------------------------------------
# Filtrar por medio de expresiones regulares:

# Digamos que quiero obtener los Pokemon que sean de tipo Fuego o Hierba:
import re

#tipo_fuego_o_hierba = df.loc[ df["Type 1"].str.contains( "Fire|Grass", regex = True) ]

# Otra forma de hacer esto mismo es ignorando las mayusculas o minusculas:
#   Usando flags = re.I
tipo_fuego_o_hierba = df.loc[ df["Type 1"].str.contains( "fire|grass", flags=re.I, regex = True) ]

#print(tipo_fuego_o_hierba)

# ----------------------------------------------------------------------------------
# Ahora con lo anterior queremos buscar Pokemons que empiecen por "Pi"
pokemons_pi = df.loc[ df["Name"].str.contains( "^pi[a-z]*", flags=re.I, regex = True) ]
print(pokemons_pi)