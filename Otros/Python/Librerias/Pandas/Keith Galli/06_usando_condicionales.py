import pandas as pd
import matplotlib.pyplot as plt

direccion_casa = "C:/Users/wrodriguez/Downloads/Mis_Cursos/Cursos_Gitlab/Otros/Pandas/Keith Galli/pokemon_data.csv"
direccion_oficina = "D:/Usuarios/wrodriguez/Downloads/Cursos_Gitlab/my-courses/Otros/Pandas/Keith Galli/pokemon_data.csv"

# ----------------------------------------------------------------------------------
#df = pd.read_csv(direccion_casa)
df = pd.read_csv(direccion_oficina)

# ----------------------------------------------------------------------------------
# Si queremos cambiar algunos valores segun equis criterio:
#df.loc[ df["Type 1"] == "Fire", "Type 1" ] = "Fueguito Intenso"

# ----------------------------------------------------------------------------------
# Cambiar varios valores segun un condicional:
#df.loc[ df["Total"] > 500, ["Generation", "Legendary"] ] = ["100", "Super Legandario Papá"]

# Yo cambié dos valores, Generation y Legendary. Y coloqué dos cosas nuevas.
# Pero yo puedo cambiar dos o mas valores por un solo valor, es decir:
df.loc[ df["Total"] > 600, ["Generation", "Legendary"] ] = "Un Solo Cambio"

print(df.head(30))