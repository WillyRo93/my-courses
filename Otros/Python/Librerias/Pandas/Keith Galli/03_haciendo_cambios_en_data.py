import pandas as pd
import matplotlib.pyplot as plt

direccion_casa = "C:/Users/wrodriguez/Downloads/Mis_Cursos/Cursos_Gitlab/Otros/Pandas/Keith Galli/pokemon_data.csv"
direccion_oficina = "D:/Usuarios/wrodriguez/Downloads/Cursos_Gitlab/my-courses/Otros/Pandas/Keith Galli/pokemon_data.csv"

# ----------------------------------------------------------------------------------
#df = pd.read_csv(direccion_casa)
df = pd.read_csv(direccion_oficina)

# ----------------------------------------------------------------------------------
# Si quisieramos por alguna  razon sumar los valores de algunas columnas:
# Esto nos va a mostrar otra columna con el nombre de "Attack + Speed"
#df["Attack + Speed"] = df["Attack"] + df["Speed"]
#print(df.head(10))

# Si quisieramos borrar esa columna creada, usamos drop:
#df = df.drop(columns=["Attack + Speed"])

# Otra manera de lograr esta suma de valores en las columnas es la siguiente:
#df["Total/Copia"] = df.iloc[:, 5:11].sum(axis=1)
#print(df.head(15))

# ----------------------------------------------------------------------------------
cols = list(df.columns)
print(cols)

print(cols[:3])
