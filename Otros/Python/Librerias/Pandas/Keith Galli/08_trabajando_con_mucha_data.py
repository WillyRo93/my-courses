import pandas as pd
import matplotlib.pyplot as plt

direccion_casa = "C:/Users/wrodriguez/Downloads/Mis_Cursos/Cursos_Gitlab/Otros/Pandas/Keith Galli/pokemon_data.csv"
direccion_oficina = "D:/Usuarios/wrodriguez/Downloads/Cursos_Gitlab/my-courses/Otros/Pandas/Keith Galli/pokemon_data.csv"

# ----------------------------------------------------------------------------------
# Para mostrar toda la data posible la puedo mostrar por partes:
for df in pd.read_csv(direccion_oficina, chunksize=20):
    print("DataFrame Recortada:", "\n", df)