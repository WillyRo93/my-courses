import pandas as pd
import matplotlib.pyplot as plt

direccion_casa = "C:/Users/wrodriguez/Downloads/Mis_Cursos/Cursos_Gitlab/Otros/Pandas/Keith Galli/pokemon_data.csv"
direccion_oficina = "D:/Usuarios/wrodriguez/Downloads/Cursos_Gitlab/my-courses/Otros/Pandas/Keith Galli/pokemon_data.csv"

# ----------------------------------------------------------------------------------
#df = pd.read_csv(direccion_casa)
df = pd.read_csv(direccion_oficina)

# ----------------------------------------------------------------------------------
# Si quiero tener un archivo .csv que no tenga los index, podemos hacer lo sig:
#df.to_csv("pokemon_data_sin_index.csv", index=False)

# Excel mostrando los Index:
#df.to_excel("pokemon_data_con_index.xlsx")

#Excel sin Index:
#df.to_excel("pokemon_data_sin_index.xlsx", index = False)

# ----------------------------------------------------------------------------------
# Si quiero guardarlo en un archivo txt:
df.to_csv("pokemon_data_texto.txt", index=False)

# Pero en vez de verlo dividido por comas (,) podemos establecer otra division:
df.to_csv("pokemon_data_texto_con_tabs.txt", index= False, sep="\t")