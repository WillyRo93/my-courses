import pandas as pd

direccion_casa = "C:/Users/wrodriguez/Downloads/Mis_Cursos/Cursos_Gitlab/Otros/Pandas/Keith Galli/pokemon_data.csv"

# Para leer archivos .csv
df= pd.read_csv(direccion_casa)

# Mostrando toda la informacion del DataFrame:
print("Tabla Completa:\n", df, "\n")

# Mosstrando los primeros valores del DataFrame:
print("Primeros Valores:\n", df.head(), "\n")

# Mostrando los ultimos valores del DataFrame:
print("Ultimos Valores:\n", df.tail(), "\n")

