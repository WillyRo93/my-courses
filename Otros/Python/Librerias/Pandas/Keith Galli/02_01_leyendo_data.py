import pandas as pd
import matplotlib.pyplot as plt

direccion_casa = "C:/Users/wrodriguez/Downloads/Mis_Cursos/Cursos_Gitlab/Otros/Pandas/Keith Galli/pokemon_data.csv"
direccion_oficina = "D:/Usuarios/wrodriguez/Downloads/Cursos_Gitlab/my-courses/Otros/Pandas/Keith Galli/pokemon_data.csv"

# ----------------------------------------------------------------------------------
#df = pd.read_csv(direccion_casa)
df = pd.read_csv(direccion_oficina)

# ----------------------------------------------------------------------------------
# Obtener los valores de las columnas o headers:
headers = df.columns
#print(headers)

# ----------------------------------------------------------------------------------
# Leer cada columna (Por ejemplo leer los nombres de los Pokemon)
nombres = df["Name"]
#print(nombres[0:50])

# ----------------------------------------------------------------------------------
# Si queremos leer varias columnas de data, lo colocamos como una lista:
data_pokemons = df[ ["Name", "HP", "Speed", "Attack"] ]
#print(data_pokemons)

# ----------------------------------------------------------------------------------
# Ahora queremos leer cada fila
# Lo hacemos con integer location (iloc)
#print(df.iloc[0:11])

# ----------------------------------------------------------------------------------
# Leer un valor en especifico (Fila, Columna):
# Sabemos que Venasaur está en la fila 2, columna 1:
venasaur = df.iloc[2:1]
#print(venasaur)

# ----------------------------------------------------------------------------------
# Leyendo la informacion de fila por fila:
#for index, row in df.iterrows():
    #print(index, row, "\n")

# Este ultimo paso en especifico para que lo puedo usar?
# Por ejemplo para iterar en los valores de los nombres:
#for index, row in df.iterrows():
    #print(index, row["Name"], "\n")

# ----------------------------------------------------------------------------------
# Funcion iloc
# Si por ejemplo quiero ver los pokemon tipo fuego:
tipo_fuego = df.loc[ df["Type 1"] == "Fire" ]
#print(tipo_fuego)

# ----------------------------------------------------------------------------------
tipo_volador = df.loc[ df["Type 2"] == "Flying" ]

# ----------------------------------------------------------------------------------
# Para describir la data:
#print(df.describe())

# ----------------------------------------------------------------------------------
# Para obtener todos los Nombres de los Pokemon:
#print(df["Name"])

# ----------------------------------------------------------------------------------
# Comparativa (Que pokemones son mas rapidos? Tipo Aire o Fuego?)
plt.figure()

plt.subplot(121)
plt.boxplot(tipo_fuego["Speed"])
plt.ylabel("Velocidad")
plt.title("Tipo Fuego")
plt.grid(True)

plt.subplot(122)
plt.boxplot(tipo_volador["Speed"])
plt.title("Tipo Aire")
plt.grid(True)

#plt.savefig("Comparativa Velocidad (Fuego Vs Aire).png")

#plt.show()

# ----------------------------------------------------------------------------------
ordenados_por_ataque = df.sort_values("Attack", ascending=False)
#print(ordenados_por_ataque.head(30))

# ----------------------------------------------------------------------------------
# Como ordenar data segun informacion de dos columnas?
tipos_por_special_attack = df.sort_values(["Type 1", "Sp. Atk"], ascending=False)
# Esto me va a dar por cada tipo de Pokemon, los que tengan mayor ataque especial
#print(tipos_por_special_attack)

# Como ordenar por columna pero con algunos datos ascendentes y otros descendentes?
mayor_ataque_menor_velocidad = df.sort_values(["Attack", "Speed"], ascending=[0,1])
#print(mayor_ataque_menor_velocidad)
# ----------------------------------------------------------------------------------
