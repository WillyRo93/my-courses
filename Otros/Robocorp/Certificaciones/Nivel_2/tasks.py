from robocorp.tasks import task
from robocorp import browser

from RPA.HTTP import HTTP
# from RPA.Excel.Files import Files

# import pandas as pd
import csv


@task
def order_robots_from_RobotSpareBin():
    """
    Orders robots from RobotSpareBin Industries Inc.
    Saves the order HTML receipt as a PDF file.
    Saves the screenshot of the ordered robot.
    Embeds the screenshot of the robot to the PDF receipt.
    Creates ZIP archive of the receipts and the images.
    """

    # open_robot_order_website()

    # fill_and_submit_order()

    download_csv()
    orders = get_orders()
    print(orders)


def open_robot_order_website():
    """Navigates to the given URL
    """
    browser.configure(
        slowmo=1000,
    )
    browser.goto("https://robotsparebinindustries.com/#/robot-order")


def download_csv():
    """Downloads csv file from the given URL"""
    http = HTTP()
    http.download(url="https://robotsparebinindustries.com/orders.csv",
                  overwrite=True)


# def fill_and_submit_order():
#     page = browser.page()
#     page.click("button:text('OK')")
#     page.select_option("#head", "Roll-a-thor head")
#     page.click("#id-body-1")
#     page.fill('input[placeholder="Enter the part number for the legs"]', "1")
#     page.fill('input[placeholder="Shipping address"]', "Wepa")

def get_orders():
    # Open the CSV file in read mode
    with open('orders.csv', 'r') as csvfile:
        # Create a reader object
        csv_reader = csv.reader(csvfile)

        # Iterate through the rows in the CSV file
        for row in csv_reader:
            # Access each element in the row
            print(row)