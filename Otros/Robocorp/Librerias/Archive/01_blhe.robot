*** Settings ***
Library  RPA.Archive

*** Tasks ***
Creating a ZIP archive
   Archive Folder With ZIP   ${CURDIR}${/}tasks  tasks.zip   recursive=True  include=*.robot  exclude=/.*
   @{files}                  List Archive             tasks.zip
   FOR  ${file}  IN  ${files}
      Log  ${file}
   END
   Add To Archive            .${/}..${/}missing.robot  tasks.zip
   &{info}                   Get Archive Info