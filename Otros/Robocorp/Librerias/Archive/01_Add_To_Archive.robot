*** Tasks ***

# This keyword adds file or list of files into existing archive.
# Files can be added to archive structure with relative path using argument folder.

Add To Archive  extrafile.txt  myfiles.zip
Add To Archive  stat.png       archive.tar.gz  images
@{files}        Create List    filename1.txt   filename2.txt
Add To Archive  ${files}       files.tar