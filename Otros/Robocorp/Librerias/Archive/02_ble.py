from RPA.Archive import Archive

lib = Archive()
lib.archive_folder_with_tar('./tasks', 'tasks.tar', recursive=True)
files = lib.list_archive('tasks.tar')
for file in files:
   print(file)