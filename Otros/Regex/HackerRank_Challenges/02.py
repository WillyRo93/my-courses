# Task:
    # Your task is to write a regular expression that matches only and
    # exactly strings of form: abc.def.ghi.jkx, where each variable a,b,c,d,e,f,g,h,i,j,k,x
    # can be any single character except the newline.

# Expected Output:
    # true

import re
import sys

regex_pattern = r""
test_string = "123.456.abc.def"

match = re.match(regex_pattern, test_string) is not None

print(str(match).lower())