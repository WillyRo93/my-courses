import re

test_string = "123abc456789abc123ABC"

# Por ejemplo, con "." obtienes todos los matches existentes
# Es decir, basicamente haces match con todo.
pattern = re.compile(r".")
matches = pattern.finditer(test_string)

for match in matches:
    print(match.group())
print("-------------", "\n")

# ----------------------------------------------------------------------

# Ahora, si quisieramos obtener un match donde encontremos el "."
# hariamos lo siguiente:
test_string = "123abc456789abc123ABC."
pattern = re.compile(r"\.")
matches = pattern.finditer(test_string)

for match in matches:
    print(match)
print("-------------", "\n")

# ----------------------------------------------------------------------

# Luego, si deseo saber si mi busqueda está al inicio del string:
pattern = re.compile(r"^123")
matches = pattern.finditer(test_string)

for match in matches:
    print(match)
print("-------------", "\n")

# ----------------------------------------------------------------------

# Finalmente, si quiero saber si mi busqueda está al final del string:
pattern = re.compile(r"ABC.$")
matches = pattern.finditer(test_string)

for match in matches:
    print(match)
print("-------------", "\n")

# ----------------------------------------------------------------------
