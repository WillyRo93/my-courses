import re

test_string = "123abc456789abc123ABC"

# Queremos buscar el pattern abc
pattern = re.compile(r"abc")
matches = pattern.finditer(test_string)

for match in matches:
    print(match)
print("\n")

# Otra manera de lograrlo es la siguiente:
matches = re.finditer(r"abc", test_string)

for match in matches:
    print(match)
print("\n")

# Que significa la r? Significa raw string, que no tiene caracteres especiales
# Por ejemplo, si tengo la siguiente variable, ella viene con un tab:
a = "\tHello"
print(a)

# Pero, si le digo que es un raw string, lo imprime tal cual está:
a = r"\tHello"
print(a)