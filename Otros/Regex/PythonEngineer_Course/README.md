1) re module

    01_re_module.py
        - re.compile()
        - re.finditer()
        - Una explicacion de que significa un raw string.

2) Methods to search for matches

    02_methods_search_for_matches.py
        - re.findall()
        - re.match()
        - re.search()

3) Methods on a match object

    03_methods_on_match_objects.py
        - re.span()
        - re.start()
        - re.end()
        - group() 

4) Meta characters

    04_meta_characters.py
        Aprendimos a hacer busquedas con los siguientes meta characters:
        .
        \
        ^
        $

5) More special sequences

6) Sets

7) Quantifier

8) Conditions

9) Grouping

10) Modification

11) Compilation flags


- Methods on a match:

    match(): Determina si los RE hacen match con el inicio del string.
    search(): Scanea a través del string, viendo por cualquier location donde el RE haga match.
    findall(): Encuentra todos los substrings donde el RE hace match, y lo retorna como una lista.
    finditer(): Encuentra todos los substrings donde el RE hace match, y los retorna como un iterador.

- Modification Methods:

    split(): Retorna una lista donde el string ha sido dividido en cada match
    sub(): Reemplaza uno o mas matches por un string

- Todos los meta characters: . ^ $ * + ? { } [ ] \ | ( )

    . Cualquier caracter (Excepto una nueva linea)
    ^ Por ejemplo si empieza por "^Holiwis"
    $ Por ejemplo si termina por "Chaito$"
    * Cero o mas ocurrencias "aix*"
    + Una o mas ocurrencias "aix+"
    ? 
    { } Exactamente el numero especificado de ocurrencias "al{2}"
    [ ] Un set de Caracteres "[a-m]"
    \ Una secuencia especial (o caracteres especiales de espacio) "\d"
    | Dos opciones, o cae o sube "cae|sube"
    ( ) Capturar y agrupar

- Mas special characters:

    \d : Hace match a cualquier digito decimal; [0-9]
    \D : Hace match a cualquier caracter que no sea digito
    \s : Hace match a cualquier whitespace character; (espacio " " tab "\t" newline "\n")
    \S : Hace match a cualquier espacio non-whitspace
    \w : Hace match a cualquier caracter alfa numerico (palabra); [a-zA-Z0-9_]
    \W : Hace match a cualquier caracter non-alphanumeric
    \b : Hace match donde los caracteres especificados esten o en el comienzo o en el final de una palabra.
    \B : Hace match donde los caracteres especificados estén presentes, pero NO al inicio o al final de una palabra.