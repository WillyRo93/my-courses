import re

test_string = "123abc456789abc123ABC"

pattern = re.compile(r"abc")

# Ahora, que podemos hacer con los matches?
    # Podemos usar group(), start(), end(), span()

# span():
    # Con este metodo obtenemos directamente la localizacion de los matches
matches = pattern.finditer(test_string)

for match in matches:
    print(match.span())
print("-------------", "\n")
# ----------------------------------------------------------------------

# start():
    # Con este metodo obtenemos el inicio del match
matches = pattern.finditer(test_string)

for match in matches:
    print(match.start())
print("-------------", "\n")
# ----------------------------------------------------------------------

# end():
    # Con este metodo obtenemos el final del match
matches = pattern.finditer(test_string)

for match in matches:
    print(match.end())
print("-------------", "\n")
# ----------------------------------------------------------------------

# group():
    # Con este metodo obtenemos los strings del match
    # Puedes incluso pedir un index en especifico
matches = pattern.finditer(test_string)

for match in matches:
    print(match.group())
print("-------------", "\n")
# ----------------------------------------------------------------------