import re

test_string = "123abc456789abc123ABC"

pattern = re.compile(r"abc")

# Ahora, usaremos los siguientes metodos: match(), search(), findall()

# findall():
    # Este metodo encuentra los dos "abc" que existen en el string
matches = pattern.findall(test_string)

for match in matches:
    print(match)
print("-------------", "\n")
# ----------------------------------------------------------------------

# match():
    # Este metodo da como resultado None, pues solo encuentra match si está al inicio del string.
match = pattern.match(test_string)
print(match)
print("\n")

    # En cambio si buscamos "123" nos va a indicar el match.
match = re.match(r"123", test_string)
print(match)
print("-------------", "\n")
# ----------------------------------------------------------------------

# search():
    # Este metodo da como resultado el primer resultado que encuentra en el string.
match = pattern.search(test_string)
print(match)
print("-------------", "\n")