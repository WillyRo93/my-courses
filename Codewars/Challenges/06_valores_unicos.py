# Implement the function unique_in_order which takes as argument a sequence
# and returns a list of items without any elements with the same value next
# to each other and preserving the original order of elements.

#For example:

#unique_in_order('AAAABBBCCDAABBB') == ['A', 'B', 'C', 'D', 'A', 'B']
#unique_in_order('ABBCcAD')         == ['A', 'B', 'C', 'c', 'A', 'D']
#unique_in_order([1,2,2,3,3])       == [1,2,3]

# ---------------------------------------------------
def unique_in_order(iterable):
	lista = []
	letra_anterior = None

	for letra_actual in iterable:
		if letra_actual != letra_anterior:
			lista.append(letra_actual)

		letra_anterior = letra_actual
	print(lista)
	return(lista)

unique_in_order('AAAABBBCCDAABBB')