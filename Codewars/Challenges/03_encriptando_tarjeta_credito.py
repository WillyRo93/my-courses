# Usually when you buy something, you're asked whether your credit card number,
# phone number or answer to your most secret question is still correct.
# However, since someone could look over your shoulder, you don't want that
# shown on your screen. Instead, we mask it.

# Your task is to write a function maskify, which changes all but the
# last four characters into '#'.

# Examples:
# maskify("4556364607935616") == "############5616"
# maskify(     "64607935616") ==      "#######5616"
# maskify(               "1") ==                "1"
# maskify(                "") ==                 ""

# "What was the name of your first pet?"
# maskify("Skippy")                                   == "##ippy"
# maskify("Nananananananananananananananana Batman!") == "####################################man!"

# --------------------------------------------------------------------
# return masked string
def maskify(cc):
	if len(cc) < 4:
		cc = cc

	elif len(cc) > 4:
		cantidad_numerales = len (cc) - 4
		numerales = ""
		for i in range(cantidad_numerales):
			numerales = numerales + "#"
		cc = numerales + str(cc[-4:])
		print(cc)
	return (cc)

cc = ''
r = maskify(cc)

cc = '123'
r = maskify(cc)

cc = 'SF$SDfgsd2eA'
r = maskify(cc)
