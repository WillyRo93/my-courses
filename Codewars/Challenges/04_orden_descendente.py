# Your task is to make a function that can take any non-negative integer
# as an argument and return it with its digits in descending order.
# Essentially, rearrange the digits to create the highest possible number.

# Examples:
# Input: 42145 Output: 54421
# Input: 145263 Output: 654321
# Input: 123456789 Output: 987654321

# --------------------------------------------------------------------
def descending_order(num):
    lista = []
    numero = ""
    num = str(num)
    if len(num) > 1:
        for i in num:
            lista.append(i)
        lista.sort(reverse=True)
        for i in lista:
            numero = numero + str(i)
        numero = int(numero)
    else:
        numero = int(num)
    print(numero)
    return (numero)

(descending_order(0), 0)
(descending_order(15), 51)
(descending_order(123456789), 987654321)