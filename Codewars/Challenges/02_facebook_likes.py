# You probably know the "like" system from Facebook and other pages.
# People can "like" blog posts, pictures or other items.
# We want to create the text that should be displayed next to such an item.

# Implement the function which takes an array containing the names of people that
# like an item. It must return the display text as shown in the examples:

# []                                -->  "no one likes this"
# ["Peter"]                         -->  "Peter likes this"
# ["Jacob", "Alex"]                 -->  "Jacob and Alex like this"
# ["Max", "John", "Mark"]           -->  "Max, John and Mark like this"
# ["Alex", "Jacob", "Mark", "Max"]  -->  "Alex, Jacob and 2 others like this"

# --------------------------------------------------------------------
def likes(names):
	cantidad = 0
	lista_nombres = []
	for nombre in names:
		lista_nombres.append(names)
	for i in names:
		cantidad = cantidad + 1

	if cantidad == 0:
		resultado = "no one likes this"
		print(resultado)
	elif cantidad == 1:
		for i in names:
			resultado = i + " likes this"
		print(resultado)
	elif cantidad == 2:
		resultado = names[0] + " and " + names [1] + " like this"
		print(resultado)
	elif cantidad == 3:
		resultado = names[0] + ", " + names [1] + " and " + names [2] + " like this"
		print(resultado)
	else:
		cantidad = cantidad - 2
		resultado = names[0] + ", " + names [1] + " and " + str(cantidad )+" others like this"
		print(resultado)

	print("La cantidad es:",cantidad)
	print("""\n""")
    # your code here


	return resultado




(likes([]), 'no one likes this')

(likes(['Peter']), 'Peter likes this')

(likes(['Jacob', 'Alex']), 'Jacob and Alex like this')

(likes(['Max', 'John', 'Mark']), 'Max, John and Mark like this')

(likes(['Alex', 'Jacob', 'Mark', 'Max']), 'Alex, Jacob and 2 others like this')