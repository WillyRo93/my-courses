# Write a function that accepts an array of 10 integers (between 0 and 9),
# that returns a string of those numbers in the form of a phone number.

# Example:
    # create_phone_number([1, 2, 3, 4, 5, 6, 7, 8, 9, 0]) # => returns "(123) 456-7890"

# --------------------------------------------------------------------
def create_phone_number(lista):
    numero = """("""
    for num in lista[0:3]:
        numero = numero + str(num)
        #print(num)
    numero = numero + ") "
    for num in lista [3:6]:
        numero = numero + str(num)
    numero = numero + "-"
    for num in lista [6:]:
        numero = numero + str(num)
    return(numero)


#test.describe("Basic tests")
create_phone_number([1, 2, 3, 4, 5, 6, 7, 8, 9, 0]), "(123) 456-7890"
#create_phone_number([1, 1, 1, 1, 1, 1, 1, 1, 1, 1]), "(111) 111-1111"
#create_phone_number([1, 2, 3, 4, 5, 6, 7, 8, 9, 0]), "(123) 456-7890"
#create_phone_number([0, 2, 3, 0, 5, 6, 0, 8, 9, 0]), "(023) 056-0890"
#create_phone_number([0, 0, 0, 0, 0, 0, 0, 0, 0, 0]), "(000) 000-0000"