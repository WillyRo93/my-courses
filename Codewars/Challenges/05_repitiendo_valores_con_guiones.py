# This time no story, no theory. The examples below show you how to write function accum:

# Examples:
# accum("abcd") -> "A-Bb-Ccc-Dddd"
# accum("RqaEzty") -> "R-Qq-Aaa-Eeee-Zzzzz-Tttttt-Yyyyyyy"
# accum("cwAt") -> "C-Ww-Aaa-Tttt"

# ------------------------------------------------------------------------

def accum(s):
	count = -1
	acumulado = ""
	if len (s) > 1: #Caso: abc
		for i in s:
			count = count + 1
			if count == 0: #Aqui solo llega "a"
				acumulado = i.upper() + "-" #A-
			if count > 0: #Aqui llegan count = 1, count = 2
				if count == len (s) - 1: #Aqui solo deberia llegar en la C, donde count = 2, len -1 =2
					acumulado = acumulado + i.upper()
					for e in range(count):
						acumulado = acumulado + i.lower()
				else:
					acumulado = acumulado + i.upper()
					for e in range(count):
						acumulado = acumulado + i.lower()
					acumulado = acumulado + "-"

	else:
		acumulado = s.upper()
	print(acumulado)
	return(acumulado)

accum("abc")
accum("RqaEzty")
accum("cwAt")