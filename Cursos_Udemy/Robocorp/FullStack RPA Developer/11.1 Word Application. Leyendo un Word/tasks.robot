*** Settings ***
Documentation   Probar con Word a ver que tal
Library         RPA.Word.Application


# +
*** Keywords ***

Leer Texto de Word
    [Arguments]    ${DOC_FILE_PATH}
    Open Application    #visible=True
    Open File    ${CURDIR}${/}DataSets${/}Archivo.docx
    ${Text} =    Get All Texts
    #Sleep    4s
    #Quit Application
    [Return]    ${Text}
    
    #filename=%{ROBOTS_ROOT}/DataSets/Archivo.docx
    #${CURDIR}${/}output${/}text.txt

# +
*** Tasks ***

Ejemplito
    ${text_doc} =    Leer Texto de Word    ${CURDIR}${/}DataSets${/}Archivo.docx
    Log    ${text_doc}
# -


