*** Settings ***
Documentation   Excel File Related Keyword Examples
Resource        KeywordLibrary/excel.robot


*** Tasks ***
Read Policies As Table
    ${policydata} =  Read Excel File WorkSheet As Table      ./DataSets/sampledatainsurance.xlsx   PolicyData
    
    @{table_dim} =  Get Table Dimensions  ${policydata}  
    ${row_value} =  Get Table Row       ${policydata}   ${0}   False    
    
    FOR    ${i}    IN RANGE    ${table_dim}[0]
        Get Table Row       ${policydata}   ${i}   False
    END

*** Tasks ***
Create New WorkSheet Tasks
    Create New WorkBook From Map   new_wbook_2.xlsx
    ${policydata} =  Read Excel File WorkSheet As Table      new_wbook_2.xlsx     Sheet

*** Tasks ***
Iterate WorkSheets Example
    Iterate WorkSheets From Workbook   ./DataSets/sampledatainsurance.xlsx

# +
*** Keywords ***

Leer el Excel como Tabla  #Aca estoy es haciendo lo mismo pero probandolo a mi manera para probar
    [Arguments]      ${EXCEL_FILE}    ${WORK_SHEET_NAME}
    Open Workbook    ${EXCEL_FILE}
    ${hoja_calculo} =    Read Worksheet   name=${WORK_SHEET_NAME}   header=${TRUE}
    Close Workbook
    ${Equis_Variable} =     Create Table    ${hoja_calculo}
    [Return]    ${Equis_Variable}
    
Crear una Nueva Hoja de Calculo
    [Arguments]      ${EXCEL_FILE_NAME} 
    Create Workbook  ${EXCEL_FILE_NAME}
    FOR    ${j}    IN RANGE    15
        ${ret} =	Generate Random String
        &{row} =      Create Dictionary
        ...           Orden   ${j}
        ...           Variable "j" multiplicada por 25   ${j * 25}
        ...           Letras Random    ${ret}
        Append Rows to Worksheet  ${row}  header=${TRUE}
    END
    Save Workbook
    
Iterando en Hoja de Calculo
    [Arguments]      ${EXCEL_FILE}
    Open Workbook    ${EXCEL_FILE}
    @{Hojas_De_Calculo} =     List Worksheets
    FOR  ${Hoja_Calculo}  IN   @{Hojas_De_Calculo}
        ${Data_De_Hoja_Calculo} =  Read Worksheet   ${Hoja_Calculo}  
        ${rows} =         Get Length  ${Data_De_Hoja_Calculo}
        ${emp_row} =      Find Empty Row  ${Hoja_Calculo}
        Log     La Hoja de Calculo '${Hoja_Calculo}' contiene ${rows} filas. La primera que esta vacia esta en ${emp_row}
    END
    Close Workbook
# -

*** Tasks ***
Leer los datos de la tabla
    ${Mis_Datos} =  Leer el Excel como Tabla    ./DataSets/Datos.xlsx    Data
    ${Dimensiones_Tabla} =  Get Table Dimensions    ${Mis_Datos}
    ${Cuantas_Columnas} =  Get Table Row    ${Mis_Datos}    ${0}    False
    
    FOR    ${i}    IN RANGE    ${Dimensiones_Tabla}[0]
        Get Table Row    ${Mis_Datos}    ${i}    False
    END

*** Tasks ***
Creando Hoja de Calculo
    Crear una Nueva Hoja de Calculo    NuevaData.xlsx
    ${Mis_Datos} =  Leer el Excel como Tabla    NuevaData.xlsx     Sheet

*** Tasks ***
Ejemplo Iterando
    Iterando en Hoja de Calculo    ./DataSets/Datos.xlsx


