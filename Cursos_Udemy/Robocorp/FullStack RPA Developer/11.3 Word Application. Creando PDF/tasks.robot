*** Settings ***
Documentation   Convirtiendo a PDF
Library         RPA.Word.Application


# +
*** Keywords ***

Convertir a PDF
    [Arguments]    ${DOC_FILE_PATH}
    Open Application    visible=False
    Open File           filename=${DOC_FILE_PATH}    
    Write Text          Netnovation
    Export To Pdf       ${CURDIR}${/}DataSets${/}ArchivoPDF.pdf
    Quit Application
# -

*** Tasks ***
Minimal task
    Convertir a PDF    ${CURDIR}${/}DataSets${/}Archivo.docx


