*** Settings ***
Documentation   Reemplazar y Guardar
Library         RPA.Word.Application


# +
*** Keywords ***

Reemplazar y Guardar
    [Arguments]    ${DOC_FILE_PATH}    ${SRC_STR}    ${DEST_STR}
    Open Application    #visible=True
    Open File    ${CURDIR}${/}DataSets${/}Archivo.docx
    Replace Text    find=${SRC_STR}    replace=${DEST_STR}
    Save Document As    ${CURDIR}${/}DataSets${/}Archivo_Nuevo.docx
    
    ${Text} =    Get All Texts
    
    [Return]    ${Text}
# -

*** Tasks ***
Minimal task
    Reemplazar y Guardar    ${CURDIR}${/}DataSets${/}Archivo.docx    Hola    Epa


