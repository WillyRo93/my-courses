-- Ahora queremos ???

-- Copiamos el formato anterior, para no perder el hilo y cambio "inner" por "join":
select p.mi_nombre, p.mi_dni, c.mi_nombre from persona p left join cargo c on p.foranea_key_cargo=c.mi_id_primary

-- ¿Que hace left join?

-- Cual es la tabla que esta a la izquierda de "persona"?
-- Muy bien, muestrame todo lo de la persona y si no tiene cargo, o el cargo es "Null", igual lo muestra todo

-- ¿Como aparecen los datos? De la siguiente manera, ahora si aparecen los datos nulos:
-- mi_nombre			mi_dni 			mi_nombre
-- Carlos				24782543		NULL
-- Ricardo				24654123		NULL
-- Alberto				24543278		NULL
-- Rigoberto			24654123		NULL
-- Alfredo Gonofredo	24543278		NULL
-- Alfredito			24654128		Gerente General
-- Gorgoncho			24543274		Jefe

-- ¿Puedo cambiar los headers? Pues si puedes, de la siguiente manera:
select p.mi_nombre as "Trabajador", p.mi_dni as "DNI Trabajador", c.mi_nombre as "Cargo Actual" from persona p left join cargo c on p.foranea_key_cargo=c.mi_id_primary

-- ¿Ahora como me aparece? De la siguiente manera:
-- Trabajador			DNI Trabajador		Cargo Actual
-- Carlos				24782543			NULL
-- Ricardo				24654123			NULL
-- Alberto				24543278			NULL
-- Rigoberto			24654123			NULL
-- Alfredo Gonofredo	24543278			NULL
-- Alfredito			24654128			Gerente General
-- Gorgoncho			24543274			Jefe