-- ¿Como podemos leer los datos de una tabla junto con otros datos foraneos?
-- Pues con "inner join"

-- Siguiendo con los datos del archivo anterior "03_llaves_foraneas.sql"

-- Quiero entonces mostrar el nombre y el dni de una persona
-- Para poder conectar con los datos de la tabla de cargos, aplico un inner join
-- Ademas la tabla persona y cargo necesitan un alias, "p" y "c" respectivamente.
-- Esto a su vez, hace que se le deba colocar una "p." previa a los datos que se quieren mostrar

-- Finalmente, como es la relacion entre ambos? con "on"
select p.mi_nombre, p.mi_dni, c.mi_nombre from persona p inner join cargo c on p.foranea_key_cargo=c.mi_id_primary


-- Al hacer todo esto, nos muestra los datos de dos personas:
-- mi_nombre	mi_dni		mi_nombre
-- Alfredito	24654128	Gerente General
-- Gorgoncho	24543274	Jefe

-- ¿Por que sucede esto?
-- Muy sencillo, en "select" estamos pidiendo que se nos muestren los siguientes datos:
-- Nombre y DNI de la tabla "personas"
-- Nombre de la tabla "Cargo"

-- Como las demás personas ingresadas, no tenían asignadas ningún cargo, simplemente no se muestran