-- Voy a crear una tabla como ejemplo para una planta

create table ingreso_planta(
	pk_id_ingreso_planta int(11) not null auto_increment,
	nombre_puerta varchar(25),

	# Para hablar de dias identificamos la variable como date
	dia_ingreso date,

	# Para hablar de horas identificamos la variable como datetime
	hora_ingreso datetime,

	fk_persona int(11) not null,

	primary key (pk_id_ingreso_planta),
	foreign key (fk_persona) references persona(mi_id_primary)

)engine=innodb;


-- "curdate" te refleja la fecha actual
-- "now" te refleja la hora exacta de la introduccion de los datos
insert into ingreso_planta values(1,"Puerta 01",curdate(),now(),2)

-- Me añadio estos datos en la tabla de "ingreso_planta":
-- pk_id_ingreso_planta			nombre_puerta				dia_ingreso				hora_ingreso				fk_persona
-- 1 							Puerta 01 					2021-05-28 				2021-05-28 19:35:44	 		2

insert into ingreso_planta values(2,"Puerta 01",curdate(),now(),3)

-- Ahora mi tabla de ingreso queda de la siguiente manera:
-- pk_id_ingreso_planta			nombre_puerta				dia_ingreso				hora_ingreso				fk_persona
-- 1 							Puerta 01 					2021-05-28 				2021-05-28 19:35:44	 		2
-- 2 							Puerta 01 					2021-05-28 				2021-05-28 19:45:14 		3