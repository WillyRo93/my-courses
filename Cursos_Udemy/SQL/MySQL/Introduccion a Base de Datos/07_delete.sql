-- Ahora quiero eliminar datos de mis tablas

-- En este caso quiero eliminar justamente lo que añadi en mi archivo anterior, solo para probar
delete from persona where mi_id_primary=1

-- Nuestra tabla, al borrar la data de mi primer trabajador queda entonces de la siguiente manera:

-- mi_id_primary			mi_dni			mi_nombre			mi_edad			foranea_key_cargo
-- 2 						24654123 		Ricardo 			65 				NULL
-- 3 						24543278 		Alberto  			19 				NULL
-- 4 						24654123 		Rigoberto 			43 				NULL
-- 5 						24543278 		Alfredo Gonofredo 	76 				NULL
-- 6 						24654128 		Alfredito 			43 				2
-- 7 						24543274 		Gorgoncho 			76 				1

-- ¿Quieres eliminar todo? Se aplica lo siguiente:

-- delete from persona;

-- NOTA: Si la tabla esta integrada con otra, es decir, con llaves foraneas, no me va a dejar borrar la tabla con "delete"

-- NOTA 2: Se borran los datos, pero la estructura de las tablas sigue intacta.