-- Ahora vamos a eliminar una tabla, ¿como lo hacemos?

-- Nota: Si esta tabla está relacionada a una llave "foranea", es mas complicado que se elimine
-- Por integridad de datos, si está relacionada en otra tabla, no la va a eliminar.

-- Nota 2: Seguimos trabajando con la tabla generada en el archivo anterior
drop table persona;