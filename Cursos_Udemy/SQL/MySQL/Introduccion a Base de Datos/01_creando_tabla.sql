-- ¿Como creamos una tabla?

-- Creo una tabla con "Create Table"
create table persona(
	-- Declaro un integer de dimencion 11, que no permita valores nulos
	-- Luego se auto incrementa 
	-- Al final se le coloca una coma (",") para definir mas valores de mi tabla
	mi_id int(11) not null auto_increment,
	-- A partir de acá, empiezo a definir por ejemplo un dni
	-- Le doy la forma de varchar, pues estos datos solo son de consulta, no hay calculos o cambios que hacer
	mi_dni varchar(8),
	mi_nombre varchar(25),
	mi_edad int(3),
	-- Ahora, debo determinar cual es mi llave primaria
	primary key (mi_id)
	)engine=innodb;

-- Ahora, como se inserta la data? Cuentamelo
insert into persona values(01,"24567893","Alberto",45);

-- Debemos saber que el "mi_id" se incrementará automaticamente
-- Por lo tanto haremos lo siguiente, de manera generica
insert into persona(mi_dni,mi_nombre,mi_edad) values ("24782543","Carlos",23)

-- Finalmente, ¿Como hago para ver los datos de esta tabla?
select * from persona;

-- ¿Que pasa si solo quiero que me muestre el nombre? Pues:
select mi_nombre from persona;