-- Quiero que mis casillas de "mi_nombre" y "mi_dni" tengan otros valores que esto determinando yo mismo
-- El "where" se usa para saber que valor se le va a dar de alguna otra tabla
-- Por ejemplo en este caso, le estamos diciendo que queremos que sea 1, es decir, Jefe

update persona set mi_nombre="Debian", mi_dni="123" where mi_id_primary=1;

-- Cuando se observa el resultado, se puede ver que el DNI y el Nombre cambiaron por lo que se determinó

-- mi_id_primary			mi_dni			mi_nombre			mi_edad			foranea_key_cargo
-- 1 						123				Debian				23 				NULL
-- 2 						24654123 		Ricardo 			65 				NULL
-- 3 						24543278 		Alberto  			19 				NULL
-- 4 						24654123 		Rigoberto 			43 				NULL
-- 5 						24543278 		Alfredo Gonofredo 	76 				NULL
-- 6 						24654128 		Alfredito 			43 				2
-- 7 						24543274 		Gorgoncho 			76 				1

-- Si quisiera tambien por ejemplo editar el campo de "Cargo" y asignarle un campo al trabajador, tambien puedo hacerlo

update persona set mi_nombre="Debian", mi_dni="123", foranea_key_cargo=1 where mi_id_primary=1;

-- Podemos observar que ahora el primer trabajador tiene modificado su campo de "Cargo"
-- mi_id_primary			mi_dni			mi_nombre			mi_edad			foranea_key_cargo
-- 1 						123				Debian				23 				1
-- 2 						24654123 		Ricardo 			65 				NULL
-- 3 						24543278 		Alberto  			19 				NULL
-- 4 						24654123 		Rigoberto 			43 				NULL
-- 5 						24543278 		Alfredo Gonofredo 	76 				NULL
-- 6 						24654128 		Alfredito 			43 				2
-- 7 						24543274 		Gorgoncho 			76 				1

-- Por ultimo, para confirmar de nuevo estos cambios, o para imprimir mi lista de trabajadores con tal cargo, hago lo siguiente:
select p.mi_nombre, p.mi_dni, c.mi_nombre from persona p inner join cargo c on p.foranea_key_cargo=c.mi_id_primary

-- Al hacer todo esto, nos muestra los datos de tres personas personas:
-- mi_nombre	mi_dni		mi_nombre
-- Debian		123 		Jefe
-- Alfredito	24654128	Gerente General
-- Gorgoncho	24543274	Jefe