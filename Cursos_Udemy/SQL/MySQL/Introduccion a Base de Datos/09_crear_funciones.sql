
# El delimitador me permite que lea el codigo y no se rompa luego de leer un punto y coma (;)
delimiter //

	create function contar_registros()
	return integer

	begin
		declare resultado int;
		select count(mi_id_primary) into resultado from ingreso_planta where dia_ingreso=curdate();
		return resultado;
	end//

delimiter ;