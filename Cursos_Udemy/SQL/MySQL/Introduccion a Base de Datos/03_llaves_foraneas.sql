-- Ahora queremos crear "llaves foraneas"

-- Voy a crear una tabla de "cargos"
create table cargo(
	mi_id_primary int(11) not null auto_increment,
	mi_nombre varchar(25),
	
	primary key (mi_id_primary)
	)engine=innodb;

-- Inserto datos a la tabla
insert into cargo(mi_nombre) values("Jefe");
insert into cargo(mi_nombre) values("Gerente General");


-- Entonces, ¿Como hago para hacer la llave foranea?


-- Creo una tabla de "personas" de nuevo, e internamente llavo una llave foranea
-- Esta llave foranea la denominaremos "foranea_key_cargo"
create table persona(

	mi_id_primary int(11) not null auto_increment,
	mi_dni varchar(8),
	mi_nombre varchar(25),
	mi_edad int(3),

	-- Aqui defino mi llave foranea
	-- Esta llave tiene la misma entrada de valores que "mi_id_primary" en la tabla de cargos
	-- Por ahora se comenta el "not null" debido a que esta expresion, exige un valor de entrada
	-- Es decir, nos exigiría que la persona tenga un cargo, y en nuestro ejemplo no queremos que sea obligatorio
	
	-- Asi estaba antes, donde no tomaba valores nulos: foranea_key_cargo int(11) not null,
	-- Ahora está asi, donde si deja tomar valores nulos para los cargos:
	-- Es decir se puede ingresar personal sin asignarle un cargo:
	foranea_key_cargo int(11),
	primary key (mi_id_primary),

	-- Posteriormente, llamo el keyword "foreign key", y referencio esta llave foranea a la tabla "cargo"
	-- Luego de definir eso, introduzco los valores de entrada que tendrá la tabla "cargo"
	foreign key (foranea_key_cargo) references cargo (mi_id_primary)

	)engine=innodb;

-- Ahora inserto datos en la tabla de personas

-- Nota: Esta forma, previamente usada en los archivos anteriores, no nos sirve
--       ¿Por que no nos sirve? Pues  no estamos definiendo un valor de entrada para "foranea_key_cargo"
-- insert into persona values(01,"24567893","Alberto",45);

-- Acá, como si estamos definiendo que valores estamos modificando, es exitoso
insert into persona(mi_dni,mi_nombre,mi_edad) values ("24782543","Carlos",23);

insert into persona(mi_dni,mi_nombre,mi_edad) values ("24654123","Ricardo",65);
insert into persona(mi_dni,mi_nombre,mi_edad) values ("24543278","Alberto",19)

-- Luego, para chequear si todo está bien, reviso la tabla de personas
select * from persona

-- Algo a tomar en cuenta es que si permitimos valores nulos en foranea_key_cargo, nos deja añadir personas
-- Pero estas personas no tendrán cargos laborales
-- De todos modos, se pueden ingresar personas con cargo laboral, ¿Como?
-- Se le debe añadir otra variable de entrada en "insert into persona" y su valor al final de "values"

insert into persona(mi_dni,mi_nombre,mi_edad,foranea_key_cargo) values ("24654128","Alfredito",43,2);
insert into persona(mi_dni,mi_nombre,mi_edad,foranea_key_cargo) values ("24543274","Gorgoncho",76,1)

-- Luego de todo lo ingresado, si queremos revisar la tabla de personas, ella se nos muestra de la siguiente manera:
	
-- mi_id_primary			mi_dni			mi_nombre			mi_edad			foranea_key_cargo
-- 1 						24782543		Carlos				23 				NULL
-- 2 						24654123 		Ricardo 			65 				NULL
-- 3 						24543278 		Alberto  			19 				NULL
-- 4 						24654123 		Rigoberto 			43 				NULL
-- 5 						24543278 		Alfredo Gonofredo 	76 				NULL
-- 6 						24654128 		Alfredito 			43 				2
-- 7 						24543274 		Gorgoncho 			76 				1