class PartyAnimal:
	x = 0

	def party(self):
		self.x = self.x + 1
		print("Muy lejos", self.x)

an = PartyAnimal()

an.party()
an.party()
an.party()

# Podemos usar dir() para encontrar las "capacidades" de la nueva clase creada:
print("El tipo es:", type(an))
print("El Dir es:", dir(an))


# ------------------------------------------

# Resultado:

# El tipo es: <class '__main__.PartyAnimal'>

# El Dir es: ['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__',
# '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__',
# '__le__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__',
# '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', 'party', 'x']


# Muy lejos 1
# Muy lejos 2
# Muy lejos 3