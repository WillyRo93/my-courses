# Assignment 6.5

# Write code using find() and string slicing (see section 6.10) to extract 
# the number at the end of the line below.

# Convert the extracted value to a floating point number and print it out.

text = "X-DSPAM-Confidence:    0.8475"

primer_espacio = text.find(" ")
#print(primer_espacio)
# 19

# Aqui tenemos el texto entero despues del espacio:
texto_despues_del_espacio = text[primer_espacio:]
#print(texto_despues_del_espacio)
#     0.8475


# Ahora, hacemos strip y tenemos solo el valor, luego lo volvemos float:
numero = texto_despues_del_espacio.strip()
numero = float(numero)
print(numero)