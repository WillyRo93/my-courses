# Como buscar el servidor en un correo enviado?

correo = "From stephen.marquard@uct.ac.za Sat Jan 5 09:14:16 2008"

# Aqui buscamos la posicion del @, es decir, donde inicia su servidor de correo
posicion_del_arroba = correo.find("@")
print("La posicion del @ es el index:",posicion_del_arroba)
	#La posicion del @ es el index: 22


# Aqui buscamos donde termina el servidor
	# Nuestro primer parametro es un espacio, estamos buscando donde hay un espacio
	# Nuestro segundo parametro es la posicion de inicio 
posicion_del_espaciado = correo.find(" ",posicion_del_arroba)
print("El servidor de correo termina en la posicion de index:",posicion_del_espaciado)
	# El servidor de correo termina en la posicion de index: 31


# Por lo tanto, el host o servidor es:
servidor = correo[posicion_del_arroba + 1 : posicion_del_espaciado]
print("El servidor del correo es:",servidor)
	# El servidor del correo es: uct.ac.za