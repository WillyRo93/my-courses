# Como usar startswith?

linea = "Please have a nice day"

print(linea.startswith("Please"))
# True

print(linea.startswith("p"))
# False