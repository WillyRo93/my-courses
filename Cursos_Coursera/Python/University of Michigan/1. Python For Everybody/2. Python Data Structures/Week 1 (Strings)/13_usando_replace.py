# Con replace encontramos un valor y lo reemplazamos por otro

saludos = "Hola que tal Bob? Todo bien?"
print(saludos)
# Hola que tal Bob? Todo bien?

# ------------------------------------------------------------------------------------

# Aca cambiamos la palabra Bob por Jane
cambiando_saludo = saludos.replace("Bob","Jane")
print(cambiando_saludo)
# Hola que tal Jane? Todo bien?

# ------------------------------------------------------------------------------------

# Puedes encontrar todas las letras que quieras y reemplazarlas por lo que desees
segundo_cambio = saludos.replace("o","xx")
print(segundo_cambio)
# Hxxla que tal Bxxb? Txxdxx bien?