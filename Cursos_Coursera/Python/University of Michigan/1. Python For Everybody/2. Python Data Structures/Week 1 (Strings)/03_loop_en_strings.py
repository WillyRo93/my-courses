# Aqui vamos a mostrar el index junto con la letra:

fruta = "banana"
index = 0

while index < len(fruta):
	letra = fruta[index]
	print("La letra",letra,"tiene como index el numero:",index)
	index = index + 1

# Respuesta:

#La letra b tiene como index el numero: 0
#La letra a tiene como index el numero: 1
#La letra n tiene como index el numero: 2
#La letra a tiene como index el numero: 3
#La letra n tiene como index el numero: 4
#La letra a tiene como index el numero: 5

#------------------------------------------------------------
# Pero es mas conveniente hacer un ciclo for:

for letra in fruta:
	print(letra)

#b
#a
#n
#a
#n
#a