# Como usar find?

fruta = "banana"

posicion_de_na = fruta.find("na")
print("La primera posicion de na es en el index numero:",posicion_de_na)
# La primera posicion de na es en el index numero: 2


posicion_de_a = fruta.find("a")
print("La primera posicion de a es en el index numero:",posicion_de_a)
# La primera posicion de a es en el index numero: 1


# Si la letra no esta en la palabra te devuelve un "-1"
letra_z = "z"
posicion_de_z = fruta.find(letra_z)
if posicion_de_z == -1 :
	print("La letra",letra_z,"no está en la palabra",fruta)
	# La letra z no está en la palabra banana

