# Contando cuantas "a" tiene banana

palabra = "banana"
letra_a_buscar = "a"
contador = 0

for letra in palabra:
	if letra == "a":
		contador = contador + 1

print("La palabra",palabra,"posee",contador,"veces la letra",letra_a_buscar)

# ----------------------------------------------------------------------------

# Respuesta

# La palabra banana posee 3 veces la letra a