# La funcion "len" se usa para saber la cantidad de letras o valores de un string

fruta = "banana"
longitud = len(fruta)
print(longitud)

# Resultado:
# 6

#-----------------------------------------------------------------------------------

mi_palabra = "externocleidomastoideo"

longitud=len(mi_palabra)

print("La palabra",mi_palabra,"tiene",longitud,"letras")

#---------------------------------------------------------------
#La palabra externocleidomastoideo tiene 22 letras