# Como usar el operador logico "in"?

fruta = "banana"

print("n" in fruta)
# True

print("m" in fruta)
# False

print("nan" in fruta)
# True

if "a" in fruta:
	print("""Encontré una "a" """)