# Como leo un archivo adecuadamente?
# Lo que debo hacer es buscar informacion a traves del texto


# En el primer metodo buscamos todas las lineas que empiecen por la palabra "Todo"
archivo = open("archivo_prueba_semana3.txt")
for linea in archivo:
	linea = linea.rstrip() #Elimino los espacios a la derecha
	if linea.startswith("Todo"):
		print(linea)

# Otra forma es, buscar las lineas que no empiecen por "Todo" y obviarlas:
archivo = open("archivo_prueba_semana3.txt")
for linea2 in archivo:
	linea2 = linea2.rstrip() #Elimino los espacios a la derecha
	if not linea2.startswith("Todo"):   #Con esto paso de largo si no encuentro una linea que empiece por "Todo"
		continue
	print(linea2)



#------------------------------------
# Solo quiero imprimir la frase que empiece por "Todo"

# Todo bien? Pues me alegro un monton
# Todo candela

# Todo bien? Pues me alegro un monton
# Todo candela