#13_prompt_de_error_al_ingresar_nombre_del_texto.py

#Ahora lo que quiero es que si el usuario escribe mal el nombre del texto, le diga que lo intente nuevamente

archivo=input("Introduzca el nombre del archivo:")
try:
	archivo = open(archivo)
except:
	print("El archivo no se pudo abrir:",archivo)
	quit()

conteo=0
for linea in archivo:
	if linea.startswith("Todo"):
		conteo = conteo + 1	

print("En mi archivo habian",conteo,"lineas con la palabra: Todo")

#----------------------------------------------------------------------------------------------------

#En caso de introducir un valor incorrecto:

	#Introduzca el nombre del archivo:sdkjhasd
	#El archivo no se pudo abrir: sdkjhasd

#----------------------------------------------------------------------------------------------------

#En caso de introducir el valor correcto:

	#Introduzca el nombre del archivo:07_archivo_prueba.txt
	#En mi archivo habian 2 lineas con la palabra: Todo