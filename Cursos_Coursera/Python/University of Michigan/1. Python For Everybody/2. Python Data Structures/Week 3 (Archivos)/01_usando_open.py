# Como usar open?


# Para leer el archivo, se usa como parametros el nombre del archivo 
# Y la "r" cuyo significado o uso es "read"
archivo_para_leer = open("archivo_prueba_semana3.txt","r")
print(archivo_para_leer)
	# <_io.TextIOWrapper name='archivo_prueba_semana3.txt' mode='r' encoding='cp1252'>


# Para escribir en un archivo, se usan como parametros el nombre del archivo
# Y la "w" cuyo significado es "write"
archivo_para_escribir = open("archivo_prueba_semana3.txt","w")
print(archivo_para_escribir)
	# <_io.TextIOWrapper name='archivo_prueba_semana3.txt' mode='w' encoding='cp1252'>