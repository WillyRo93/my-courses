# Assignment 7.2

# Write a program that prompts for a file name, then opens that file and reads
# through the file, looking for lines of the form:
#       X-DSPAM-Confidence:    0.8475

# Count these lines and extract the floating point values from each of the lines
# and compute the average of those values and produce an output as shown below. 
# Do not use the sum() function or a variable named sum in your solution.
# You can download the sample data at http://www.py4e.com/code3/mbox-short.txt 
# When you are testing below enter mbox-short.txt as the file name.

# Usa "mbox-short.txt" como el nombre del archivo
nombre_archivo = input("Introduzca el nombre del archivo: ")
archivo = open(nombre_archivo)

total = 0
cantidad_de_valores = 0
for linea in archivo:
	if linea.startswith("X-DSPAM-Confidence:"):
		linea = linea.rstrip()
		index_xdspam = linea.find(" ")
		linea_sin_xdspam = linea[index_xdspam + 1 : ]
		#print(linea)
		# #print(index_xdspam)
		# #print(linea_sin_xdspam)

		valores_float = float(linea_sin_xdspam)
		cantidad_de_valores = cantidad_de_valores + 1
		total   = valores_float + total 
		promedio = total   / cantidad_de_valores
		#print(total ) 
		#print(promedio)


print("Average spam confidence:",promedio)