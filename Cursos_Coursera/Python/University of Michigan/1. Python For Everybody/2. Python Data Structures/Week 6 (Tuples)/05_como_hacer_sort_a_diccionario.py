# Como podemos hacerle un sort a un diccionario?

# Declaramos el diccionario:
d = {"b":10, "c":1, "a":22}

# Aqui vemos los items del diccionario y chequeamos que es lo que deseamos:
print(d.items())
	# dict_items([('b', 10), ('c', 1), ('a', 22)])

# Y acá hacemos un sort de los items que tiene el diccionario:
print(sorted(d.items()))
	# [('a', 22), ('b', 10), ('c', 1)]