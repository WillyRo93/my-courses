# Como comparamos dos tuples?

# ----------------------------------------------------------
# El tuple se va comparando item por item

if (0, 1, 2) < (5, 1, 2):
	print("Si, es menor")
	# True
else:
	print("No, es mayor")

# ---------------------------------------

if (0, 1, 2000000000) < (0, 3, 4):
	print("Si, es menor")
	# True
else:
	print("No, es mayor")

# ----------------------------------------------------------
# Si comparamos con strings tenemos lo siguiente:

if ("Jones", "Sally") < ("Jones", "Sam"):
	print("Si, es menor")
	# True
else:
	print("No, es mayor")

# ---------------------------------------

if ("Jones", "Sally") < ("Adams", "Sam"):
	print("Si, es menor")
	# True
else:
	print("No, es mayor")
	#True