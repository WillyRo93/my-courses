# Como usar sorted?

# Declaro el diccionario:
d = {"c":10, "a":1, "b":22}

# ---------------------------------------------------------------------------
# Declaro una variable "t" la cual va a tener los items del diccionario ordenados
t = sorted(d.items())
print(t)
	# [('a', 1), ('b', 22), ('c', 10)]

# ---------------------------------------------------------------------------
# Y si quiero mis elementos manejados uno por uno, puedo hacer lo siguiente:
for k,v in sorted(d.items()):
	print(k,v)
		# a 1
		# b 22
		# c 10