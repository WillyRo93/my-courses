c = {"c":10, "a":1, "b":22}
lista_temporal = []

# En este paso, invierto los keys con los values:
for k,v in c.items():
	lista_temporal.append((v,k))
print(lista_temporal)
	# [(10, 'c'), (1, 'a'), (22, 'b')]

# Luego puedo ordenar, tanto de manera ascendente como descendente:
lista_temporal = sorted(lista_temporal, reverse=True)
print(lista_temporal)
	# [(22, 'b'), (10, 'c'), (1, 'a')]

lista_temporal = sorted(lista_temporal)
print(lista_temporal)
	# [(1, 'a'), (10, 'c'), (22, 'b')]