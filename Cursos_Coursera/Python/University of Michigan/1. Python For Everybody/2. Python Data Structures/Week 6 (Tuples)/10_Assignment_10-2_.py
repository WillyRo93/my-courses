# Write a program to read through the mbox-short.txt and figure out the 
# distribution by hour of the day for each of the messages. 
# You can pull the hour out from the 'From ' line by finding the time and
# then splitting the string a second time using a colon.

# 		From stephen.marquard@uct.ac.za Sat Jan  5 09:14:16 2008

# Once you have accumulated the counts for each hour, print out the counts, 
# sorted by hour as shown below.

nombre_archivo = input("Enter file:")

if len(nombre_archivo) < 1:
    nombre_archivo = "mbox-short.txt"

archivo = open(nombre_archivo)
diccionario_conteo = {}

for linea in archivo:
	linea = linea.rstrip()
	if not linea.startswith("From "):
		continue
	palabras = linea.split()
	hora_completa = palabras[5]
	horas_minutos_segundos = hora_completa.split(":")
	hora = horas_minutos_segundos[0]
	diccionario_conteo[hora] = diccionario_conteo.get(hora, 0) + 1

	#print(palabras)
	#print(hora_completa)
	#print(horas_minutos_segundos)
	#print(hora)

#print(diccionario_conteo)

diccionario_ordenado_por_keys = sorted(diccionario_conteo.items())
#print(diccionario_ordenado_por_keys)

for k,v in diccionario_ordenado_por_keys:
	print(k,v)

# -----------------------------------------------------------------------------
# Resultado:

# 04 3
# 06 1
# 07 1
# 09 2
# 10 3
# 11 6
# 14 1
# 15 2
# 16 4
# 17 2
# 18 1
# 19 1