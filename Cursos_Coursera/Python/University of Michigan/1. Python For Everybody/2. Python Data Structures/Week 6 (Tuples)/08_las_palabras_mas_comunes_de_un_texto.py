# Abrimos el archivo
archivo = open("romeo.txt")

# --------------------------------------------------------------------------
# Declaramos el diccionario:
diccionario_conteo = {}

# --------------------------------------------------------------------------
# Creamos el diccionario con la cantidad de veces que se repiten las palabras:
for linea in archivo:
	palabras = linea.split()
	for palabra in palabras:
		diccionario_conteo[palabra] = diccionario_conteo.get(palabra, 0) + 1

print(diccionario_conteo)
	# {'But': 1, 'soft': 1, 'what': 1, 'light': 1, 'through': 1, 'yonder': 1, 'window': 1, 
	#  'breaks': 1, 'It': 1, 'is': 3, 'the': 3, 'east': 1, 'and': 3, 'Juliet': 1, 'sun': 2,
	#  'Arise': 1, 'fair': 1, 'kill': 1, 'envious': 1, 'moon': 1, 'Who': 1, 'already': 1,
	#  'sick': 1, 'pale': 1, 'with': 1, 'grief': 1}

# --------------------------------------------------------------------------
# Luego invertimos el valor de las llaves y los valores y lo volvemos una lista
# Esta es una lista de tuples.
lista = []
for key,value in diccionario_conteo.items():
	nuevo_tuple = (value,key)
	lista.append(nuevo_tuple)
print(lista)
	# [(1, 'But'), (1, 'soft'), (1, 'what'), (1, 'light'), (1, 'through'), (1, 'yonder'), (1, 'window'), (1, 'breaks'), 
	# (1, 'It'), (3, 'is'), (3, 'the'), (1, 'east'), (3, 'and'), (1, 'Juliet'), (2, 'sun'), (1, 'Arise'), (1, 'fair'), 
	# (1, 'kill'), (1, 'envious'), (1, 'moon'), (1, 'Who'), (1, 'already'), (1, 'sick'), (1, 'pale'), (1, 'with'), (1, 'grief')]

# --------------------------------------------------------------------------
# Invertimos el orden de la lista, en base a cuantas veces se repite
# Es decir, en base a los values inicales
lista = sorted(lista, reverse=True)
print(lista)
	# [(3, 'the'), (3, 'is'), (3, 'and'), (2, 'sun'), (1, 'yonder'), (1, 'with'), (1, 'window'), (1, 'what'), (1, 'through'), 
	# (1, 'soft'), (1, 'sick'), (1, 'pale'), (1, 'moon'), (1, 'light'), (1, 'kill'), (1, 'grief'), (1, 'fair'), (1, 'envious'), 
	# (1, 'east'), (1, 'breaks'), (1, 'already'), (1, 'Who'), (1, 'Juliet'), (1, 'It'), (1, 'But'), (1, 'Arise')]

# --------------------------------------------------------------------------
# Finalmente, se imprimen (key,value) y tenemos nuestro codigo listo.
for value,key in lista[:10]:
	print(key,value)
		# the 3
		# is 3
		# and 3
		# sun 2
		# yonder 1
		# with 1
		# window 1
		# what 1
		# through 1
		# soft 1