# Como declarar un tuple?
x = ("Glenn", "Sally", "Joseph")

# Como imprimo el 3er valor del tuple?
print(x[2])

# Como imprimo el tuple completo?
print(x)

# Como imprimo el maximo valor de un tuple?
y = (1,9,4,5,6)
print(max(y))

# -----------------------------------------------
# Que no podemos hacer con los tuples?
	z = (5,6,7)

	# No pueden modificarse, son inmutables.
		# Esto no se puede ---->  z[2] = 0 

	# No puedes reordenarlos con sort.
		# Esto no se puede ---->  z.sort()

	# No puedes hacerle un append al tuple.
		# Esto no se puede ---->  z.append(5)

	# Ni tampoco lo puedes poner en reverse.
		# Esto no se puede ---->  z.reverse() 