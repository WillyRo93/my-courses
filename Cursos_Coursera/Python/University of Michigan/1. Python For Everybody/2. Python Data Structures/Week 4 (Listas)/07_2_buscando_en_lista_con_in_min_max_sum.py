#21_buscando_en_una_lista.py

mi_lista=[1,2,4,5,6,78,4546,321,134214,1231]

# ------------------------------------------------------------------------------

#Quiero ver si los numeros "5" y "2342" estan en mi lista de numeros
print(5 in mi_lista)      
# True

print(2342 in mi_lista)
# False

# ------------------------------------------------------------------------------

#Buscando el minimo valor:
print("El minimo valor de mi lista es:",min(mi_lista))
#El minimo valor de mi lista es: 1

print("El maximo valor de mi lista es:",max(mi_lista))
#El maximo valor de mi lista es: 134214

# ------------------------------------------------------------------------------

#Sumando los valores de la lista:
print("La suma de todos mis valores es:",sum(mi_lista))
#La suma de todos mis valores es: 140408

# Si quiero el promedio:
print("El promedio de mi lista es:",sum(mi_lista)/len(mi_lista))
#El promedio de mi lista es: 14040.8