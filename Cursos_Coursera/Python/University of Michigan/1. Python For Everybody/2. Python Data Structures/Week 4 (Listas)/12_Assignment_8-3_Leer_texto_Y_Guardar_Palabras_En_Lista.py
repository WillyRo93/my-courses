# Assignment 8.4

# Open the file romeo.txt and read it line by line. 
# For each line, split the line into a list of words using the split() method.
# The program should build a list of words.
# For each word on each line check to see if the word is already in the list and if not append it to the list.
# When the program completes, sort and print the resulting words in alphabetical order.

# You can download the sample data at http://www.py4e.com/code3/romeo.txt

nombre_archivo = input("Introduzca el nombre del archivo: ")
archivo = open(nombre_archivo)
mi_lista = list()

for linea in archivo:
	linea=linea.rstrip()
	linea=linea.split()
	#print(linea)

	for palabra in linea:
		#print(palabra)
		if palabra not in mi_lista:
			mi_lista.append(palabra)

mi_lista.sort()
print(mi_lista)
