#19_sumando_listas.py

a=[1,2,3]
b=[4,5,6]

c=a+b

print("Mi lista a es:",a)
print("Mi lista b es:",b)
print("La suma de mis listas es:",c)

#----------------------------------------------------
#Mi lista a es: [1, 2, 3]
#Mi lista b es: [4, 5, 6]
#La suma de mis listas es: [1, 2, 3, 4, 5, 6]