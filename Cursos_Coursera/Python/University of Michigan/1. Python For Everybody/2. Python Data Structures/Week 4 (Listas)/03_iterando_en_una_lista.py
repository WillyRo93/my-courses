# Como podemos iterar en una lista?

amigos = ["Jose", "Glenn", "Sally"]

for amigo in amigos:
	print("Feliz navidad", amigo)


print("------------------------------")


for i in range(len(amigos)):
	amigo = amigos[i]
	print("Feliz navidad", amigo)

# Resultado:

# Feliz navidad Jose
# Feliz navidad Glenn
# Feliz navidad Sally
# ------------------------------
# Feliz navidad Jose
# Feliz navidad Glenn
# Feliz navidad Sally