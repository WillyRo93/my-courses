#23_creando_listas_con_split.py

linea1 = "Un monton de    espacios"
sin_espacios1 = linea1.split()
print(sin_espacios1)
# ['Un', 'monton', 'de', 'espacios']


linea2 = "primero;segundo;tercero;cuarto"
sin_espacios2 = linea2.split(";")
print(sin_espacios2)
# ['primero', 'segundo', 'tercero', 'cuarto']