amigos = ["Jose", "Glenn", "Sally"]

# Ordeno la lista y la imprimo:
amigos.sort()
print(amigos)

# Respuesta:

# ['Glenn', 'Jose', 'Sally']