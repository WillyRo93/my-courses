 #17_cambiando_un_valor_en_una_lista.py

#Como cambiamos el valor de una celda de una lista?

loteria=[2,14,24,41,63]
print("Antes de modificar cualquier cosa, mi lista es:",loteria)

loteria[2]=987654
print("Ahora cambio el 3er valor de mi lista y queda: ",loteria)

#--------------------------------------------------------------------
#Antes de modificar cualquier cosa, mi lista es: [2, 14, 24, 41, 63]
#Ahora cambio el 3er valor de mi lista y queda:  [2, 14, 987654, 41, 63]