#22_listas_con_input.py

mi_lista=[]

while True:
	numero_ingresado = input ("Introduzca un valor, y cuando desee finalizar coloque Done   ")
	if numero_ingresado == "Done":
		break
	valor = float (numero_ingresado)
	mi_lista.append(valor)

promedio = sum(mi_lista)/len(mi_lista)
print("El promedio de los valores ingresados es:",promedio)

#-----------------------------------------------------------------------
#Introduzca un valor, y cuando desee finalizar coloque Done   3
#Introduzca un valor, y cuando desee finalizar coloque Done   9
#Introduzca un valor, y cuando desee finalizar coloque Done   5
#Introduzca un valor, y cuando desee finalizar coloque Done   Done
#El promedio de los valores ingresados es: 5.666666666666667