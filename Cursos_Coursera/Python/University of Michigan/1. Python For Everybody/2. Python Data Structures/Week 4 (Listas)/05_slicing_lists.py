# Podemos mostrar datos parciales de alguna tabla?

t = [9, 41, 12, 3, 74, 15]

tabla1 = t[1:3]
tabla2 = t[:4]
tabla3 = t[3:]

print(t)
print(tabla1)
print(tabla2)
print(tabla3)

# Respuesta:

# [9, 41, 12, 3, 74, 15]
# [41, 12]
# [9, 41, 12, 3]
# [3, 74, 15]