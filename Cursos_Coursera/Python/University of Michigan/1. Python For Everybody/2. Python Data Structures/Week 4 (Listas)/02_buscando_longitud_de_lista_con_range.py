#18_buscando_longitud_de_lista.py

#Como cambiamos el valor de una celda de una lista?

loteria = [2,14,24,41,63]

longitud_lista = len(loteria)

rango_lista = range(longitud_lista)

print("El largo de mi lista es de:",longitud_lista,"items")
print("")
print("El rango de esos items es:",rango_lista)

#----------------------------------------------------------
#El largo de mi lista es de: 5 items

#El rango de esos items es: range(0, 5)