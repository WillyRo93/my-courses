# Como imprimir las llaves y los valores con el metodo "items"?

# "counts" es mi diccionario.
counts = {"Chuck" : 34, "Fred": 56, "Jane": 100}

for key,value in counts.items():
	print(key,value)

# Chuck 34
# Fred 56
# Jane 100