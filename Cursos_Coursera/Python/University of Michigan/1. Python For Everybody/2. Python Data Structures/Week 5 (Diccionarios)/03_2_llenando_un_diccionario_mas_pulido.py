# Como podemos llenar un diccionario de manera mas pulida?
# Se usa el metodo llamado "get"

# Como se usa?
# Basicamente es igual al anterior codigo pero se simplifica todo.

diccionario_conteo = {}
nombres = ["Carlos", "Gwen", "Carlos", "William", "Gwen"]

for cada_nombre in nombres:
	diccionario_conteo[cada_nombre] = diccionario_conteo.get(cada_nombre, 0) + 1

print(diccionario_conteo)
	# {'Carlos': 2, 'Gwen': 2, 'William': 1}