# Si queremos contar las palabras que se agregan por medio de un input:

counts = {}
print("Introduzca una linea de texto")
linea = input("")

palabras = linea.split()

print("Palabras:", palabras)

print("Contando...")

for palabra in palabras:
	counts[palabra] = counts.get(palabra,0) + 1
print("Conteo:",counts)

# -----------------------------------------------------

# Resultado:
# Introduzca una linea de texto
# Hey que tal como estas? Yo muy bien, muchas gracias, que bonito dia claro que si, si señor, umjuuuu
# Palabras: ['Hey', 'que', 'tal', 'como', 'estas?', 'Yo', 'muy', 'bien,', 'muchas', 'gracias,',
			#'que', 'bonito', 'dia', 'claro', 'que', 'si,', 'si', 'señor,', 'umjuuuu']
# Contando...
# onteo: {'Hey': 1, 'que': 3, 'tal': 1, 'como': 1, 'estas?': 1, 'Yo': 1, 'muy': 1, 'bien,': 1, 'muchas': 1, 
		# 'gracias,': 1, 'bonito': 1, 'dia': 1, 'claro': 1, 'si,': 1, 'si': 1, 'señor,': 1, 'umjuuuu': 1}