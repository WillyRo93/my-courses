cartera = dict()

cartera["dinero"] = 12
cartera["caramelos"] = 3
cartera["pañuelos"] = 75

print(cartera)
	# {'dinero': 12, 'caramelos': 3, 'pañuelos': 75}

# ------------------------------------------------------------------

# Ahora a uno de los valores del diccionario le sumamos valores:

cartera["caramelos"] = cartera["caramelos"] + 5
print(cartera)
	# {'dinero': 12, 'caramelos': 8, 'pañuelos': 75}

# -------------------------------------------------------------------

# Ahora vamos a intercambiar uno de los valores (value)

cartera["caramelos"] = 8000000
print(cartera)
	# {'dinero': 12, 'caramelos': 8000000, 'pañuelos': 75}
