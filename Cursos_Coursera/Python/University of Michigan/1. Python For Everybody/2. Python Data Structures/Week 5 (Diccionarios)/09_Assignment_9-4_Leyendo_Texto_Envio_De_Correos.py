# Assignment 9.4

# Write a program to read through the mbox-short.txt and figure out 
# who has sent the greatest number of mail messages. 

# The program looks for 'From ' lines and takes the second word
# of those lines as the person who sent the mail. 

# The program creates a Python dictionary that maps the sender's mail address 
# to a count of the number of times they appear in the file. 

# After the dictionary is produced, the program reads through the dictionary 
# using a maximum loop to find the most prolific committer.

nombre_archivo = input("Introduzca el nombre del archivo a leer:")
if len(nombre_archivo) < 1:
    nombre_archivo = "mbox-short.txt"

archivo = open(nombre_archivo)

lista_correos = []
diccionario_correos = {}

for linea in archivo:
	if linea.startswith("From "):
		linea=linea.rstrip()
		linea = linea.split()
		correo = linea[1]
		lista_correos.append(correo)
		#print(linea)
		#print(correo)
#print(lista_correos)


for cada_correo in lista_correos:
	diccionario_correos[cada_correo] = diccionario_correos.get(cada_correo, 0) + 1
print(diccionario_correos)

bigcount = None
bigword = None
for key,value in diccionario_correos.items():
	if bigcount is None or value > bigcount:
		bigword = key
		bigcount = value

#print("""La palabra mas repetida es: " """+ bigword + """ " y se repite""", bigcount, "veces")

print(bigword, bigcount)