# Introducimos el archivo "words.txt" como entrada
nombre_archivo = input("Introduzca el nombre del archivo: ")
archivo = open(nombre_archivo)

counts = {}
for linea in archivo:
	palabras = linea.split()
	for palabra in palabras:
		counts[palabra] = counts.get(palabra, 0) + 1

bigcount = None
bigword = None
for palabra,conteo in counts.items():
	if bigcount is None or conteo > bigcount:
		bigword = palabra
		bigcount = conteo

print("""La palabra mas repetida es: " """+ bigword + """ " y se repite""", bigcount, "veces")

# ---------------------------------------------------------------------------------------------
# Cuando introducimos el archivo "words.txt":

	# Introduzca el nombre del archivo: words.txt
	# La palabra mas repetida es: " to " y se repite 16 veces

# ---------------------------------------------------------------------------------------------
# Cuando introducimos el archivo "clown.txt":

	# Introduzca el nombre del archivo: clown.txt
	# La palabra mas repetida es: " the " y se repite 7 veces