# "counts" es mi diccionario.
counts = {"Chuck" : 1, "Fred": 1, "Jane": 100}

# ----------------------------------------------------------
# Imprimiendo las llaves como si fueran una lista:
print(list(counts))
	# ['Chuck', 'Fred', 'Jane']

# ----------------------------------------------------------
# Imprimiendo las llaves con el metodo "keys"
print(counts.keys())
	# dict_keys(['Chuck', 'Fred', 'Jane'])

# ----------------------------------------------------------
# Imprimiendo los valores con el metodo "values"
print(counts.values())
	# dict_values([1, 1, 100])

# ----------------------------------------------------------
# Imprimiedo los items, en forma de tuple
print(counts.items())
	# dict_items([('Chuck', 1), ('Fred', 1), ('Jane', 100)])