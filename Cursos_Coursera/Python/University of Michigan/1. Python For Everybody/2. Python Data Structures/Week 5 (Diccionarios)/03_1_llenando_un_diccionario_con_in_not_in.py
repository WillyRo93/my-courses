diccionario_conteo = {}
nombres = ["Carlos", "Gwen", "Carlos", "William", "Gwen"]

for cada_nombre in nombres:
	if cada_nombre not in diccionario_conteo:
		diccionario_conteo[cada_nombre] = 1
	else:
		diccionario_conteo[cada_nombre] = diccionario_conteo[cada_nombre] + 1

print(diccionario_conteo)
	# {'Carlos': 2, 'Gwen': 2, 'William': 1}