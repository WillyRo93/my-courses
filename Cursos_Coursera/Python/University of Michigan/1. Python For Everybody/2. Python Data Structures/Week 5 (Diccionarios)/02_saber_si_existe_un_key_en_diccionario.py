# No se puede llamar a una llave que no exista.
# Por lo tanto, debemos averiguar si alguna llave existe antes de llamarla.
# Como lo hacemos?

mi_diccionario = {}

if "alguna_key" in mi_diccionario:
	print(""" Si, existe la key llamada "alguna_key" """)
else:
	print("""No, no existe la key llamada "alguna_key" """)

#No, no existe la key llamada "alguna_key"