#20_metodos_listas.py

a=[]

# Entendiendo que mi lista inicial es: [3453, 3454353543543, 10, 24]

a.append(3453)
a.append(3454353543543)
a.append(10)
a.append(24) #Agrega valores en la lista creada, por ejemplo: [3453, 3454353543543, 10, 24]

#a.clear()

#a.copy()

#a.count()

#a.extend()

#a.index()

#a.insert()

#a.pop()

#a.remove()

a.reverse() # Cambia el orden de la lista, queda de atras para adelante: [24, 10, 3454353543543, 3453]

a.sort() #Ordena la lista, por ejemplo queda: [10, 24, 3453, 3454353543543]


print(a)


# pagina de documentacion:

# http://docs.python.org/tutorial/datastructures.html