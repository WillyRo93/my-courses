# Como y para que se usa center?

# DOCUMENTACION:

# Return centered in a string of length width. Padding is done using the specified fillchar (default is an ASCII space). 
# The original string is returned if width is less than or equal to len(s).

# Syntax:
# string.center(length, character)

# Parameter Values:
# length ----->	Required. The length of the returned string
# character -->	Optional. The character to fill the missing space on each side. Default is " " (space)

saludo = "Hola"
saludo = saludo.center(10,"-")
print(saludo)
# ---Hola---

print(len(saludo))
# 10 (Este es el primer valor que se coloca, la longitud)

epa = "Epale"
epa = epa.center(15)
print(epa)
#      Epale

# Deja los espacios en blanco en este caso, y centra el texto