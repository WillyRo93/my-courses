# Como y para que se usa "casefold"?

# DOCUMENTACION:
# Return a casefolded copy of the string. Casefolded strings may be used for caseless matching.

# Casefolding is similar to lowercasing but more aggressive because it is intended to 
# remove all case distinctions in a string. 

#For example, the German lowercase letter 'ß' is equivalent to "ss". 
# Since it is already lowercase, lower() would do nothing to 'ß'; casefold() converts it to "ss".

saludo = "hOLAS que Thal chabalINes coñooooññññññ   ß ß"

saludo = saludo.casefold()

print(saludo)
# holas que thal chabalines coñooooññññññ   ss ss
