# Como y cuando usar encode?

# The encode() method encodes the string, using the specified encoding. 
# If no encoding is specified, UTF-8 will be used.

# Syntax:
# string.encode(encoding=encoding, errors=errors)

# Parameter Values:
# encoding	--> Optional. A String specifying the encoding to use. Default is UTF-8
# errors	--> Optional. A String specifying the error method. Legal values are:

				# 'backslashreplace'	- uses a backslash instead of the character that could not be encoded
				# 'ignore'	- ignores the characters that cannot be encoded
				# 'namereplace'	- replaces the character with a text explaining the character
				# 'strict'	- Default, raises an error on failure
				# 'replace'	- replaces the character with a questionmark
                # 'xmlcharrefreplace'	- replaces the character with an xml character

txt = "My name is Ståle"

print(txt.encode(encoding="ascii",errors="backslashreplace"))
print(txt.encode(encoding="ascii",errors="ignore"))
print(txt.encode(encoding="ascii",errors="namereplace"))
print(txt.encode(encoding="ascii",errors="replace"))
print(txt.encode(encoding="ascii",errors="xmlcharrefreplace"))

# b'My name is St\\xe5le'
# b'My name is Stle'
# b'My name is St\\N{LATIN SMALL LETTER A WITH RING ABOVE}le'
# b'My name is St?le'
# b'My name is Ståle'