# Como y para que se usa count?

# DOCUMENTACION:

# count(sub[, start[, end]])
# Return the number of non-overlapping occurrences of substring sub in the range [start, end].
# Optional arguments start and end are interpreted as in slice notation.

# Syntax:
# string.count(value, start, end)

# Parameter Values:
# value	--> Required. A String. The string to value to search for
# start	--> Optional. An Integer. The position to start the search. Default is 0
# end	--> Optional. An Integer. The position to end the search. Default is the end of the string

txt = "I love apples, apple are my favorite fruit"

x = txt.count("a", 10, 24)
y = txt.count("a")
w = txt.count("apple",10,24)
z = txt.count("apple")

print(x)
# 2

print(y)
# 4

print(w)
# 1

print(z)
# 2