# Como y para que se usa expandtabs

# The expandtabs() method sets the tab size to the specified number of whitespaces.

# Syntax:
# string.expandtabs(tabsize)

# Parameter Values
# Parameter	Description
# tabsize	--> Optional. A number specifying the tabsize. Default tabsize is 8

txt = "H\te\tl\tl\to"
x =  txt.expandtabs(2)
print(x)
# H e l l o


txt = "H\te\tl\tl\to"
print(txt)
# H       e       l       l       o

print(txt.expandtabs())
# H       e       l       l       o

print(txt.expandtabs(2))
# H e l l o

print(txt.expandtabs(4))
# H   e   l   l   o

print(txt.expandtabs(10))
# H         e         l         l         o