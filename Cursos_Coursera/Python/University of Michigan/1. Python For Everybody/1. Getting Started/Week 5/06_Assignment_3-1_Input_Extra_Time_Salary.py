# Assignment 3.1

# Write a program to prompt the user for hours and rate per hour using input to compute gross pay.
# Pay the hourly rate for the hours up to 40 and 1.5 times the hourly rate for all hours worked above 40 hours.
# Use 45 hours and a rate of 10.50 per hour to test the program (the pay should be 498.75).
# You should use input to read a string and float() to convert the string to a number. 
# Do not worry about error checking the user input - assume the user types numbers properly.

horas = input("Enter Hours: ")
try:
	horas = float(horas)
except:
	salida_horas = -1
	print("Por favor introduzca un valor numerico para las horas / Please enter a number")

rate = input("Enter rate: ")
try:
	rate = float(rate)
except:
	salida_rate = -1 
	print("Por favor introduzca un valor numerico para rate / Please enter a number")

try:
	if horas > 40 :
		horas_extras = horas - 40
		pago_extra = horas_extras * 1.5 * rate
		pago = (40*rate) + pago_extra
	else:
		pago = horas*rate
	print(pago)
except:
	salida = "Uno de los dos valores introducidos no es numerico, por favor arreglelo / At least one of the elements is not a number"
	print(salida)