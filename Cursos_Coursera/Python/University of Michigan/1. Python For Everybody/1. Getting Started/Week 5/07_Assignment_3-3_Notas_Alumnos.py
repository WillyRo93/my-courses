# Assignment 3.3

# Write a program to prompt for a score between 0.0 and 1.0.
# If the score is out of range, print an error.
# If the score is between 0.0 and 1.0, print a grade using the following table:

# Score Grade
# >= 0.9 A
# >= 0.8 B
# >= 0.7 C
# >= 0.6 D
# < 0.6 F
# If the user enters a value out of range, print a suitable error message and exit.
# For the test, enter a score of 0.85.

score = input("Enter Score: ")
try:
	score = float (score)
except:
	score = -1

if score > 1 or score < 0:
	nota = "Por favor introduzca un valor entre 0.0 y 1.0 / Please enter a number between 0.0 and 1.0"
elif score >= 0.9 :
	nota = "A"
elif score >= 0.8:
	nota = "B"
elif score >= 0.7:
	nota = "C"
elif score >= 0.6:
	nota = "D"
elif score < 0.6:
	nota = "F"

print(nota)