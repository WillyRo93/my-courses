# Try - Except se usa para hacer pruebas
# Donde sabes que el codigo puede fallar
# Si el codigo falla puedes imprimir un mensaje o seguir con tu codigo

mensaje="Hola Bob"

# Aqui por ejemplo voy a intentar volver integer a "mensaje", lo cual va a fallar
try:
	integer_mensaje=int(mensaje)
except:
	integer_mensaje=-1

print("Esto dará error, por lo tanto el valor que saldrá es:",integer_mensaje)


numero="123"

try:
	integer_numero=int(numero)
except:
	integer_numero=-1

print("Este valor si puede ser convertido a integer:",integer_numero)

#------------------------------------------------------------------------------------

#Esto dará error, por lo tanto el valor que saldrá es: -1
#Este valor si puede ser convertido a integer: 123