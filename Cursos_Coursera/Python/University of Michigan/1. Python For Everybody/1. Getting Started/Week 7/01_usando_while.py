# La manera de usar el while es cambiando el valor de entrada
# Si esto no se hace, se puede hacer un loop infinito

n=5
while n>0:
	print(n)
	n=n-1

print("Listo, se enterminó lo que se endaba")
print("")
print("Este es el valor final de n: ",n)