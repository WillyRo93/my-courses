# Como reviso una lista por medio de booleanos?

# Aqui hago una busqueda con mi primera lista:
encontrado = False

mi_primera_lista = [9,41,74,12,15]
for valor in mi_primera_lista:
	if valor == 12:
		encontrado = True

print("----------------------------------------------------")
print("La primera lista es:",mi_primera_lista)
print("Encontré algun 12 en la primera lista?",encontrado)

# Aqui hago la busqueda con una segunda lista:
encontrado = False

mi_segunda_lista = [9,41,74,0,15]
for valor in mi_segunda_lista:
	if valor == 12:
		encontrado = True

print("----------------------------------------------------")
print("La primera lista es:",mi_segunda_lista)
print("Encontré algun 12 en la segunda lista?",encontrado)