# Como contar la cantidad de valores que tengo en una lista?

conteo=0
print("Antes de empezar, tenia: ",conteo,"valores contados")

mi_lista=[9,41,74,12,15]
for cosa in mi_lista:
	conteo=conteo+1

print("Luego de revisar la lista: ",mi_lista)
print("Puedo ver que tengo: ",conteo," valores")

#-------------------------------------------------------------

# Antes de empezar, tenia:  0 valores contados
# Luego de revisar la lista:  [9, 41, 74, 12, 15]
# Puedo ver que tengo:  5  valores