# Como se usa break?

# En este caso lo usamos en un loop, donde introducimos valores
# Hasta el momento en que deseamos terminar el ciclo, introduciendo la palabra "Done"

while True:
	linea = input("Introduzca un numero: ")
	if linea == "Done" :
		break
	print("El numero o valor introducido es:",linea)
	print("""Si desea que el código deje de correr Introduzca la palabra "Done" """)
	print("--------------------------------------------------------------------")
print("Listo!!")