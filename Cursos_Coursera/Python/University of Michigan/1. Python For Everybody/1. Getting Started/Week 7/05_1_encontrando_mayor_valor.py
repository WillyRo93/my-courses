# Como puedo hacer para encontrar el valor mas grande de una lista?

mas_largo_hasta_ahora=None
print("Antes, el valor mas grande era: ",mas_largo_hasta_ahora)

mi_lista=[9,32,41,12,3,76,15]

for numero in mi_lista:
	if mas_largo_hasta_ahora is None:
		mas_largo_hasta_ahora=numero

	elif numero > mas_largo_hasta_ahora:
		mas_largo_hasta_ahora=numero

print("Luego de ver la lista: ",mi_lista)
print("Puedo decir que el mas grande es, aparte del Dieguito Maradroga:",mas_largo_hasta_ahora)

#-------------------------------------------------------------------------------------------
#Antes, el valor mas grande era:  None
#Luego de ver la lista:  [9, 32, 41, 12, 3, 76, 15]
#Puedo decir que el mas grande es, aparte del Dieguito Maradroga: 76