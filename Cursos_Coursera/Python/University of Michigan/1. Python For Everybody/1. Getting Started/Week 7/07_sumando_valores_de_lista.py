# Como sumar los valores de una lista?

suma=0
print("Antes de empezar, tenia como suma: ",suma)

mi_lista=[9,41,74,12,15]
for cosa in mi_lista:
	suma=cosa+suma

print("Luego de revisar la lista: ",mi_lista)
print("Puedo ver que tengo la siguiente suma: ",suma)

# -----------------------------------------------------
# Antes de empezar, tenia como suma:  0
# Luego de revisar la lista:  [9, 41, 74, 12, 15]
# Puedo ver que tengo la siguiente suma:  151