# Como encuentro el menor valor? Es similar a encontrar el mayor valor

menor_valor_hasta_ahora=None
print("Antes, el menor valor era: ",menor_valor_hasta_ahora)

mi_lista=[9,32,41,12,3,76,15]

for numero in mi_lista:
	if menor_valor_hasta_ahora is None:
		menor_valor_hasta_ahora=numero

	elif numero < menor_valor_hasta_ahora:
		menor_valor_hasta_ahora=numero

print("Luego de ver la lista: ",mi_lista)
print("Puedo decir que el menor valor es:",menor_valor_hasta_ahora)

# -------------------------------------------------------------------------------------------
# Antes, el valor mas grande era:  None
# Luego de ver la lista:  [9, 32, 41, 12, 3, 76, 15]
# Puedo decir que el menor valor es: 3