# Como filtro valores en una lista?
# Quiero encontrar valores mayores a 20

print("--------------------------")
conteo = 0

mi_lista=[9,41,74,12,15]
for valor in mi_lista:
	if valor > 20:
		conteo = conteo + 1
		print(valor,"es mayor que",20)

print("Se encontraron",conteo,"valores mayores a 20")