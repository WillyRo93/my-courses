# Cuando se usa continue?

# Cuando queremos terminar una iteracion o un loop
# Usamos "continue" para iniciar la siguiente iteracion o loop

# Acá usamos el continue para no imprimir los valores que empiecen por "#"
while True:
	linea = input("Introduzca un numero o valor: ")
	if linea[0] == "#" :
		continue
	if linea == "Done" :
		break
	print("El valor intruducido es:",linea)
	print("""Si desea culminar el ciclo, introduzca la palabra "Done" """)
	print("------------------------------")
print("Listo!!!")