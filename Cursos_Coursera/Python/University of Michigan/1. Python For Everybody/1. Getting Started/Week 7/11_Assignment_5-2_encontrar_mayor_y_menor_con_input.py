#Assignment 5.2

# Write a program that repeatedly prompts a user for integer numbers until 
# the user enters 'done'. Once 'done' is entered, print out the largest 
# and smallest of the numbers. 
# If the user enters anything other than a valid number catch it with 
# a try/except and put out an appropriate message and ignore the number.
# Enter 7, 2, bob, 10, and 4 and match the output below.

lista_de_numeros = []

# Aqui definimos una funcion para encontrar el mayor valor
def mayor_valor (una_lista):
	mas_largo_hasta_ahora=None

	for numero in una_lista:
		if mas_largo_hasta_ahora is None:
			mas_largo_hasta_ahora=numero

		elif numero > mas_largo_hasta_ahora:
			mas_largo_hasta_ahora=numero

	return(mas_largo_hasta_ahora)

# Aqui definimos una funcion para encontrar el menor valor
def menor_valor (una_lista):
	menor_valor_hasta_ahora=None

	for numero in una_lista:
		if menor_valor_hasta_ahora is None:
			menor_valor_hasta_ahora=numero

		elif numero < menor_valor_hasta_ahora:
			menor_valor_hasta_ahora=numero

	return(menor_valor_hasta_ahora)

# Aqui buscamos que el usuario ingrese los valores
while True:
    numero = input("""Si desea finalizar el proceso, introduzca "listo". Si no, introduzca un valor: """)
    if numero == "listo":
        break
    else:
    	try:
    		numero=int(numero)
    	except:
    		numero="invalido"
    	if numero == "invalido":
    		print("Invalid input")
    	else:
    		lista_de_numeros.append(numero)

# print(lista_de_numeros)

mas_grande = mayor_valor(lista_de_numeros)
mas_pequeno = menor_valor(lista_de_numeros)

print("Maximum is", mas_grande)
print("Minimum is",mas_pequeno)

# ---------------------------------------------------------------------------
# Respuesta:

# Invalid input
# Maximum is 10
# Minimum is 2