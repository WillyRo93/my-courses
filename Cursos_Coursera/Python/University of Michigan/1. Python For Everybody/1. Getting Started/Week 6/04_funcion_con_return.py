# Si no coloco el return, la funcion no me devuelve ningun valor

#Hago input de mis valores y luego los vuelvo un numero, ya que ellos entran como letra
num_1=input("Primer valor a sumar: ")
num_1=int(num_1)

num_2=input("Segundo valor a sumar: ")
num_2=int(num_2)

#Aca defino mi funcion
def sumando_dos_valores(a,b):
	suma=a+b
	return suma

#Aqui estoy finalmente llamando a mi funcion, para luego imprimir su resultado
x = sumando_dos_valores(num_1,num_2)
print("El resultado de la suma es: ",x)

#---------------
# El resultado de la suma es:  8