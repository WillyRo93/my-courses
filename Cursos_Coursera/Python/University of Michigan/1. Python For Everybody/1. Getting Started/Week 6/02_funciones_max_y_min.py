# Aca tenemos dos funciones: max y min

#Aca buscamos la letra con mayor valor
big=max("Hello world")

#Aca buscamos la letra con menor valor
tiny=min("Hello world")

print("La letra con mayor valor es :",big)
print("La letra con menor valor es :",tiny)

# ----------------------------------------------
# La respuesta que me muestra es:
# La letra con mayor valor es : w
# La letra con menor valor es :