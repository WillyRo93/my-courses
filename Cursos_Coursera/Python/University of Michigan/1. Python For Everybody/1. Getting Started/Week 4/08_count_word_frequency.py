# Abro "Archivo.txt"
archivo = input("Introduzca el nombre del archivo: ")
handle = open(archivo,"r")

# Luego, para encontrar la frecuencia de una palabra en el archivo:

conteo = dict()

for linea in handle:
	palabras = linea.split()
	for palabra in palabras:
		conteo[palabra] = conteo.get(palabra,0) + 1

print("Aqui podemos ver la lista de palabras encontradas")
print(conteo)