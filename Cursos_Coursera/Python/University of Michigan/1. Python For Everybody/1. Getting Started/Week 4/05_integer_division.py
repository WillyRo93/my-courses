# Las divisiones dan un float como resultado

print(10/2)
# Respuesta: 5.0

print(9/2)
# Respuesta: 4.5

print(99/100)
# Respuesta: 0.99

print(10.0/2.0)
# Respuesta: 5.0

print(99.0/100.0)
# Respuesta: 0.99