# Abro "Archivo.txt"
archivo = input("Introduzca el nombre del archivo: ")
handle = open(archivo,"r")

# Luego, para encontrar la frecuencia de una palabra en el archivo:
conteo = dict()

for linea in handle:
	palabras = linea.split()
	for palabra in palabras:
		conteo[palabra] = conteo.get(palabra,0) + 1


# Luego, para encontrar la palabra que mas se repite:
big_count = None
big_word = None

for palabra,repetida in conteo.items():
	if big_count is None or repetida > big_count:
		big_word = palabra 
		big_count = repetida

print("La palabra mas repetida es:",big_word,"y se repite",big_count,"veces.")