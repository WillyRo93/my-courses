# Como manejar XML con multiples tags?

import xml.etree.ElementTree as ET

entrada = """
<stuff>
	<users>
		<user x="2">
			<id>001</id>
			<name>Chuck</name>
		</user>

		<user x="7">
			<id>009</id>
			<name>Brent</name>
		</user>
	</users>
</stuff>
"""

stuff = ET.fromstring(entrada)
lista = stuff.findall("users/user")
print("User count:", len(lista))

for item in lista:
	print("Nombre:", item.find("name").text)
	print("ID:",item.find("id").text)
	print("Atrribute:", item.get("x"))
	print("\n")

#-------------------------------------------------
# Respuesta:

# User count: 2
# Nombre: Chuck
# ID: 001
# Atrribute: 2

# Nombre: Brent
# ID: 009
# Atrribute: 7