import xml.etree.ElementTree as ET

data = """
<person>
	<name>Chuck</name>
	<phone type="intl">	
		+1 734 303 4456
	</phone>
	<email hide = "yes"/>
</person>
"""

tree = ET.fromstring(data) #Esto toma un string y lo convierte en un arbol 
print("Name:", tree.find("name").text)
print("Attributes:", tree.find("email").get("hide"))
print("Correo:",tree.find("phone").get("type"))