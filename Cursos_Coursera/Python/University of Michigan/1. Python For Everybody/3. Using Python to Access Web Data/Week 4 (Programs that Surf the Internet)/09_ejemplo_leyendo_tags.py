import urllib.request, urllib.parse, urllib.error

from bs4 import BeautifulSoup

import ssl

# Aqui ignoramos los errores de certificado, buscar significado en Stack Overflow
ctx = ssl.create_default_context()
ctx.check_hostname = False 
ctx.verify_mode = ssl.CERT_NONE

# Acá pedimos al usuario la pagina web que será procesada
# Podemos usar como paginas web:
# 	http://www.dr-chuck.com/
#	https://www.si.umich.edu/

url = input("Introduzca una pagina web:")
html = urllib.request.urlopen(url, context=ctx).read()
soup = BeautifulSoup(html, "html.parser")

# Tomamos los tags que queremos, en este caso serán los anchor
tags = soup("a")
numero_de_tags = 0
for tag in tags:
	numero_de_tags = numero_de_tags + 1
	print("El tag #",numero_de_tags, "es:", tag.get("href",None))