# Following Links in HTML Using BeautifulSoup

import urllib.request, urllib.parse, urllib.error

# --------------------------------------------------------------------------------------
# Esto de acá lo hago para comprender como viene la informacion del html y visualizarla.
# Actualmente está comentado, pero sirve para visualizar el html.
#fhand = urllib.request.urlopen("http://py4e-data.dr-chuck.net/known_by_Fikret.html")
#for line in fhand:
#	print(line.decode().strip())

from bs4 import BeautifulSoup

import ssl

# Aqui ignoramos los errores de certificado, buscar significado en Stack Overflow
ctx = ssl.create_default_context()
ctx.check_hostname = False 
ctx.verify_mode = ssl.CERT_NONE

# Aca verdaderamente empieza mi codigo
url = input("Introduzca una pagina web: ")
repeticion = int (input("Cuantas veces quieres repetir el proceso? ") )
posicion = int ( input("Que numero de posicion quiere tomar ") )
print("\n")

while repeticion > 0 :
	print("Procesando la pagina numero",repeticion,",la cual es:",url)
	repeticion = repeticion - 1

	# Aqui leemos la pagina
	html = urllib.request.urlopen(url, context=ctx).read()
	soup = BeautifulSoup(html, "html.parser")

	# Queremos el tag "a", este contiene la informacion que buscamos
	tags = soup("a")
	

	print("En esta pagina, el nombre de la persona numero",posicion,"es:",tags[posicion -1].contents[0],"\n")
	url = tags[posicion - 1].get('href', None)