import urllib.request, urllib.parse, urllib.error

from bs4 import BeautifulSoup

import ssl

# Aqui ignoramos los errores de certificado, buscar significado en Stack Overflow
ctx = ssl.create_default_context()
ctx.check_hostname = False 
ctx.verify_mode = ssl.CERT_NONE

# Acá pedimos al usuario la pagina web que será procesada
# Podemos usar como paginas web:
# 	http://www.dr-chuck.com/
#	https://www.si.umich.edu/

url = input("Introduzca una pagina web:")
html = urllib.request.urlopen(url, context=ctx).read()
soup = BeautifulSoup(html, "html.parser")

# Hasta aqui llega lo que ya hemos manejado
# ---------------------------------------------------------------------------------------

#Luego a partir de acá tenemos como procesar las distintas tags de anchor

# Retrieve all of the anchor tags
tags = soup('a')
for tag in tags:
   # Look at the parts of a tag
   print ('TAG:',tag)
   print ('URL:',tag.get('href', None))
   print ('Contents:',tag.contents[0])
   print ('Attrs:',tag.attrs)