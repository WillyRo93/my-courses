# When we talk to an external resource like a network socket we send bytes, so we need
# to encode Python strings into a given character encoding.

# When we read data from an external resource, we must decode it based on the character
# set so it is properly represented in Python as a string.

while True:
	data = mysock.rcv(512)
	if (len (data) < 1) :
		break
	mystring = data.decode()
	print(mystring)