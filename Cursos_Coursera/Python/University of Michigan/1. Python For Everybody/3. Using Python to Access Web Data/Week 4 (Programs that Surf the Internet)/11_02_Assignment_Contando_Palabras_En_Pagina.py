import urllib.request, urllib.parse, urllib.error

# --------------------------------------------------------------------------------------
# Esto de acá lo hago para comprender como viene la informacion del html y visualizarla.
# Actualmente está comentado, pero sirve para visualizar el html.
# fhand = urllib.request.urlopen("http://py4e-data.dr-chuck.net/comments_42.html")
# for line in fhand:
#	print(line.decode().strip())
# --------------------------------------------------------------------------------------

from bs4 import BeautifulSoup

import ssl

# Aqui ignoramos los errores de certificado, buscar significado en Stack Overflow
ctx = ssl.create_default_context()
ctx.check_hostname = False 
ctx.verify_mode = ssl.CERT_NONE

# Acá pedimos la pagina web que será procesada
# Usaremos como paginas web:
# 	Sample Data:   http://py4e-data.dr-chuck.net/comments_42.html
#	Actual Data:   http://py4e-data.dr-chuck.net/comments_1346638.html

url = input("Introduzca una pagina web:")
html = urllib.request.urlopen(url, context=ctx).read()
soup = BeautifulSoup(html, "html.parser")

# Basandonos en "10_como_usar_tags.py", tomamos los tags que queremos.
tags = soup("span")
numero_de_tags = 0
for tag in tags:
	numero_de_tags = numero_de_tags + 1
	# print("El tag #",numero_de_tags, "es:", tag.get("class",None))
	# print ('TAG:',tag)
	# print ('URL:',tag.get('href', None))
	# print ('Contents:',tag.contents[0])
	# print ('Attrs:',tag.attrs)

# --------------------------------------------------------------------------------------
# Si usamos tags = soup("tr"), el formato viene siendo algo como:

	# <tr><td>Modu</td><td><span class="comments">90</span></td></tr>
	# <tr><td>Kenzie</td><td><span class="comments">88</span></td></tr>
	# <tr><td>Hubert</td><td><span class="comments">87</span></td></tr>

# Las primeras respuestas que me va dando son:

# El tag # 51 es: None
# TAG: <tr><td>Inika</td><td><span class="comments">2</span></td></tr>
# URL: None
# Contents: <td>Inika</td>
# Attrs: {}
# --------------------------------------------------------------------------------------
# Si usamos tags = soup("span"), el formato viene siendo algo como:

	# TAG: <span class="comments">2</span>
	# Contents: 2
# --------------------------------------------------------------------------------------

# Por lo tanto, ya con esto vemos como proceder para encontrar los valores o contenidos:

tags = soup("span")
suma_de_contenidos = 0
for tag in tags:
	suma_de_contenidos = suma_de_contenidos + int(tag.contents[0])
	#print ('Contents:',tag.contents[0])

print("\n")
print("La suma de todos los valores es:", suma_de_contenidos)