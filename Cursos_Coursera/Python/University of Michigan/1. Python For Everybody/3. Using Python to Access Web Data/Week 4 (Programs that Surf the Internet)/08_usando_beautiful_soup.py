# Para que usamos Beautiful Soup?
# En este caso queremos que nuestra salida sea limpia y libre de errores.
# Ademas queremos tener algun header dentro de todo el html que se imprime.

import urllib.request, urllib.parse, urllib.error

from bs4 import BeautifulSoup

# Aqui pido que ingresen la pagina, en nuestra prueba usaremos: http://www.dr-chuck.com/page1.htm
url = input ("Introduzca la pagina web: ")
print("\n")

html = urllib.request.urlopen(url).read()

soup = BeautifulSoup(html, "html.parser")

# Quiero que me de todas las anchor tags
tags = soup("a")
for tag in tags:
	print( tag.get("href", None) )


# Respuesta:
	# http://www.dr-chuck.com/page2.htm