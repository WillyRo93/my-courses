# Python has built-in support TCP Sockets

import socket

mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mysock.connect( ("data.pr4e.org", 80))


print(mysock)
	# "data.pr4e.org" es el host
	# "80" es el port

# Respuesta:

	# <socket.socket fd=376, family=AddressFamily.AF_INET, type=SocketKind.SOCK_STREAM, proto=0, 
	# laddr=('172.17.2.212', 49519), raddr=('192.241.136.170', 80)>