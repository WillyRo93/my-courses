# Finding Numbers in a Haystack

# In this assignment you will read through and parse a file with text and numbers. 
# You will extract all the numbers in the file and compute the sum of the numbers.

# Data Files.
# We provide two files for this assignment. One is a sample file where we give you the sum
# for your testing and the other is the actual data you need to process for the assignment.

	# ° Sample data: http://py4e-data.dr-chuck.net/regex_sum_42.txt (There are 90 values with a sum=445833)
	# ° Actual data: http://py4e-data.dr-chuck.net/regex_sum_1346636.txt (There are 81 values and the sum ends with 735)

# Data Format.
# The file contains much of the text from the introduction of the textbook except that random numbers 
# are inserted throughout the text. Here is a sample of the output you might see:

		# Why should you learn to write programs? 7746
		# 12 1929 8827
		# Writing programs (or programming) is a very creative 
		# 7 and rewarding activity.  You can write programs for 
		# many reasons, ranging from making your living to solving
		# 8837 a difficult data analysis problem to having fun to helping 128
		# someone else solve a problem.  This book assumes that 
		# everyone needs to know how to program ...

# The sum for the sample text above is 27486. The numbers can appear anywhere in the line.
# There can be any number of numbers in each line (including none).

# Handling The Data
# The basic outline of this problem is to read the file, look for integers using the re.findall(), 
# looking for a regular expression of '[0-9]+' and then converting the extracted strings to integers and summing up the integers.


# -----------------------------------------------------------------------------------------------------------
# La asignacion es leer todo un texto y sacar la suma de los valores numericos
# En el texto de ejemplo "regex_sum_42.txt" la suma de los 90 valores debe ser de 445833

# El codigo a continuacion está basado en "12_codigo_ejemplo_spam_confidence.py"

import re

input_archivo = input("Introduzca el nombre del archivo: ")
handle = open(input_archivo)
numlist = list()
conteo = 0

for line in handle:
	line = line.rstrip()
	stuff = re.findall("([0-9]+)", line)
	for i in stuff:
		try:
			numero = int(i)
			numlist.append(numero)
			conteo = conteo + numero
			# print("El numero actual es:", numero)
		except:
			x = -1
	#print(stuff)

print(conteo)
print(len(numlist))