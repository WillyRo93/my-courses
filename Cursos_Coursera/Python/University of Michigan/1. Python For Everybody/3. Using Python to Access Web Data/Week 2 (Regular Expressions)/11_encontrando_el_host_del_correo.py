import re

line = "From stephen.marquard@uct.ac.za Sat Jan 5 09:14:16 2008"
y = re.findall("@([^ ]*)", line)
print(y)

# Explicando: "@([^ ]*)"
	# @ 	Look through the string until you find an at sign
	# [^ ]  Match non-blank character
	# * 	Match any of them
	# Basicamente, luego de encontrar el arroba, tomas lo siguiente despues de el.

# Respuesta:
	# ['uct.ac.za']

# -------------------------------------------------------------------------------------------

# Otra manera de hacerlo es con: "^From .*@([^ ]*)"

y = re.findall("^From .*@([^ ]*)", line)
print(y)

# Explicando: "^From .*@([^ ]*)"
	# ^From Comienza la linea con la palabra "From", luego un espacio y de nuevo lo mismo:
		# .*    Quiero cualquier numero de caracteres hasta encontrar un arroba
		# @ 	Look through the string until you find an at sign
		# [^ ]  Match non-blank character
		# * 	Match any of them
		# Basicamente, luego de encontrar el arroba, tomas lo siguiente despues de el.

# Respuesta:
	# ['uct.ac.za']