# Los parentesis NO son parte del match
# Los parentesis indican donde inicia y donde se detiene la extraccion de strings.

import re

x = "From stephen.marquard@uct.ac.za Sat Jan 5 09:14:16 2008"

y = re.findall("^From (\S+@\S+)", x)
print(y)

# Explicando, tenemos:
	# ^From      Inicia con From, pero no forma parte del resultado, pues no está en el parentesis.
	# (\S+@\S+)  Por lo menos un "no-whitespace character", es lo que se va a tomar como resultado.

# Respuesta:
	# ['stephen.marquard@uct.ac.za']