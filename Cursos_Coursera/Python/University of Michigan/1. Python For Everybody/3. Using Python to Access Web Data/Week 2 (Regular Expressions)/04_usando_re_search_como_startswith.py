# Usando re.search() como se usaría el metodo startswith()

# Como se hacia antes?

handle = open("mbox-short.txt")
for line in handle:
	line = line.rstrip()
	if line.startswith("From: "):
		print(line)

print("--------------------------------------------------------")

# Como se hace con el metodo re.search()?

import re 

handle = open("mbox-short.txt")
for line in handle:
	line = line.rstrip()
	if re.search("^From:", line):
		print(line)