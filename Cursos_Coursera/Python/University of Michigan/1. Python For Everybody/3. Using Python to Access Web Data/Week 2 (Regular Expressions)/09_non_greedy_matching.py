# Not all regular expressions repeat codes are greedy
# If you add a "?" character, the + and * chill out a bit...

import re
x = "From: Using the : character"
y = re.findall("^F.+?:", x)
print(y)

# Explicando, tenemos:
	# ^F Primer caracter que hace match es "F"  
	# .+? Uno o mas caracteres PERO NOT GREEDY
	# : Ultimo caracter que hace match es ":" 

# Respuesta:
	# ['From:']

# En este caso SI tomó "From:"