# Usando re.search() como se usaría el metodo find()

# Como se hacia antes?

handle = open("mbox-short.txt")
for line in handle:
	line = line.rstrip()
	if line.find("From: ") >= 0:
		print(line)

print("--------------------------------------------------------")

# Como se hace con el metodo re.search()?

import re 

handle = open("mbox-short.txt")
for line in handle:
	line = line.rstrip()
	if re.search("From: ", line):
		print(line)



# Esto me va a mostrar todas las lineas que empiezan por "From:"