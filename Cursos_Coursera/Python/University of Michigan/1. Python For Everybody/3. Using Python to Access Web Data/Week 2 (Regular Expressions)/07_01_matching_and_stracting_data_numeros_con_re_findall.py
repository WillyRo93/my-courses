# Entonces, re.search() retorna un True/False dependiendo de
# si el string hace match con una regular expresion (?)

# Si buscamos extraer las strings que hacen match , usamos re.findall()

import re
x = "My 2 favorite numbers are 19 and 42"
y = re.findall("[0-9]+", x)
print(y)

# Explicándolo, seria esto:
	# [0-9]+ Significa uno o mas digitos

# ---------------------------------------------------------------

# Respuesta:
# ['2', '19', '42']