# Cuando usamos re.findall(), retorna una lista de cero o mas strings que hacen match
# con las expresiones regulares.

import re
x = "My 2 favorite numbers are 19 and 42"
y = re.findall ("[AEIOU]", x)
print (y)

# Respuesta:
	# []

# ---------------------------------------------------------------------------------

y = re.findall ("[aeiou]", x)
print (y)

# Respuesta:
	# ['a', 'o', 'i', 'e', 'u', 'e', 'a', 'e', 'a']