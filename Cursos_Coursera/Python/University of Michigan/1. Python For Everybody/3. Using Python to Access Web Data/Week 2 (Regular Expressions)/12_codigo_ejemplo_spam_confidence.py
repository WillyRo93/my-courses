import re

handle = open("mbox-short.txt")
numlist = list()

for line in handle:
	line = line.rstrip()
	stuff = re.findall("^X-DSPAM-Confidence: ([0-9.]+)", line)
	if len(stuff) != 1: # Es decir, que la linea solo contega un numero
		continue
	num = float(stuff[0])
	numlist.append(num)

print("El maximo es: ",max(numlist))

# Explicando:
	# ^X-DSPAM-Confidence:   Que empiece literalmente por "X-DSPAM-Confidence:"
	# ([0-9.])               A partir de aqui, empiezas a extraer numeros float desde 0.1 hasta 9.0 
	# +                      Una o mas veces

# Resultado:
	# El maximo es:  0.9907