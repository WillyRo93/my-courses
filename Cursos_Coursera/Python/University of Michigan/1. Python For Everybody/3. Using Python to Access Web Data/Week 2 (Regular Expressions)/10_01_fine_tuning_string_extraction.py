# Se puede refinar el match para re.findall() y separadamente determinar que porcion
# del match va a ser extraido, ¿Como se hace? Usando parentesis

import re

x = "From stephen.marquard@uct.ac.za Sat Jan 5 09:14:16 2008"

y = re.findall("\S+@\S+", x)
print(y)

# Explicando, tenemos:
	# \S Por lo menos un "no-whitespace character"

# Respuesta:
	# ['stephen.marquard@uct.ac.za']