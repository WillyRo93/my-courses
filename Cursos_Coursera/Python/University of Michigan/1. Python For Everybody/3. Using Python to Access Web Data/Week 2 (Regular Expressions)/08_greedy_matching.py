# The repeat characters (* and +) push outward in both directions (greedy)
# to match the largest possible string

import re
x = "From: Using the : character"
y = re.findall("^F.+:", x)
print(y)

# Explicando, tenemos:
	# ^F Primer caracter que hace match es "F"  
	# .+ Uno o mas caracteres
	# : Ultimo caracter que hace match es ":" 

# Respuesta:
	# ['From: Using the :']

# En este caso NO tomó "From:", sino que tomó mas caracteres

