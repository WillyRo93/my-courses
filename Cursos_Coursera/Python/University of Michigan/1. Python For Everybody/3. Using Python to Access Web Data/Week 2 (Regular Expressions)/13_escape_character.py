# If you want a special regular expression character to just behave normally (most of the time)
# you prefix it with "\"

import re

x = "We just received $10.00 for cookies."
y = re.findall("\$[0-9.]+", x)

print(y)

# -----------------------------------------------------------------

# Explicando:
	# \$       El backslash es un prefijo que me indica que voy a buscar el simbolo "$"
	# [0-9.]   Luego del simmbolo del dolar busco valores float, sin espacios de por medio luego del "$"
	# + 	   Por lo menos una o mas veces.

# Resultado:
	# ['$10.00']