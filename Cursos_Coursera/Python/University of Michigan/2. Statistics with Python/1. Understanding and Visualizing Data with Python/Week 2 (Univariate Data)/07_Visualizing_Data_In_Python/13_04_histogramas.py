# We first need to import the packages that we will be using
import seaborn as sns # For plotting
import matplotlib.pyplot as plt # For showing plots

# Load in the data set
tips_data = sns.load_dataset("tips")

# ---------------------------------------------------------------------------

					# Creating Histograms
# After we have a general 'feel' for the data, it is often good to get a
# feel for the shape of the distribution of the data.

# ---------------------------------------------------------------------------
# Plot a histogram of the total bill ----> "13_04_total_bill.png"
sns.histplot(tips_data["total_bill"], kde = False).set_title("Histogram of Total Bill")
# Si colocamos True en kde =, podemos tener un density plot
plt.show()

# ---------------------------------------------------------------------------
# Plot a histogram of the Tips only ----> "13_04_total_tip.png"
sns.histplot(tips_data["tip"], kde = False).set_title("Histogram of Total Tip")
plt.show()

# ---------------------------------------------------------------------------
# Plot a histogram of both the total bill and the tips' ----> "13_04_totals.png"
sns.histplot(tips_data["total_bill"], kde = False)
sns.histplot(tips_data["tip"], kde = False).set_title("Histogram of Both Tip Size and Total Bill")
plt.show()