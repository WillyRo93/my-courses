# We first need to import the packages that we will be using
import seaborn as sns # For plotting
import matplotlib.pyplot as plt # For showing plots

# Load in the data set
tips_data = sns.load_dataset("tips")

# ---------------------------------------------------------------------------

						# Creating a Boxplot
# Boxplots do not show the shape of the distribution, but they can give us a
# better idea about the center and spread of the distribution as well as any
# potential outliers that may exist. Boxplots and Histograms often complement
# each other and help an analyst get more information about the data

# ---------------------------------------------------------------------------
# Create a boxplot of the total bill amounts ----> "13_05_box_bill.png"
sns.boxplot(tips_data["total_bill"]).set_title("Box plot of the Total Bill")
plt.show()

# ---------------------------------------------------------------------------
# Create a boxplot of the tips amounts ----> "13_05_box_tips.png"
sns.boxplot(tips_data["tip"]).set_title("Box plot of the Tip")
plt.show()

# ---------------------------------------------------------------------------
# Create a boxplot of the tips and total bill amounts - do not do it like this
# ----> "13_05_box_totals.png"
sns.boxplot(tips_data["total_bill"])
sns.boxplot(tips_data["tip"]).set_title("Box plot of the Total Bill and Tips")
plt.show()

# ---------------------------------------------------------------------------
############## En todos los casos me dice lo siguiente: ##############

# Pass the following variable as a keyword arg: x. From version 0.12,
# the only valid positional argument will be `data`, and passing other arguments
# without an explicit keyword will result in an error or misinterpretation.
#  warnings.warn(