# We first need to import the packages that we will be using
import seaborn as sns # For plotting
import matplotlib.pyplot as plt # For showing plots

# Load in the data set
tips_data = sns.load_dataset("tips")

# ---------------------------------------------------------------------------

		# Creating Histograms and Boxplots Plotted by Groups
# While looking at a single variable is interesting, it is often useful to see
# how a variable changes in response to another. Using graphs, we can see
# if there is a difference between the tipping amounts of smokers vs. non-smokers,
# if tipping varies according to the time of the day,
# or we can explore other trends in the data as well.

# ---------------------------------------------------------------------------
# Create a boxplot and histogram of the tips grouped by smoking status
# ----> "13_06_bxplts_smoker.png"
sns.boxplot(x = tips_data["tip"], y = tips_data["smoker"])
plt.show()

# ---------------------------------------------------------------------------
# Create a boxplot and histogram of the tips grouped by time of day
# ----> "13_06_bxplts_tips.png"
sns.boxplot(x = tips_data["tip"], y = tips_data["time"])

g = sns.FacetGrid(tips_data, row = "time")
g = g.map(plt.hist, "tip")
plt.show()

# ---------------------------------------------------------------------------
# Create a boxplot and histogram of the tips grouped by the day
# ----> "13_06_bxplts_tipsday.png"
sns.boxplot(x = tips_data["tip"], y = tips_data["day"])

g = sns.FacetGrid(tips_data, row = "day")
g = g.map(plt.hist, "tip")
plt.show()