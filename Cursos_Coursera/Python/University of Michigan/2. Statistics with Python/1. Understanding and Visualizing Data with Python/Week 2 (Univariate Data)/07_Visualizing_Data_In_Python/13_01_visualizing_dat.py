# Tables, Histograms, Boxplots, and Slicing for Statistics

# When working with a new dataset, one of the most useful things to do is to begin to visualize the data.
# By using tables, histograms, box plots, and other visual tools, we can get
# a better idea of what the data may be trying to tell us, and we can gain
# insights into the data that we may have not discovered otherwise.

# Today, we will be going over how to perform some basic visualisations in Python,
# and, most importantly, we will learn how to begin exploring data from a graphical perspective.

#------------------------------------------------------------
# We first need to import the packages that we will be using
import seaborn as sns # For plotting
import matplotlib.pyplot as plt # For showing plots

# Load in the data set
tips_data = sns.load_dataset("tips")