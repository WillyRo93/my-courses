# We first need to import the packages that we will be using
import seaborn as sns # For plotting
import matplotlib.pyplot as plt # For showing plots

# Load in the data set
tips_data = sns.load_dataset("tips")

# ---------------------------------------------------------------------------

				# Visualizing the Data - Tables
# When you begin working with a new data set, it is often best to print
# out the first few rows before you begin other analysis. This will show
# you what kind of data is in the dataset, what data types you are working with,
# and will serve as a reference for the other plots that we are about to make.

# Print out the first few rows of the data
tips_data.head() # En Jupyter con esto lo imprime
print(tips_data.head())

								# Resultado

			#    total_bill   tip     sex smoker  day    time  size
			# 0       16.99  1.01  Female     No  Sun  Dinner     2
			# 1       10.34  1.66    Male     No  Sun  Dinner     3
			# 2       21.01  3.50    Male     No  Sun  Dinner     3
			# 3       23.68  3.31    Male     No  Sun  Dinner     2
			# 4       24.59  3.61  Female     No  Sun  Dinner     4

