# We first need to import the packages that we will be using
import seaborn as sns # For plotting
import matplotlib.pyplot as plt # For showing plots

# Load in the data set
tips_data = sns.load_dataset("tips")

# ------------------------------------------------------------------

					# Describing Data. 

# Summary statistics, which include things like the mean, min,
# and max of the data, can be useful to get a feel for how large some
# of the variables are and what variables may be the most important.

# Print out the summary statistics for the quantitative variables
tips_data.describe()
print(tips_data.describe())

							# Resultado:

			#        total_bill         tip        size
			# count  244.000000  244.000000  244.000000
			# mean    19.785943    2.998279    2.569672
			# std      8.902412    1.383638    0.951100
			# min      3.070000    1.000000    1.000000
			# 25%     13.347500    2.000000    2.000000
			# 50%     17.795000    2.900000    2.000000
			# 75%     24.127500    3.562500    3.000000
			# max     50.810000   10.000000    6.000000