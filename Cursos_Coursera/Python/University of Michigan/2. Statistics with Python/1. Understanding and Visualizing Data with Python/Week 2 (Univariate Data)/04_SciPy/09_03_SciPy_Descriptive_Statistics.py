			# SciPy Descriptive Statictics.
					
from scipy import stats
import numpy as np

# ---------------------------------------------------
np.random.seed(282629734)

# Con esto, generamos --> 1000 Student’s T continuous random variables.
x = stats.t.rvs(10, size=1000)

# Realizamos una descriptive statistics
print(x.min())   # equivalente a np.min(x)
	# -3.7897557242248197

print(x.max())   # equivalente a np.max(x)
	# 5.263277329807165

print(x.mean())  # equivalente a np.mean(x)
	# 0.014061066398468422

print(x.var())   # equivalente a np.var(x))
	# 1.288993862079285

# Describimos el resultado:
stats.describe(x)
	# DescribeResult (nobs=1000, 
					# minmax=(-3.7897557242248197, 5.263277329807165),
					# mean=0.014061066398468422,
					# variance=1.2902841462255106,
					# skewness=0.21652778283120955,
					# kurtosis=1.055594041706331)