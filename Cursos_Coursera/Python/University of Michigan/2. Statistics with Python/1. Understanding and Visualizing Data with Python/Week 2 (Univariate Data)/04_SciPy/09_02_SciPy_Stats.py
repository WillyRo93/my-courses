					# SciPy Stats.
					
from scipy import stats
import numpy as np

# ---------------------------------------------------
### Imprimiendo Variables al Azar
print(stats.norm.rvs(size = 10))
	# [ 1.32575613 -0.1511631   0.1342587   0.69181522  0.22727344  0.56171048  0.38019245  0.56264841 -0.01274742  0.18809171]

# ---------------------------------------------------
from pylab import *

# Aqui creamos data para hacer tests
dx = .01
X  = np.arange(-2,2,dx)
Y  = exp(-X**2)

# Normalizamos la data para un proper PDF
Y /= (dx*Y).sum()

# Computamos el CDF
CY = np.cumsum(Y*dx)

# Graficamos ambos casos
plot(X,Y)
plot(X,CY,'r--')

show()

### Compute the Normal CDF of certain values.
print(stats.norm.cdf(np.array([1,-1., 0, 1, 3, 4, -2, 6])))
	# [0.84134475 0.15865525 0.5        0.84134475 0.9986501  0.99996833 0.02275013 1.        ]