Como se puede observar en el archivo "05_02_boxplots.png", tenemos una nueva forma de representar valores de manera grafica.

- El cuadrado representa el IQR, o la data entre el 1er y 3er Cuarto de la data.

- La media se presenta en el cuadro principal de la grafica.

- Los limites superiores e inferiores representan los maximos y los minimos de la data presentada. Se alejan de la data mediante unas lineas segmentadas o punteadas denominadas generalmente whiskers.

EJEMPLOS:
	Ejemplo 1:
		- En el archivo "05_03_ejemplo1_boxplots.png" podemos ver la data y su respectiva grafica.

	Ejemplo 2:
		- En el archivo "05_03_ejemplo2_boxplots.png" podemos ver la data y su respectiva grafica. Aqui podemos identificar como nos puede ayar los boxplots a visualizar los puntos inusuales o outliers.

	Ejemplo 3:
		- En el archivo "05_03_ejemplo3_boxplots.png" podemos ver la data y su respectiva grafica. Podemos observar como el boxplot esconde la forma de la grafica, al no mostrar los valores intermedios que no existen. Esto es algo a tomar en cuenta.

	QUIZ:
		- Nos preguntan que podemos observar acá. Archivo "05_03_quiz_boxplots.png"
		- Lo que se observa es que a medida que se toman las medidas de presion sanguinea a la gente mayor, aumenta la presion considerablemente con los años. Tambien podemos ver que mientras mayores son las personas, mayor dispersion existe en la data. Los hombres jovenes tienen mayor presion sanguinea pero al aumentar la edad, son las mujeres las que tienen mayor presion sanguinea. Esto puede deberse a que los hombres con problemas de hipertension, suelen morir antes, y esto hace que la muestra restante para hacer las pruebas sean generalmente hombres mas sanos.