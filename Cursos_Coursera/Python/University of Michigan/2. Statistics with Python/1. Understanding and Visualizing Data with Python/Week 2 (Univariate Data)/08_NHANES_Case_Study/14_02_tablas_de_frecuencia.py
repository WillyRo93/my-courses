import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np

# Next we will load the NHANES data from a file.

da = pd.read_csv("nhanes_2015_2016.csv")

# -------------------------------------------------------
						#Frequency tables
#The value_counts method can be used to determine the number of times
#that each distinct value of a variable occurs in a data set. In statistical
#terms, this is the "frequency distribution" of the variable.

	# Below we show the frequency distribution of the DMDEDUC2 variable,
	# which is a variable that reflects a person's level of educational attainment.
	# The value_counts method produces a table with two columns. The first column
	# contains all distinct observed values for the variable. 
	# The second column contains the number of times each of these values occurs.
	# Note that the table returned by value_counts is actually a Pandas data frame,
	# so can be further processed using any Pandas methods for working with data frames.

	# The numbers 1, 2, 3, 4, 5, 9 seen below are integer codes for the 6 possible
	# non-missing values of the DMDEDUC2 variable. The meaning of these codes is given in
	# the NHANES codebook located here, and will be discussed further below. This table shows,
	# for example, that 1621 people in the data file have DMDEDUC=4, which indicates that the
	# person has completed some college, but has not graduated with a four-year degree.

print(da.DMDEDUC2.value_counts())
		# Resultado:
		# 4.0    1621
		# 5.0    1366
		# 3.0    1186
		# 1.0     655
		# 2.0     643
		# 9.0       3
		# Name: DMDEDUC2, dtype: int64

		# En Jupyter solo hace falta colocar:
			#da.DMDEDUC2.value_counts()

# -------------------------------------------------------
	# Note that the value_counts method excludes missing values. We confirm this
	# below by adding up the number of observations with a DMDEDUC2 value equal
	# to 1, 2, 3, 4, 5, or 9 (there are 5474 such rows), and comparing this to
	# the total number of rows in the data set, which is 5735.
	# This tells us that there are 5735 - 5474 = 261 missing values for this
	# variable (other variables may have different numbers of missing values).
print(da.DMDEDUC2.value_counts().sum())
print(1621 + 1366 + 1186 + 655 + 643 + 3) # Manually sum the frequencies
print(da.shape)

		# Resultado:
		# 5474
		# 5474
		# (5735, 28)

# -------------------------------------------------------
	# Another way to obtain this result is to locate all the null (missing)
	# values in the data set using the isnull Pandas function, and count the number of such locations.
print(pd.isnull(da.DMDEDUC2).sum())
	#261

# -------------------------------------------------------
	# In some cases it is useful to replace integer codes with a text label that reflects the
	# code's meaning. Below we create a new variable called 'DMDEDUC2x' that is recoded with
	# text labels, then we generate its frequency distribution.
da["DMDEDUC2x"] = da.DMDEDUC2.replace({1: "<9", 2: "9-11", 3: "HS/GED", 4: "Some college/AA",
										5: "College", 7: "Refused", 9: "Don't know"})
print(da.DMDEDUC2x.value_counts())

							# Resultado:
						# Some college/AA    1621
						# College            1366
						# HS/GED             1186
						# <9                  655
						# 9-11                643
						# Don't know            3
						# Name: DMDEDUC2x, dtype: int64

# -------------------------------------------------------
	# We will also want to have a relabeled version of the gender variable,
	# so we will construct that now as well. We will follow a convention here of appending
	# an 'x' to the end of a categorical variable's name when it has been recoded
	# from numeric to string (text) values.
da["RIAGENDRx"] = da.RIAGENDR.replace({1: "Male", 2: "Female"})

# -------------------------------------------------------
	# For many purposes it is more relevant to consider the proportion of the sample with
	# each of the possible category values, rather than the number of people in each category.
	# We can do this as follows:
x = da.DMDEDUC2x.value_counts()  # x is just a name to hold this value temporarily
x = x / x.sum()
print(x)
						# Resultado:
				# Some college/AA    0.296127
				# College            0.249543
				# HS/GED             0.216661
				# <9                 0.119657
				# 9-11               0.117464
				# Don't know         0.000548
				# Name: DMDEDUC2x, dtype: float64

# -------------------------------------------------------
	# In some cases we will want to treat the missing response category as another
	# category of observed response, rather than ignoring it when creating summaries.
	# Below we create a new category called "Missing", and assign all missing values to it usig fillna.
	# Then we recalculate the frequency distribution. We see that 4.6% of the responses are missing.
da["DMDEDUC2x"] = da.DMDEDUC2x.fillna("Missing")
x = da.DMDEDUC2x.value_counts()
x = x / x.sum()
print(x)
							# Resultado:
					# Some college/AA    0.282650
					# College            0.238187
					# HS/GED             0.206800
					# <9                 0.114211
					# 9-11               0.112119
					# Missing            0.045510
					# Don't know         0.000523
					# Name: DMDEDUC2x, dtype: float64