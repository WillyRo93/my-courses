				# Univariate data analyses - NHANES case study

# Here we will demonstrate how to use Python and Pandas to perform some basic analyses with univariate data,
# using the 2015-2016 wave of the NHANES study to illustrate the techniques.

# The following import statements make the libraries that we will need available.
# Note that in a Jupyter notebook, you should generally use the %matplotlib inline directive,
# which would not be used when running a script outside of the Jupyter environment.

#%matplotlib inline
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np

# Next we will load the NHANES data from a file.

da = pd.read_csv("nhanes_2015_2016.csv")