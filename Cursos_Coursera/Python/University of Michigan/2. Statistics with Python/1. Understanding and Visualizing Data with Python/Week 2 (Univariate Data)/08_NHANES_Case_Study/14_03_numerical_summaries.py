import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np

# Next we will load the NHANES data from a file.

da = pd.read_csv("nhanes_2015_2016.csv")

# -------------------------------------------------------
				# Numerical summaries
# A quick way to get a set of numerical summaries for a quantitative
# variable is with the describe data frame method.

	# Below we demonstrate how to do this using the body weight variable (BMXWT).
	# As with many surveys, some data values are missing,
	# so we explicitly drop the missing cases using the dropna
	# method before generating the summaries.
print(da.BMXWT.dropna().describe())
			# Resultado:
		# count    5666.000000
		# mean       81.342676
		# std        21.764409
		# min        32.400000
		# 25%        65.900000
		# 50%        78.200000
		# 75%        92.700000
		# max       198.900000
		# Name: BMXWT, dtype: float64

# -------------------------------------------------------
	# It's also possible to calculate individual summary statistics
	# from one column of a data set.
	# This can be done using Pandas methods, or with numpy functions:
x = da.BMXWT.dropna()  # Extract all non-missing values of BMXWT into a variable called 'x'
print(x.mean()) # Pandas method
print(np.mean(x)) # Numpy function

print(x.median())
print(np.percentile(x, 50))  # 50th percentile, same as the median
print(np.percentile(x, 75))  # 75th percentile
print(x.quantile(0.75)) # Pandas method for quantiles, equivalent to 75th percentile

			# Resultado:
		# 81.34267560889516
		# 81.34267560889516
		# 78.2
		# 78.2
		# 92.7
		# 92.7

# -------------------------------------------------------
	# Next we look at frequencies for a systolic blood pressure measurement (BPXSY1).
	# "BPX" here is the NHANES prefix for blood pressure measurements,
	# "SY" stands for "systolic" blood pressure (blood pressure at the peak of a heartbeat cycle),
	# and "1" indicates that this is the first of three systolic blood presure
	# measurements taken on a subject.

	# A person is generally considered to have pre-hypertension when their systolic
	# blood pressure is between 120 and 139, or their diastolic blood pressure is between
	# 80 and 89. Considering only the systolic condition, we can calculate the proprotion
	# of the NHANES sample who would be considered to have pre-hypertension.
print(np.mean((da.BPXSY1 >= 120) & (da.BPXSY2 <= 139)))  # "&" means "and"
		# Resultado:
	# 0.3741935483870968

# -------------------------------------------------------
	# Next we calculate the propotion of NHANES subjects who are
	# pre-hypertensive based on diastolic blood pressure.
np.mean((da.BPXDI1 >= 80) & (da.BPXDI2 <= 89))
		# Resultado:
	# 0.14803836094158676

# -------------------------------------------------------
	# Finally we calculate the proportion of NHANES subjects who are pre-hypertensive
	# based on either systolic or diastolic blood pressure. Since some people are
	# pre-hypertensive under both criteria, the proportion below is less than the
	# sum of the two proportions calculated above.

	# Since the combined systolic and diastolic condition for pre-hypertension
	# is somewhat complex, below we construct temporary variables
	# 'a' and 'b' that hold the systolic and diastolic pre-hypertensive
	# status separately, then combine them with a "logical or" to obtain the final
	# status for each subject.
a = (da.BPXSY1 >= 120) & (da.BPXSY2 <= 139)
b = (da.BPXDI1 >= 80) & (da.BPXDI2 <= 89)
print(np.mean(a | b))  # "|" means "or"
			# Resultado:
		# 0.43975588491717527

# -------------------------------------------------------
	# Blood pressure measurements are affected by a phenomenon called "white coat anxiety",
	# in which a subject's bood pressure may be slightly elevated if they are nervous
	# when interacting with health care providers

	# Typically this effect subsides if the blood pressure is measured several times in sequence.
	# In NHANES, both systolic and diastolic blood pressure are meausred three times
	# for each subject (e.g. BPXSY2 is the second measurement of systolic blood pressure).

	# We can calculate the extent to which white coat anxiety is present in the NHANES data
	# by looking a the mean difference between the first two systolic or diastolic blood
	# pressure measurements.

print(np.mean(da.BPXSY1 - da.BPXSY2))
print(np.mean(da.BPXDI1 - da.BPXDI2))
			# Resultado:
		# 0.6749860309182343
		# 0.3490407897187558