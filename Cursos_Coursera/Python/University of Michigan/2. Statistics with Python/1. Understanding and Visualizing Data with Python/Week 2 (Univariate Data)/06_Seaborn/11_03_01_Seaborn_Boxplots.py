			# Seaborn BoxPlots.

# Import necessary libraries
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd

############ Aqui hacemos lo mismo que antes #############
														 #
# Store the url string that hosts our .csv file          #
url = "Cartwheeldata.csv"                                #
														 #
# Read the .csv file and store it as a pandas Data Frame #
df = pd.read_csv(url)									 #
														 #
##########################################################

# Desde acá empiezan los boxplots:

# ---------------------------------------------------------------
sns.boxplot(data=df.loc[:, ["Age", "Height", "Wingspan", "CWDistance", "Score"]])

# Con esto mostramos el boxplot "11_03_02_BoxPlots.png"
plt.show()

# ---------------------------------------------------------------
# Male Boxplot
sns.boxplot(data=df.loc[df['Gender'] == 'M', ["Age", "Height", "Wingspan", "CWDistance", "Score"]])

# Con esto mostramos el boxplot "11_03_03_BoxPlots.png"
plt.show()

# ---------------------------------------------------------------
# Female Boxplot
sns.boxplot(data=df.loc[df['Gender'] == 'F', ["Age", "Height", "Wingspan", "CWDistance", "Score"]])

# Con esto mostramos el boxplot "11_03_04_BoxPlots.png"
plt.show()

# ---------------------------------------------------------------
# Male Boxplot
sns.boxplot(data=df.loc[df['Gender'] == 'M', ["Score"]])

# Con esto mostramos el boxplot "11_03_05_BoxPlots.png"
plt.show()

# ---------------------------------------------------------------
# Female Boxplot
sns.boxplot(data=df.loc[df['Gender'] == 'F', ["Score"]])

# Con esto mostramos el boxplot "11_03_06_BoxPlots.png"
plt.show()