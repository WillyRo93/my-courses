			# Seaborn ScatterPlots.

# Import necessary libraries
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd

# Store the url string that hosts our .csv file
url = "Cartwheeldata.csv"

# Read the .csv file and store it as a pandas Data Frame
df = pd.read_csv(url)

# Create Scatterplot
sns.lmplot(x='Wingspan', y='CWDistance', data=df)

# Con esto mostramos la grafica "11_02_02_Scatterplots.png"
plt.show()

# -----------------------------------------------
# Scatterplot arguments
sns.lmplot(x='Wingspan', y='CWDistance', data=df,
           fit_reg=False, # No regression line
           hue='Gender')   # Color by evolution stage

# Con esto mostramos la grafica "11_02_03_Scatterplots.png"
plt.show()

# -----------------------------------------------
# Construct Cartwheel distance plot
sns.swarmplot(x="Gender", y="CWDistance", data=df)

# Con esto mostramos la grafica "11_02_04_Scatterplots.png"
plt.show()