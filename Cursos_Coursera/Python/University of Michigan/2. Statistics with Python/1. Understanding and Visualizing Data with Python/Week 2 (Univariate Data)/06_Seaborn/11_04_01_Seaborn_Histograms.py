			# Seaborn Histograms.

# Import necessary libraries
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd

############ Aqui hacemos lo mismo que antes #############
														 #
# Store the url string that hosts our .csv file          #
url = "Cartwheeldata.csv"                                #
														 #
# Read the .csv file and store it as a pandas Data Frame #
df = pd.read_csv(url)									 #
														 #
##########################################################

# Desde acá empiezan los histogramas:

# ---------------------------------------------------------------
# Distribution Plot (a.k.a. Histogram)
sns.distplot(df.CWDistance)

# Con esto mostramos el boxplot "11_04_02_Histograms.png"
plt.show()

# WARNING:
	# FutureWarning:
	# `distplot` is a deprecated function and will be removed in a future version.
	# Please adapt your code to use either
		# ° `displot` (a figure-level function with similar flexibility)
		# ° Or `histplot` (an axes-level function for histograms).

# ---------------------------------------------------------------
