			# Seaborn Count Plots.

# Import necessary libraries
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd

############ Aqui hacemos lo mismo que antes #############
														 #
# Store the url string that hosts our .csv file          #
url = "Cartwheeldata.csv"                                #
														 #
# Read the .csv file and store it as a pandas Data Frame #
df = pd.read_csv(url)									 #
														 #
##########################################################

# Desde acá empiezan los Count Plots:

# ---------------------------------------------------------------
# Count Plot (a.k.a. Bar Plot)
sns.countplot(x='Gender', data=df)
 
plt.xticks(rotation=-45)

# Con esto mostramos el boxplot "12_01_02_Count_Plots.png"
plt.show()