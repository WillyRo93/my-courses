					# NumPy Datatypes in Arrays.

import numpy as np

# ---------------------------------------------------
### Integer.
j = np.array([1, 2])
print(j.dtype)  
	# int64

### Float.
k = np.array([1.0, 2.0])
print(k.dtype)         
	# float64

### Forzando el tipo de data que queremos.
l = np.array([1.0, 2.0], dtype=np.int64)
print(l.dtype)
	# int64