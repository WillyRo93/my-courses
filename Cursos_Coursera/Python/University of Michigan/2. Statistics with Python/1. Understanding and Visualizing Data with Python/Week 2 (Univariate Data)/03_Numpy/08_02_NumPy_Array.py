					# NumPy Array.
					
import numpy as np

# ---------------------------------------------------
### Creamos un arreglo 3x1
a = np.array([1,2,3])

### Imprimimos el tipo de object
print(type(a))
	# <class 'numpy.ndarray'>

### Imprimimos la forma o shape
print(a.shape)
	# (3,)

### Imprimimos los valores de a
print(a[0], a[1], a[2])
	# 1 2 3

# ---------------------------------------------------
### Ahora creamos un segundo arreglo 2x2
b = np.array([[1,2],[3,4]])
	
print(b.shape)
	# (2, 2)

print(b[0,0], b[0,1], b[1,1])
	# 1 2 4

# ---------------------------------------------------
### Luego hacemos un arreglo 3x2:
c = np.array([[1,2],[3,4],[5,6]])
	
print(c.shape)
	# (3, 2)

print(c[0,1], c[1,0], c[2,0], c[2,1])
	# 2 3 5 6

# ---------------------------------------------------
### Creamos un arreglo 2x3 de ceros: 
d = np.zeros((2,3))
	
print(d)
	# [[0. 0. 0.] [0. 0. 0.]]

# ---------------------------------------------------
### Arreglo 4x2 de unos:
e = np.ones((4,2))

print(e)
	# [[1. 1.] [1. 1.] [1. 1.] [1. 1.]]

# ---------------------------------------------------
### 2x2 constant array
f = np.full((2,2), 9)
	
print(f)
	# [[9 9] [9 9]]

# ---------------------------------------------------
### 3x3 random array
g = np.random.random((3,3))

print(g)
	# [[0.98887378 0.06276123 0.19956888]
	#  [0.81528075 0.51330995 0.40639745]
	#  [0.02140286 0.97775396 0.30704391]]
# ---------------------------------------------------