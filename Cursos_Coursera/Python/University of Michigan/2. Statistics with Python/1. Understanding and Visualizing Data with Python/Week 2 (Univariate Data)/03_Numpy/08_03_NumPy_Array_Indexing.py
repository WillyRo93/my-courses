					# NumPy Array Indexing.

import numpy as np

# ---------------------------------------------------
### Creamos un arreglo 3x4
h = np.array([[1,2,3,4,], [5,6,7,8], [9,10,11,12]])

print(h)
	# [[ 1  2  3  4]
	#  [ 5  6  7  8]
	#  [ 9 10 11 12]]

### Le hacemos un slice a nuestro arreglo para tener un subarreglo
### Este nuevo arreglo será de 2x2
i = h[:2, 1:3]

print(i)
	# [[2 3] [6 7]]

# ---------------------------------------------------

# Imprimo mi valor (0,1)
print(h[0,1])
	# 2

### Modifico el slice, cambio el valor de (0,0)
i[0,0] = 1738

### Imprimimos para ver como modificando el slice, tambien
### modificamos el objeto base.
print(h[0,1])
	# 1738