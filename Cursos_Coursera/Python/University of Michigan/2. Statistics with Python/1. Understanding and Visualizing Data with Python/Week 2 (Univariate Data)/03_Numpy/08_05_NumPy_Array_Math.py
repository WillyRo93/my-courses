# Basic mathematical functions operate elementwise on arrays, and
# are available both as operator overloads and as functions in the numpy module:

					# NumPy Array Math.

import numpy as np

# ---------------------------------------------------
x = np.array([[1,2],[3,4]], dtype=np.float64)
y = np.array([[5,6],[7,8]], dtype=np.float64)

# Suma de elementos.
print(x + y)
print(np.add(x, y))
	# Ambos producen el mismo resultado:
	# [[ 6.0  8.0] [10.0 12.0]]

# Diferencia o Resta de Elementos.
print(x - y)
print(np.subtract(x, y))
	# Ambos producen el mismo resultado:
	# [[-4.0 -4.0] [-4.0 -4.0]]

# Multiplicacion de Elementos.
print(x * y)
print(np.multiply(x, y))
	# Ambos producen el mismo resultado:
	# [[ 5.0 12.0] [21.0 32.0]]

# Division de Elementos.
print(x / y)
print(np.divide(x, y))
	# Ambos producen el mismo resultado:
	# [[ 0.2         0.33333333]  [ 0.42857143  0.5       ]]

# Elevando al cuadrado.
print(np.sqrt(x))
	# [[ 1.          1.41421356]  [ 1.73205081  2.        ]]

# ---------------------------------------------------
# Volvemos a armar un array denominado x
x = np.array([[1,2],[3,4]])

### Este proceso computa la suma de todos los elementos, imprime"10"
print(np.sum(x))

### Este proceso computa la suma de cada columna, imprime "[4 6]"
print(np.sum(x, axis=0)) 

### Este proceso computa la suma de cada fila, imprime "[3 7]"
print(np.sum(x, axis=1))

# ---------------------------------------------------
# De nuevo, volvemos a armar un array denominado x
x = np.array([[1,2],[3,4]])

### Para sacar el promedio de todos los elementos, imprime "2.5"
print(np.mean(x))

### Para sacar el promedio de cada columna, imprime "[2 3]"
print(np.mean(x, axis=0)) 

### Para sacar el promedio de cada fila, imprime "[1.5 3.5]"
print(np.mean(x, axis=1))