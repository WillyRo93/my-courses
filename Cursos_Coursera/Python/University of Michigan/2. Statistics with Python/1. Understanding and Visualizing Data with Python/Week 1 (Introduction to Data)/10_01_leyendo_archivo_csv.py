import numpy as np
import pandas as pd

#-------------------------------------------------------------
				# Aqui importamos la data:

# En la variable url guardamos los datos de nuestro .csv
url = "Cartwheeldata.csv"

# Leemos el .csv y lo guardamos en el Data Frame de Pandas
df = pd.read_csv(url)

# Imprimimos el tipo de Salida que tenemos:
print(type(df))
	# <class 'pandas.core.frame.DataFrame'>

#-------------------------------------------------------------
				# Luego, visualizamos la data:

# Podemos visualizar la data llamando a la funcion head(), funciona en Jupyter
	# Solo muestra los 5 primeros datos.
df.head()

# Para mostrar toda la data, usamos "df"
df

# Para mostrarnos las columnas o titulos del header, usamos "df.columns"
df.columns

# Podemos mostrar el tipo de data que tenemos, el print no hace falta
print(df.dtypes)
	# Resultado:
	#ID                 int64
	#Age                int64
	#Gender            object
	#GenderGroup        int64
	#Glasses           object
	#GlassesGroup       int64
	#Height           float64
	#Wingspan         float64
	#CWDistance         int64
	#Complete          object
	#CompleteGroup      int64
	#Score              int64
	#dtype: object

# Si quiero hacer un listado de los diferentes valores que están contenidos en la columna df["Gender"]
df.Gender.unique()
	# Resultado
	# array(['F', 'M'], dtype=object)

# Si queremos ver, por ejemplo, cuantos niños y niñas hay en mi data, podemos usar las funciones groupby() y size():
df.groupby(['Gender','GenderGroup']).size()
		# Resultado:
		#Gender  GenderGroup
		#F       1              12
		#M       2              13
		#dtype: int64

#-------------------------------------------------------------
# Supongamos que queremos dividir la data y seleccionar porciones especificas de la data
# Para ello hay 3 maneras de hacerlo:

	#.loc()
	#.iloc()
	#.ix()