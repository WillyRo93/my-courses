import numpy as np
import pandas as pd

# Importamos el csv
url = "Cartwheeldata.csv"
df = pd.read_csv(url)

# Imprimimos las columnas o headers para saber con que estamos trabajando
df.columns
# Index([u'ID', u'Age', u'Gender', u'GenderGroup', u'Glasses', u'GlassesGroup', u'Height', u'Wingspan',
       # u'CWDistance', u'Complete', u'CompleteGroup', u'Score'], 
       # dtype='object')

# -------------------------------------------------------------------------
# iloc() divide en base a los integer, iloc() usa labels/column names.
# A continuacion unos ejemplos:

# Muestrame los datos de las primeras 4 filas
df.iloc[:4]
	# Resultado
	#	ID	Age		Gender	GenderGroup	Glasses	GlassesGroup	Height	Wingspan	CWDistance	Complete	CompleteGroup	Score
	#0	1	56		F		1			Y		1				62.0	61.0		79			Y			1				7
	#1	2	26		F		1			Y		1				62.0	60.0		70			Y			1				8
	#2	3	33		F		1			Y		1				66.0	64.0		85			Y			1				7
	#3	4	39		F		1			N		0				64.0	63.0		87			Y			1				10

# Muestrame los datos de las filas [1:5] y muestrame el rango de columnas [2:4]
df.iloc[1:5, 2:4]
	# Resultado
	#	Gender	GenderGroup
	#1	F		1
	#2	F		1
	#3	F		1
	#4	M		2

# No puede hacer un slicing  con los nombres de las columnas, deben ser integers
df.iloc[1:5, ["Gender", "GenderGroup"]]