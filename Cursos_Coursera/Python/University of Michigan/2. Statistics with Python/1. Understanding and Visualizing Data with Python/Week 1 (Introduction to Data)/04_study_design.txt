Areas donde se pueden desarrollar estudios:

- Clinical trials for drugs and other medical treatments.
- Reliability and quality-assurance studies for manufactured products.
- Observational studies for human health.
- Pubic opinion and other surveys.
- Studies involving administrative and other ncidental data.
- Market research studies.
- Agricultural field trials.

# -----------------------------------------------------------------------
Tipos de Estudios:
- Exploratory Vs. Confirmatory studies.
	Confirmatory:
		Metodo cientifico - especificar una hipotesis que puede o no fallar y probarla --> Recopilar datos para abordar una sola pregunta preespecificado.

	Exploratory:
		Recopilar y analizar data sin una pregunta preespecificada.


- Comparative Vs. Non-Comparative Studies.
	Comparative:
		Finalidad: Contrastar una cantidad con respecto a otra.

		EJEMPLOS DE COMPARATIVE:
		- Podemos comparar el rendimiento de naranjas en parcelas de tierra que se tratan con diferentes fertilizantes.
		- Podemos comparar la preferencia de los votantes por uno de los candidatos a la alcaldia con respecto a otros.
		- Podemos comparar los rates en los cuales los viewers vana  hacer click en uno de dos versiones de comerciales en una propaganda online. 

	Non-Comparative
		Finalidad: Estimar o predecir cantidades absolutas - no (explicitamente) comparativas.

		EJEMPLOS DE NON-COMPARATIVE:
		- Podemos predecir el valor de una compania en la bolsa en un año.
		- Podemos estimar la reduccion absoluta de presion sanguinea cuando se sigue un tratamiento con cierta medicina en especifico.


- Observational studies Vs. Experiments.
	Observational:
		Surgen "naturalmente", contrastes basados en la "autoselección" de unidades en grupos.

		EJEMPLOS DE OBSERVATIONAL:
		- Identificar los efectos en la salud de fumar, para esto podemos comparar la esperanza de vida entre personas fumadoras y no fumadoras. O quizas otra variable como cancer de pulmon entre fumadores y no fumadores. 
		- Identificar el impacto que la experiencia de un profesor tiene en el aprendizaje de un estudiante, lo mejora? lo facilita? Como se compara? Se puede comparar con notas de examenes estandarizados entre estudiantes que estan con profesores muy experimentados Versus alumnos que no están con profesores tan experimentados.

		NOTA: En las observaciones, mas que ser asignados a algo en especifico, los sujetos se ven expuestos a una condicion. (De manera pasiva o electa por ellos mismos, esto es aplicado cuando es impractico y/o su asignacion es poco etica)		

	Experiments:
		Involucra manipulación o asignación --> El experimentador trata deliberadamente diferentes unidades de diferentes maneras.

		EJEMPLOS DE EXPERIMENTS:
		- Comparar rendimientos de lechugas que han sido y que no han sido tratadas con fertilizantes. Como se hace en este caso? Se pueden tomar los campos dividirlos en parcelas, de manera aleatoria asignar ciertas parcelas para ser tratadas con fertilizantes y otras sin fertilizantes.
		- Evaluar si es más probable que las personas hagan clic en una de las dos versiones de un anuncio en línea, podemos exponer a las personas al azar a una versión u otra y luego comparar esas tasas de clics.

		NOTA: En los experimentos, generalmente los sujetos se ven expuestos a asignaciones al azar.
