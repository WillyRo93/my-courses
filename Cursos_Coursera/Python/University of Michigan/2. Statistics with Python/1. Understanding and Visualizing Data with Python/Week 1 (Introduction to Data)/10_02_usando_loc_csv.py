import numpy as np
import pandas as pd

# Importamos el csv
url = "Cartwheeldata.csv"
df = pd.read_csv(url)

# Imprimimos las columnas o headers para saber con que estamos trabajando
df.columns
# Index([u'ID', u'Age', u'Gender', u'GenderGroup', u'Glasses', u'GlassesGroup', u'Height', u'Wingspan',
       # u'CWDistance', u'Complete', u'CompleteGroup', u'Score'], 
       # dtype='object')
       
# -------------------------------------------------------------------------
# .loc() toma dos single/list/range operadores separados por una ','.
# Mi primer valor de entrada indica las filas y la segunda, las columnas.

# Retorna toda las observaciones que existen en "CWDistance"
df.loc[:,"CWDistance"]
              # Resultado:
              #0      79
              #1      70
              #2      85
              #3      87
              #4      72
              #5      81
              #6     107
              #7      98
              #8     106
              #9      65
              #10     96
              #11     79
              #12     92
              #13     66
              #14     72
              #15    115
              #16     90
              #17     74
              #18     64
              #19     85
              #20     66
              #21    101
              #22     82
              #23     63
              #24     67
              #Name: CWDistance, dtype: int64

# Seleccionar todas las filas para multiples columnas, en este caso: CWDistance, Height, Wingspan
df.loc[:,["CWDistance", "Height", "Wingspan"]]
              # Resultado:
              #       CWDistance    Height        Wingspan
              #0      79            62.00         61.0
              #1      70            62.00         60.0
              #2      85            66.00         64.0
              #3      87            64.00         63.0
              #4      72            73.00         75.0
              #5      81            75.00         71.0
              #6      107           75.00         76.0
              #7      98            65.00         62.0
              #8      106           74.00         73.0
              #9      65            63.00         60.0
              #10     96            69.50         66.0
              #11     79            62.75         58.0
              #12     92            65.00         64.5
              #13     66            61.50         57.5
              #14     72            73.00         74.0
              #15     115           71.00         72.0
              #16     90            61.50         59.5
              #17     74            66.00         66.0
              #18     64            70.00         69.0
              #19     85            68.00         66.0
              #20     66            69.00         67.0
              #21     101           71.00         70.0
              #22     82            70.00         68.0
              #23     63            69.00         71.0
              #24     67            65.00         63.0

# Seleccionar algunas de las filas para multiples columnas, en este caso: CWDistance, Height, Wingspan
df.loc[:9, ["CWDistance", "Height", "Wingspan"]]
              # Resultado:
              #       CWDistance    Height        Wingspan
              #0      79            62.00         61.0
              #1      70            62.00         60.0
              #2      85            66.00         64.0
              #3      87            64.00         63.0
              #4      72            73.00         75.0
              #5      81            75.00         71.0
              #6      107           75.00         76.0
              #7      98            65.00         62.0
              #8      106           74.00         73.0
              #9      65            63.00         60.0

# Seleccionando un rango de filas para todas las columnas:
df.loc[10:15]
       # Resultado
       # ID     Age    Gender Gender Group  Glasses     GlassesGroup  Height Wingspan      CWDistance    Complete      CompleteGroup Score
       # 10     11     30     M      2      Y           1             69.50  66.0          96            Y             1             6
       # 11     12     28     F      1      Y           1             62.75  58.0          79            Y             1             10
       # 12     13     25     F      1      Y           1             65.00  64.5          92            Y             1             6
       # 13     14     23     F      1      N           0             61.50  57.5          66            Y             1             4
       # 14     15     31     M      2      Y           1             73.00  74.0          72            Y             1             9
       # 15     16     26     M      2      Y           1             71.00  72.0          115           Y             1             6

# Ahora, si queremos los 10 primeros datos de "CWDistance":
df.loc[:9, "CWDistance"]
              # Resultado:
              #0      79
              #1      70
              #2      85
              #3      87
              #4      72
              #5      81
              #6     107
              #7      98
              #8     106
              #9      65
              #Name: CWDistance, dtype: int64

