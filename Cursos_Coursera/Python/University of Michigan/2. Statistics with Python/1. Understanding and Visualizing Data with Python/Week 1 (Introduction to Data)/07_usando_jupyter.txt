Como colocar Headers con mayor o menor tamaño?

# Header
## Header
### Header

Mientras mas asteriscos, menor el tamaño

# -------------------------------------------------
Modificaciones de textos:

- Italics: *Epale* o tambien puede ser _Epale_
- Negrilla: **Esto está en negrilla* o tambien puede ser __Esto tambien__
- Italics y Negrillas: **__Esto que esta acá está en italic y negrillas __**
- Tachado: ~Esto aca está tachado~

# -------------------------------------------------
Como hacer listas?

1. Primer item
2. Segundo item
	* Lista sin orden
1. De hecho los numeros no importan. Solo importa que ponga un numero.
	1. Sub lista con orden numerico
4. Cuarto item

* Listas sin orden pueden llevar un asterisco
- O tambien pueden llevar un simbolo de resta
+ Tambien simbolos de suma

# -------------------------------------------------
Como colocar links?

Si queremos podemos poner la pagina como queramos, por ejemplo:
http://www.umich.edu

<http://www.umich.edu>

[Sitio de la Universidad de Michigan].(http://www.umich.edu)
Con este ultimo le estamos dando un caption a la pagina web para que nos aparezca como queremos