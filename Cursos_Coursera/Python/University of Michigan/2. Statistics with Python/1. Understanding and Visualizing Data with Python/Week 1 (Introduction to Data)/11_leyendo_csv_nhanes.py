# Leeremos la data de NHANES del periodo 2015-2016
# La pagina es: https://wwwn.cdc.gov/nchs/nhanes/Default.aspx
import pandas as pd

# Esta es la forma para abrir el archivo desde internet
url = "nhanes_2015_2016.csv"
da = pd.read_csv(url)

# Esta es la forma de leer el archivo si ya esta en tu directorio

# Para comprobar nuestra data, vemos la cantidad de filas y columnas:
print(da.shape)
	# Resultado
	# (5735, 28)

# Explorando la data de las columnas tenemos:

#Index(['SEQN', 'ALQ101', 'ALQ110', 'ALQ130', 'SMQ020', 'RIAGENDR', 'RIDAGEYR',
#       'RIDRETH1', 'DMDCITZN', 'DMDEDUC2', 'DMDMARTL', 'DMDHHSIZ', 'WTINT2YR',
#       'SDMVPSU', 'SDMVSTRA', 'INDFMPIR', 'BPXSY1', 'BPXDI1', 'BPXSY2',
#       'BPXDI2', 'BMXWT', 'BMXHT', 'BMXBMI', 'BMXLEG', 'BMXARML', 'BMXARMC',
#       'BMXWAIST', 'HIQ210'],
#      dtype='object')

# Evaluamos el tipo de la data:
da.dtypes
		# Resultado:
		#SEQN          int64
		#ALQ101      float64
		#ALQ110      float64
		#ALQ130      float64
		#SMQ020        int64
		#RIAGENDR      int64
		#RIDAGEYR      int64
		#RIDRETH1      int64
		#DMDCITZN    float64
		#DMDEDUC2    float64
		##DMDMARTL    float64
		#DMDHHSIZ      int64
		#WTINT2YR    float64
		#SDMVPSU       int64
		#SDMVSTRA      int64
		#INDFMPIR    float64
		#BPXSY1      float64
		#BPXDI1      float64
		#BPXSY2      float64
		#BPXDI2      float64
		#BMXWT       float64
		#BMXHT       float64
		#BMXBMI      float64
		#BMXLEG      float64
		#BMXARML     float64
		#BMXARMC     float64
		#BMXWAIST    float64
		#HIQ210      float64
		#dtype: object

# Dividiendo mi data set
	# As discussed above, a Pandas data frame is a rectangular data table, in which the rows represent 
	# cases and the columns represent variables. One common manipulation of a data frame is to extract
	# the data for one case or for one variable. There are several ways to do this, as shown below.

	# To extract all the values for one variable, the following three approaches are equivalent
	# ("DMDEDUC2" here is an NHANES variable containing a person's educational attainment).
	# In these four lines of code, we are assigning the data from one column of the data frame da into 
	# new variables w, x, y, and z. The first three approaches access the variable by name.
	# The fourth approach accesses the variable by position (note that DMDEDUC2 is in position 9 of
	# the da.columns array shown above -- remember that Python counts starting at position zero).

w = da["DMDEDUC2"]
x = da.loc[:, "DMDEDUC2"]
y = da.DMDEDUC2
z = da.iloc[:, 9]  # DMDEDUC2 is in column 9

# Another reason to slice a variable out of a data frame is so that we can then pass it 
# into a function. For example, we can find the maximum value over all DMDEDUC2 values using
# any one of the following four lines of code:

print(da["DMDEDUC2"].max())
print(da.loc[:, "DMDEDUC2"].max())
print(da.DMDEDUC2.max())
print(da.iloc[:, 9].max())
	# Resultado:
	# 9.0
	# 9.0
	# 9.0
	# 9.0

#It may also be useful to slice a row (case) out of a data frame. Just as a data frame's columns have
# names, the rows also have names, which are called the "index". However many data sets do not
# have meaningful row names, so it is more common to extract a row of a data frame using its position.
# The iloc method slices rows or columns from a data frame by position (counting from 0).
# The following line of code extracts row 3 from the data set (which is the fourth row, counting from zero).
x = da.iloc[3, :]

#Another important data frame manipulation is to extract a contiguous block of rows or columns from the data set.
# Below we slice by position, in the first case taking row positions 3 and 4 (counting from 0,
# which are rows 4 and 5 counting from 1), and in the second case taking columns 2, 3, and 4
# (columns 3, 4, 5 if counting from 1).
x = da.iloc[3:5, :]
y = da.iloc[:, 2:5]

# Missing Values
 # When reading a dataset using Pandas, there is a set of values including 'NA', 'NULL', and 'NaN' that are
 # taken by default to represent a missing value. The full list of default missing value
 # codes is in the 'read_csv' documentation here. This document also explains how to change
 # the way that 'read_csv' decides whether a variable's value is missing.

 # Pandas has functions called isnull and notnull that can be used to identify where the missing and
 # non-missing values are located in a data frame. Below we use these functions to count the number
 # of missing and non-missing DMDEDUC2 values.

print(pd.isnull(da.DMDEDUC2).sum())
print(pd.notnull(da.DMDEDUC2).sum())

# As an aside, note that there may be a variety of distinct forms of missingness in a variable, and in some
# cases it is important to keep these values distinct. For example, in case of the DMDEDUC2 variable,
# in addition to the blank or NA values that Pandas considers to be missing, three people
# responded "don't know" (code value 9). In many analyses, the "don't know" values will also be
# treated as missing, but at this point we are considering "don't know" to be a distinct category of observed response.